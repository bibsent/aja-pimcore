<?php

namespace App\Migrations;

use CoreShop\Component\Pimcore\DataObject\ClassUpdate;
use Doctrine\DBAL\Schema\Schema;
use AppBundle\Model\Product;
use AppBundle\Service\ExportService;
use Pimcore\Migrations\Migration\AbstractPimcoreMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Fyller ut det nye feltet 'exportedToPromusVersion' for eksisterende varer.
 */
class Version20201201125402 extends AbstractPimcoreMigration implements ContainerAwareInterface
{
    /**
     * @var ExportService
     */
    private $exportService;

    /**
     * Sets the container.
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->exportService = $container->get('aja.promus_export_service');
    }

    /**
     * Skip the "Migration X was executed but did not result in any SQL statements." warning.
     *
     * @return bool
     */
    public function doesSqlMigrations(): bool
    {
        return false;
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        foreach (Product::getList() as $product) {
            if ($product->getExportedToPromus()) {
                list($xml, $xmlHash) = $this->exportService->generateXml($product);
                if (!is_null($xmlHash)) {
                    $product->setExportedToPromusVersion($xmlHash)
                        ->save(['versionNote' => 'Registrerte eksportert versjon']);
                    $this->writeMessage(sprintf('<info>Updated product %s</info>', $product->getKey()));
                }
            }
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        //
    }
}
