<?php

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Pimcore\Migrations\Migration\AbstractPimcoreMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210105092648 extends AbstractPimcoreMigration
{
    /**
     * Skip the "Migration X was executed but did not result in any SQL statements." warning.
     *
     * @return bool
     */
    public function doesSqlMigrations(): bool
    {
        return false;
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        //
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        //
    }
}
