<?php

/**
 * This file is used to overwrite constants defined in lib/Bootstrap.php
 */

/**
 * Keep the cache directory outside the Pimcore folder per
 * <https://gitlab.com/bibsent/aja-pimcore/-/issues/1>
 */
define("PIMCORE_SYMFONY_CACHE_DIRECTORY", PIMCORE_PROJECT_ROOT . '/../cache');
