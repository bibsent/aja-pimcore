<?php

return [
    "general" => [
        "archive_treshold_logs" => 7,
        "executeWithMaintenance" => TRUE,
        "processTimeoutMinutes" => 15
    ],
    "email" => [
        "recipients" => [

        ]
    ],
    "executorClasses" => [
        [
            "class" => "\\Elements\\Bundle\\ProcessManagerBundle\\Executor\\PimcoreCommand"
        ],
        [
            "class" => "\\Elements\\Bundle\\ProcessManagerBundle\\Executor\\CliCommand"
        ],
        [
            "class" => "\\Elements\\Bundle\\ProcessManagerBundle\\Executor\\ClassMethod"
        ]
    ],
    "executorLoggerClasses" => [
        [
            "class" => "\\Elements\\Bundle\\ProcessManagerBundle\\Executor\\Logger\\File"
        ],
        [
            "class" => "\\Elements\\Bundle\\ProcessManagerBundle\\Executor\\Logger\\Console"
        ],
        [
            "class" => "\\Elements\\Bundle\\ProcessManagerBundle\\Executor\\Logger\\Application"
        ],
        [
            "class" => "\\Elements\\Bundle\\ProcessManagerBundle\\Executor\\Logger\\EmailSummary"
        ]
    ],
    "executorActionClasses" => [
        [
            "class" => "\\Elements\\Bundle\\ProcessManagerBundle\\Executor\\Action\\Download"
        ],
        [
            "class" => "\\Elements\\Bundle\\ProcessManagerBundle\\Executor\\Action\\OpenItem"
        ],
        [
            "class" => "\\Elements\\Bundle\\ProcessManagerBundle\\Executor\\Action\\JsEvent"
        ]
    ],
    "executorCallbackClasses" => [
        [
            "class" => "\\Elements\\Bundle\\ProcessManagerBundle\\Executor\\Callback\\ExecutionNote"
        ],
        [
            "class" => "\\AppBundle\\ProcessManager\\Executor\\Callback\\ImportSdArguments"
        ]
    ]
];
