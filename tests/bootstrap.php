<?php

require dirname(__DIR__) . '/vendor/autoload.php';

putenv('PIMCORE_ENVIRONMENT=test');

\Pimcore\Bootstrap::setProjectRoot();
\Pimcore\Bootstrap::bootstrap();
