<?php

namespace Tests\Controller;

use Pimcore\Test\WebTestCase;

class CoverControllerTest extends WebTestCase
{
    public function testRedirect(): void
    {
        $client = static::createClient();

        $uuid = "99cfce13-884b-4898-85a7-3a2d1de26b44";
        $client->request("GET", "/$uuid/cover/thumbnail.jpg");

        $this->assertResponseStatusCodeSame(301);
        $this->assertResponseRedirects("https://media.example/$uuid/cover/thumbnail.jpg");
    }
}
