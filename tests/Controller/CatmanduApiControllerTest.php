<?php

namespace Tests\Controller;

use Pimcore\Test\WebTestCase;

class CatmanduApiControllerTest extends WebTestCase
{
    public function testInvalidApiKey()
    {

        $client = static::createClient();

        $client->request("PUT", "/integrations/catmandu/products/123", [], [], [
            'HTTP_APIKEY' => 'invvalid-key',
            'CONTENT_TYPE' => 'application/json; charset=UTF-8',
            // The header format is quite weird. Mind the HTTP_ prefix for non standard headers!
            // Ref: https://symfony.com/doc/current/testing.html#sending-custom-headers
        ], json_encode([
            'hello' => 'world',
        ]));

        $this->assertResponseStatusCodeSame(403);
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    public function testValidApiKey()
    {
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );

        $client = static::createClient();

        $client->request("PUT", "/integrations/catmandu/products/123", [], [], [
            'HTTP_APIKEY' => 'valid-key',
            'CONTENT_TYPE' => 'application/json; charset=UTF-8',
            // The header format is quite weird. Mind the HTTP_ prefix for non standard headers!
            // Ref: https://symfony.com/doc/current/testing.html#sending-custom-headers
        ], json_encode([
            'hello' => 'world',
        ]));

        $this->assertResponseIsSuccessful();
    }


}
