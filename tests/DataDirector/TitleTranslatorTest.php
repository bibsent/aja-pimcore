<?php

namespace Tests\DataDirector;

use AppBundle\DataDirector\TitleTranslator;
use PHPUnit\Framework\TestCase;

class TitleTranslatorTest extends TestCase {

    /**
     * @dataProvider splitTitleExamples
     */
    public function testSplitTitle(?string $input, array $expectedOutput): void
    {
        $this->assertSame(
            TitleTranslator::splitTitle($input),
            $expectedOutput
        );
    }

    public function splitTitleExamples(): array {
        return [
            'T-1 Skiller nummerert serietittel fra hovedtittel adskilt med punktum' => [
                'Portalpilotene 1. Det aller første oppdraget',
                [
                    'seriestitle' => 'Portalpilotene',
                    'seriesnumber' => '1',
                    'maintitle' => 'Det aller første oppdraget',
                    'subtitle' => null,
                ]
            ],
            'T-2 Skiller undertittel fra hovedtittel adskilt med bindestrek' => [
                'En fet hemmelighet - et godt liv uten strenge dietter',
                [
                    'seriestitle' => null,
                    'seriesnumber' => null,
                    'maintitle' => 'En fet hemmelighet',
                    'subtitle' => 'et godt liv uten strenge dietter',
                ]
            ],
            'T-3 Returnerer null for alle ledd som ikke blir funnet' => [
                'Det er en superhelt i boka di',
                [
                    'seriestitle' => null,
                    'seriesnumber' => null,
                    'maintitle' => 'Det er en superhelt i boka di',
                    'subtitle' => null,
                ]
            ],
            'T-4 Hvis tittel er helt tom, blir alle ledd null' => [
                '',
                [
                    'seriestitle' => null,
                    'seriesnumber' => null,
                    'maintitle' => null,
                    'subtitle' => null,
                ]
            ],
            'T-5 Ikke serie før punktum hvis første bokstav etter punktum er liten' => [
                'Arbeiderpartiet og 22. juli',
                [
                    'seriestitle' => null,
                    'seriesnumber' => null,
                    'maintitle' => 'Arbeiderpartiet og 22. juli',
                    'subtitle' => null,
                ]
            ],
        ];
    }

}
