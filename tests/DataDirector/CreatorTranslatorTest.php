<?php

namespace Tests\DataDirector;

use AppBundle\DataDirector\CreatorTranslator;
use PHPUnit\Framework\TestCase;

class CreatorTranslatorTest extends TestCase
{

    /**
     * @dataProvider splitNamesExmples
     */
    public function testSplitNames(string $input, array $expectedOutput): void
    {
        $this->assertSame(
            CreatorTranslator::splitNames($input),
            $expectedOutput
        );
    }

    /**
     * @dataProvider invertNamesSometimesExamples
     */
    public function testInvertNamesSometimes(array $input, bool $invertAlways=false, array $expectedOutput): void
    {
        $this->assertSame(
            CreatorTranslator::invertNamesSometimes($input, $invertAlways),
            $expectedOutput
        );
    }

    public function splitNamesExmples(): array
    {
        return [

            # Eksempler fra Cappelen Damm: Som regel er kun første navn invertert
            'CD-1 Splitt på `og`' => [
                'Viestad, Vibeke Maria og Helena Lindholm',
                [
                    'Viestad, Vibeke Maria',
                    'Helena Lindholm',
                ]
            ],
            'CD-2 Splitt på `/`, ikke splitt på første påfølgende `,`' => [
                'Moestrup, Mette / Søyseth, Eira',
                [
                    'Moestrup, Mette',
                    'Søyseth, Eira',
                ]
            ],
            'CD-3 Ikke splitt på første `,`' => [
                'Lirhus, Agnar',
                [
                    'Lirhus, Agnar',
                ],
            ],
            'CD-4 Splitt på `,`, men ikke det første' => [
                'Eidsvold, Trude Trønnes, Rosa von Krogh og Kine Yvonne Kjær',
                [
                    'Eidsvold, Trude Trønnes',
                    'Rosa von Krogh',
                    'Kine Yvonne Kjær',
                ]
            ],

            # Eksempler fra Vigmostad Bjørke: Invertererer ofte flere navn
            'VB-1 Splitt på `&`, ikke splitt på første påfølgende `,`' => [
                'Drange, Lene & Hauge, Tore Løchstøer',
                [
                    'Drange, Lene',
                    'Hauge, Tore Løchstøer',
                ]
            ],
            'VB-2 Splitt på `og`, ikke splitt på første påfølgende `,`' => [
                'Jansson, Tove og Davidsson, Cecilia',
                [
                    'Jansson, Tove',
                    'Davidsson, Cecilia',
                ]
            ],

            'VB-3 Splitt på annenhvert `,` innad i hver  gruppe adskilt med `og`' => [
                'Bing, Jon, Bringsværd, Tor Åge og Lilleeng, Sigbjørn',
                [
                    'Bing, Jon',
                    'Bringsværd, Tor Åge',
                    'Lilleeng, Sigbjørn',
                ]
            ],
            'VB-4 Splitt på annenhvert `,` innad i hver gruppe adskilt med `&`' => [
                'Di Fiore, Fredrik & Houm, Nicolai & Rudebjer, Lars',
                [
                    'Di Fiore, Fredrik',
                    'Houm, Nicolai',
                    'Rudebjer, Lars',
                ]
            ],
            'VB-5 Splitt på `;`' => [
                'James, Alice; illustrert av Mar Hernandez',
                [
                    'James, Alice',
                    'illustrert av Mar Hernandez',
                ]
            ],
            // 'VB-6' => [  # Denne virker nærmest umulig å fikse uten å ødelegge andre, men har kun sett den én gang
            //     'Lars Mæhle, Jens A. Larsen Aas',
            // ]
        ];
    }

    public function invertNamesSometimesExamples(): array
    {
        return [

            'I-1 Hvis $invertAlways=false: inverter resten av ansvarshavere, kun hvis første ansvarerhaver har invertert navn' => [
                [
                    ['creatorName' => 'Kaluza, Susanne'],
                    ['creatorName' => 'Leonard Furuberg']
                ],
                false,
                [
                    ['creatorName' => 'Kaluza, Susanne'],
                    ['creatorName' => 'Furuberg, Leonard']
                ]
            ],
            'I-2 Hvis $invertAlways=false: ingen endring hvis første ansvarshaver ikke er invertert' => [
                [
                    ['creatorName' => 'Diverse forfattere']
                ],
                false,
                [
                    ['creatorName' => 'Diverse forfattere']
                ]
            ],
            'I-3 Hvis $invertAlways=true: Inverter alle ansvarshavere' => [
                [
                    ['creatorName' => 'Bjørn Ingvaldsen'],
                    ['creatorName' => 'Kristoffer Kjølberg']
                ],
                true,
                [
                    ['creatorName' => 'Ingvaldsen, Bjørn'],
                    ['creatorName' => 'Kjølberg, Kristoffer']
                ]
            ]

        ];
    }

}
