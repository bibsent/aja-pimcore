<?php

namespace Tests\DataDirector;

use AppBundle\DataDirector\BindingTranslator;
use Pimcore\Model\DataObject\AbstractObject;
use Pimcore\Model\DataObject\Binding;
use Pimcore\Model\Element;

class BindingTranslatorTest extends \Pimcore\Test\KernelTestCase
{

    public function setUp(): void
    {
        self::bootKernel();

        $bindingObjects = $this->createDataObjects([
            [
                'key' => 'xxx',
                'name' => 'BindingX'
            ]], '/Kategorier/Innbindinger'
        );
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    public function createDataObjects(array $objects, string $pathToParent): array
    {
        return array_map( function($obj) use($pathToParent){
            $pathToObject = $pathToParent . '/'. $obj['key'];
            $object = AbstractObject::getByPath($pathToObject);

            if(is_null($object))
            {
                $parentId = AbstractObject::getByPath($pathToParent)->getId();
                $object = (new Binding)
                    ->setKey(Element\Service::getValidKey($obj['key'], 'object'))
                    ->setParentId($parentId)
                    ->setName($obj['name'])
                    ->setCode($obj['key'])
                    ->setPublished(true)
                    ->save();
            }
            return $object;
        }, $objects);
    }
    /**
     * @dataProvider bindingExamples
     * @param string|null $input
     * @param string|null $expectedOutput
     */
    public function testBinding(?string $input, ?string $expectedOutput): void
    {
        $this->assertSame(
            BindingTranslator::translate($input),
            $expectedOutput
        );
    }

    public function bindingExamples(): array
    {
        return [
            'B-1: Returnerer navn på innbindingen hvis innbindingskoden blir funnet' => [
                'xxx',
                'BindingX'
            ],
            'B-2: Returnerer null hvis innbindingskoden ikke blir funnet' => [
                'yyy',
                null
            ],
            'B-3 returner null hvis innbindingskoden er tom' => [
                '',
                null
            ],
            'B-4 returner null hvis innbindingskoden null' => [
                null,
                null
            ]
        ];
    }

}
