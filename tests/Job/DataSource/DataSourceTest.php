<?php

namespace Tests\Job\DataSource;

use AppBundle\Job\DataSource\Filesystem;
use Elements\Bundle\ProcessManagerBundle\Model\MonitoringItem;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;

class DataSourceTest extends TestCase
{
    public function testFilenameFilter()
    {
        $source = new Filesystem(
            $this->createMock(MonitoringItem::class),
            new NullLogger(),
            [],
        );

        $source->setFilenameFilter('# Comment line\n^97.+\.jpg\n');

        $this->assertFalse($source->filterFilename('0014551499916.jpg'));
        $this->assertFalse($source->filterFilename('9781565812314.txt'));
        $this->assertTrue($source->filterFilename('9781565812314.jpg'));
    }
}
