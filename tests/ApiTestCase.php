<?php

namespace Tests;

use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;

class ApiTestCase extends TestCase
{
    protected $client;

    protected function setUp()
    {
        parent::setUp();

        $this->client = new Client([
            'base_url' => 'http://localhost:8080',
            'defaults' => [
                'exceptions' => false,
            ]
        ]);
    }
}
