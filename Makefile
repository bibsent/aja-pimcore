export PIMCORE_INSTALL_ADMIN_USERNAME ?= pimcore
export PIMCORE_INSTALL_ADMIN_PASSWORD ?= pimcore
export PIMCORE_INSTALL_MYSQL_HOST_SOCKET ?= db
export PIMCORE_INSTALL_MYSQL_DATABASE ?= pimcore
export PIMCORE_INSTALL_MYSQL_USERNAME ?= pimcore
export PIMCORE_INSTALL_MYSQL_PASSWORD ?= pimcore

PHP_SERVICE ?= php

PHP = docker-compose run --rm \
	-e PIMCORE_INSTALL_ADMIN_USERNAME \
	-e PIMCORE_INSTALL_ADMIN_PASSWORD \
	-e PIMCORE_INSTALL_MYSQL_HOST_SOCKET \
	-e PIMCORE_INSTALL_MYSQL_DATABASE \
	-e PIMCORE_INSTALL_MYSQL_USERNAME \
	-e PIMCORE_INSTALL_MYSQL_PASSWORD \
	$(PHP_SERVICE)

COMPOSER = $(PHP) /usr/bin/composer

build: vendor/bin/pimcore-install var/config/system.yml ## Fetch dependencies and run the first-time Pimcore setup
	@echo "Done"

vendor/bin/pimcore-install:
	$(COMPOSER) install --no-scripts --no-interaction
	$(COMPOSER) run-script --no-interaction post-create-project-cmd

var/config/system.yml:
	$(PHP) ./bin/install
	@echo  -------------------------------------------------------------------
	@echo  Pimcore login:
	@echo  - Username: $(PIMCORE_INSTALL_ADMIN_USERNAME)
	@echo  - Password: $(PIMCORE_INSTALL_ADMIN_USERNAME)
	@echo  --------------------------------------------------------------------

upgrade: ## Upgrade Pimcore after composer.lock has changed
	$(COMPOSER) install --no-scripts --no-interaction
	sleep 3  # allow docker volume some time to update
	$(COMPOSER) run-script --no-interaction post-update-cmd

clean:
	docker-compose rm -s -f -v
	rm -rf var/config/system.yml var/cache var/logs var/bundles var/tmp var/versions var/email var/installer var/recyclebin
	rm -rf web/bundles web/var
	rm -rf vendor

test: var/config/system.yml
	@echo "Done (sort of)"

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: build upgrade help
