# aja-pimcore

Denne filen dokumenterer utvikling av Pimcore-applikasjonen som inngår i Ája, og som kjører på <https://aja-test.bibbi.dev> (test) og <https://aja.bs.no> (prod).
Første del er en guide til hvordan en kommer i gang med oppsett av utviklingsmiljø og vanlige operasjoner.
Resten er dokumentasjon av praksis, kjente problemer og løsninger osv.
Informasjon om deployering og infrastruktur finnes ikke her, men i <https://gitlab.com/bibsent/aja-infra>.

- [1. Lokalt utviklingsmiljø](#1-lokalt-utviklingsmiljø)
  - [1.1. Komme i gang](#11-komme-i-gang)
  - [1.2. Starte stacken](#12-starte-stacken)
  - [1.3. Litt generelt om kommandoer i Docker](#13-litt-generelt-om-kommandoer-i-docker)
  - [1.4. Krypteringsnøkkel](#14-krypteringsnøkkel)
  - [1.5. Tester](#15-tester)
- [2. Praksis](#2-praksis)
  - [2.1. Konfigurasjon](#21-konfigurasjon)
  - [2.2. Identifikatorer for klasser](#22-identifikatorer-for-klasser)
  - [2.3. Endring av klassedefinisjoner for Data Objects](#23-endring-av-klassedefinisjoner-for-data-objects)
    - [2.3.1. classes-rebuild](#231-classes-rebuild)
    - [2.3.2. Symfony migrations](#232-symfony-migrations)
  - [2.4. Data Director](#24-data-director)
    - [2.4.1. Oppgradere Data Director](#241-oppgradere-data-director)
  - [2.5. Process Manager](#25-process-manager)
  - [2.6. Advanced Object Search](#26-advanced-object-search)
  - [2.7. Oppgradere Pimcore](#27-oppgradere-pimcore)
- [3. API-er](#3-api-er)
  - [3.1. Catmandu](#31-catmandu)
    - [3.1.1. `PUT /integrations/catmandu/product`](#311-put-integrationscatmanduproduct)
- [4. Feilsøking, kjente problemer m.m.](#4-feilsøking-kjente-problemer-mm)
  - [4.1. Slette cache](#41-slette-cache)
  - [4.2. Logger](#42-logger)
  - [4.3. 403-feil pga. symbolske lenker](#43-403-feil-pga-symbolske-lenker)
  - [4.4. Docker for Mac er treg og spiser mye CPU](#44-docker-for-mac-er-treg-og-spiser-mye-cpu)
    - [4.4.1. Docker med NFS](#441-docker-med-nfs)
  - [4.5. Open upstream issues that affect us](#45-open-upstream-issues-that-affect-us)
  - [4.6. Upstream issues we've contributed to fixing](#46-upstream-issues-weve-contributed-to-fixing)

## 1. Lokalt utviklingsmiljø

### 1.1. Komme i gang

For å bygge Ája trenger du:

- [Docker](https://www.docker.com/) med minst 8 GB minne tilgjengelig 
  (innstilling under `Preferences > Resources` i Docker Desktop).
- [Git](https://git-scm.com/)

Lokal installasjon av Apache, PHP m.m. er ikke nødvendig.

Start med å hente ned koden til egen maskin og aktivere git hooks:

    git clone git@gitlab.com:bibsent/aja-pimcore.git
    cd aja-pimcore
    git config core.hooksPath git-hooks

Opprett en fil `auth.json` i denne mappen:

```json
{
  "gitlab-token": {
    "gitlab.com": "DITT_GITLAB_TOKEN"
  },
  "github-oauth": {
    "github.com": "DITT_GITHUB_TOKEN"
  }
}
```

der 

* DITT_GITLAB_TOKEN er et [personal access token på GitLab](https://gitlab.com/-/profile/personal_access_tokens) (scope: `read_api`). Dette gir til private avhengigheter som Data Director.
* DITT_GITHUB_TOKEN er et [tilsvarende token på GitHub](https://github.com/settings/tokens/new?scopes=public_repo&description=Composer+on+aja-pimcore+dev) (scope: `public_repo`).
  Dette gjør at Composer kan gjøre langt flere API-kall uten å støte på trafikkbegrensninger,
  og gjør henting av Composer-avhengigheter mye raskere.

Pimcore kjører som brukeren `www-data` som har UID 33 som standard,
men du kan endre dette med miljøvariabelen `RUN_AS_UID`.
I utviklingsmiljøet monteres hele prosjektmappa inn i containeren,
og du kan få problemer med filrettigheter når `www-data` prøver å skrive til filer.
Den enkleste måten å håndtere dette på, er å legge til

    RUN_AS_UID=DIN_UID

i `.env.local` (opprett fila hvis den ikke finnes), der `DIN_UID` er ID-en du får fra `id -u`.
`www-data`-brukeren vil da kjøre med samme UID som din egen bruker.
Se [Symfony-dokumentasjonen](https://symfony.com/doc/current/configuration.html) for mer informasjon
om `.env.local` og konfigurasjon via miljøvariabler.

Nå kan du kjøre

    make

som henter inn avhengigheter og kjører gjennom Pimcore-installasjonen, aktivering av bundles osv.
Ta gjerne en titt på [`Makefile`](./Makefile) og [`bin/install`](./bin/install) for å se hvilke kommandoer som kjøres.

### 1.2. Starte stacken

For å starte de containerbaserte tjenestene definert i [`docker-compose.yml`](./docker-compose.yml) (mariadb, elasticsearch, php osv.), kjør:

    docker-compose up -d

Etter en liten stund, blir admingrensesnittet tilgjengelig på <https://localhost:8080/admin>, og du kan logge inn med pimcore / pimcore.
Hvis port 8080 er opptatt, kan du velge en annen port ved å sette `HTTP_PORT`-variabelen slik:

    HTTP_PORT=8081 docker-compose up -d

### 1.3. Litt generelt om kommandoer i Docker

Med mindre annet er angitt, kjøres alle kommandoer beskrevet i resten av dette dokumentet (som `composer`, `bin/console`, osv.) i php-containeren.
Hvis stacken er oppe (du har kjørt `docker-compoe up`), kan du kjøre en kommando (f.eks. `whoami`) i den kjørende containeren slik:

    docker-compose exec php whoami

Merk at de aller fleste kommandoer bør kjøres som `www-data` for å unngå gale filrettigheter:

    docker-compose exec -u www-data php whoami

For å kjøre flere kommandoer, kan du også starte et shell i containeren:

    docker-compose exec -u www-data php bash

Prosjektmappen (denne mappen) er montert som `/var/www/html` inni containeren. Cache-mappen er utelatt.

Hvis stacken ikke er oppe (eller en ikke husker), kan en også kjøre en kommando i en ny container for engangsbruk:

    docker-compose run --rm php whoami

Med `--rm` blir containeren kastet når den avsluttes, så den ikke blir liggende og ta opp plass.
Merk at entrypoint-scriptet sørger for at alle kommandoer kjører som `www-data`.


### 1.4. Krypteringsnøkkel

For å kunne jobbe med [krypterte felt](https://pimcore.com/docs/pimcore/current/Development_Documentation/Objects/Object_Classes/Data_Types/Others.html#page_Encrypted-Field)
må du opprette en lokal krypteringsnøkkel.
Bruk kommandoen

    vendor/bin/generate-defuse-key

og lagre resultatet i `.env.local` som `PIMCORE_ENCRYPTION_KEY`:

    PIMCORE_ENCRYPTION_KEY=Legg-inn-nøkkelen-her

### 1.5. Tester

For å kjøre tester:

    composer test

Kommandoen installerer PhpUnit-versjonen som er angitt i `phpunit.xml.dist`. Denne fila inneholder også andre innstillinger for PhpUnit. Miljøvariabler for testmiljøet er satt i `.env.test`. 

Hvis endringer i konfigurasjon ikke fanges opp, prøv

    ./bin/console cache:clear --env=test

For å sjekke konfigurasjonen:

    bin/console debug:config --env=test FrameworkBundle

Vi bruker [PhpUnit Bridge for Symfony](https://symfony.com/doc/current/components/phpunit_bridge.html), som er anbefalt av Pimcore.

## 2. Praksis

### 2.1. Konfigurasjon

Konfigurasjon finnes i `app/config`, samt i miljøvariabler i `.env`.
Hemmeligheter (passord og nøkler) legges alltid i `.env.local`, som ignoreres av Git.

### 2.2. Identifikatorer for klasser

Vi valgte innledningsvis å ikke bruke løpenummerbaserte identifikatorer for klassene for 
å redusere risikoen for at en klasse blir overskrevet hvis f.eks. to
utviklere oppretter en klasse samtidig med samme løpenummer.
Løpenumre er jo ikke ideelt i distribuerte systemer.
I praksis er vi et så lite team at det sikkert aldri hadde blitt noe problem,
men dette er i hvert fall grunnen til at vi heller bruker
korte, alfabetiske identifikatorer for klasser, f.eks. PRD for Product.


### 2.3. Endring av klassedefinisjoner for Data Objects

#### 2.3.1. classes-rebuild

Mange endringer kan gjøres direkte i Admin-GUI-et til Pimcore uten at det byr på utfordringer: vi kan f.eks. legge til felt, endre tittel, hjelpetekster, flytte rundt på og slette felt der.
Når vi lagrer i GUI-et, oppdateres klassedefinisjonsfilene i `var/classes`-mappen (f.eks. `var/classes/definition_MyClass.php`), som så kan committes til Git.

Når andre henter inn endringene, må de kjøre en kommando for å oppdatere databasen til å følge definisjonsfilene:

    bin/console pimcore:deployment:classes-rebuild -c

På serverne kjøres denne automatisk etter hver `git pull`. Kommandoen legger til og sletter klasser, legger til, oppdaterer og sletter felt.

OBS: Kommandoen forholder seg bare til siste tilstand, den har ikke noen forståelse av endringer. Hvis et felt har endret navn fra A til B, sletter den bare A og oppretter et nytt felt B, med følgen at alle data i feltet går tapt. Konsekvens: Vi endrer aldri navn på felt i Admin-GUI-et, men lager i stedet Symfony migrations for dette.

#### 2.3.2. Symfony migrations

- Migrations ligger i `app/Migrations` og kjøres med `bin/console pimcore:migrations:migrate`.
- Migrations er utmerket for å migrere data (f.eks. populere et nytt felt eller gjøre gjennomgående endringer)
- Migrations bør, hvis det er mulig, ikke brukes til å gjøre endringer i klassedefinisjonene.

Det siste kan virke rart hvis en kommer fra andre rammeverk, men har med samspillet med `classes-rebuild` å gjøre.

La oss si at vi ønsker å omdøpe feltet `author` til `creator` i klassen `Book` (uten å miste data).
Vi kjører `bin/console pimcore:migrations:generate` og finner ut at det finnes en `renameField`-funksjon i `ClassUpdate`-klassen
i [CoreShop Pimcore Utilities Component](https://packagist.org/packages/coreshop/pimcore)
(som til tross for navnet er en generisk pakke med Pimcore-støttefunksjoner som kan brukes helt uavhengig av de andre CoreShop-pakkene).
Vi kjører migreringen lokalt, og den funker akkurat som den skal – den oppdaterer databasen *og*
klassedefinisjonsfilen `var/classes/definition_Book.php`. Sweet!

Men så begynner utfordringene. Hvis vi committer den oppdaterte klassedefinsjonsfilen til Git, vil ikke migreringen ikke fungere for andre (det vil si: serverne våre og eventuelle kollegaer).
Migreringen lærer nemlig om tilstanden til klassen fra klassedefinisjonsfilen, ikke fra å spørre databasen!
Så hvis klassedefinsjonsfilen allerede er oppdatert, vil ikke migreringen gjøre noe, uavhengig av hvilken tilstand databasen er i.
Andre kan kjøre `classes-rebuild` for å få databasen i sync med definisjonsfilen igjen, men da med datatap som følge.

En mulighet kan være å *vente* med å committe definisjonsfila til migreringen er kjørt hos andre.
Men når migreringen kjører på serverne, får vi da et git repo med *unstaged changes* fordi `definition_Book.php` har blitt oppdatert.
Vi kan la Git overskrive den ved neste deploy, men da har vi plutselig en definisjonsfil som er i utakt med tilstanden til databasen igjen, og risikerer datatap hvis `classes-rebuild` skulle bli kjørt.
I praksis kan vi da *ikke* kan gjøre en ny deploy før vi har oppdatert definisjonsfilen i Git-repoet.
Dette begynner å se ut som en ganske skjør prosess med mange fallgruver.

En mer robust prosess vil være å gjøre oppdateringen i to adskilte operasjoner. Først:

1. I Admin-GUI-et: Legg til det nye feltet (`creator`) og skjul det gamle (`author`). Commit definisjonsfilen til Git.
1. Lag en migreringsfil som ikke gjør endringer i klassedefinisjonen, men kun kopierer data over til det nye feltet. Commit både migrasjonen og klassedefinisjonen.
1. Automatisk deploy kjører føst `classes-rebuild` (som oppretter det nye feltet), deretter `migrations:migrate` (som kopierer dataene) på serverne.

På et senere tidspunkt kan vi fjerne det gamle feltet:

1. I Admin-GUI-et: Slett det gamle feltet (`author`). Commit definisjonsfilen til Git.
2. Automatisk deploy kjører føst `classes-rebuild` (slettet feltet), deretter `migrations:migrate` (ingen endringer) på serverne.

Fordelen med denne måten å gjøre det på er vi aldri kommer i en situasjon der klassedefinisjonsfilen er ute av sync,
og det er ikke noe hast med å gjøre andre del av prosessen – vi kan fint deploye andre endringer i mellomtiden uten å risikere datatap.

Når en lager migreringer må en ta høyde for at migreringen også skal kunne kjøre på nyinstallasjoner i fremtiden, der databasen allerede er i nyeste tilstand i det migreringen kjører.
I eksempelet over vil det innebære at en avslutter på en anstendig måte (uten å kræsje) hvis feltet `author` ikke finnes.

### 2.4. Data Director

Data Director lagrer import/eksport-definisjoner som JSON-filer i `var/bundles/BlackbitDataDirector`.
Databasen oppdateres fra filene med kommandoen:

    bin/console data-director:deployment:dataport-rebuild

På serveren kjøres denne automatisk etter hver pull.
Vi har ikke git hooks som minner oss på å kjøre gjøre det samme lokalt, men det hadde vært fint å fikset en påminnelse kun når relevante filer har blitt endret (siden den tar litt tid å kjøre).

#### 2.4.1. Oppgradere Data Director

Composer henter Data Director fra `bibsent`-grenen av https://gitlab.com/bibsent/data-director,
som inneholder våre egne patcher på toppen av en versjon fra det private BitBucket-repoet til BlackBit.

For å sette opp repoet:

    git clone git@gitlab.com:bibsent/data-director.git
    git remote add blackbit git@bitbucket.org:blackbitwerbung/pimcore-plugins-data-director.git
    git fetch blackbit

På et eller annet tidspunkt kan det f.eks. se slik ut:

```
              o--o---o origin/bibsent
             /
    ···-o---o  v2.0.1
```

der grenen vår, `origin/bibsent`, bygger på `v2.0.1`, men også har tre egne commits på toppen av denne.

På et senere tidspunkt har kanskje BlackBit gjort et par nye commits og sluppet versjon 2.1.1:

```
              o--o---o origin/bibsent
             /
    ···-o---o  v2.0.1
             \
              o---o---o---o---o---o---o  v2.1.1
```

For å flytte grenen vår fra å bygge på `v2.0.1` til å bygge på `v2.1.1`, kan vi bruke rebase, slik:

    git rebase --onto v2.1.1 v2.0.1 bibsent

som gir

```
    ···-o---o  v2.0.1
             \
              o---o---o---o---o---o---o  v2.1.1
                                       \
                                        o--o---o origin/bibsent
```

Hvis alt går som det skal, er det bare å ta:

    git push origin bibsent --force

(vi må ha med `--force` her fordi vi har omskrevet historikken)
og gå tilbake til `aja-pimcore`-repoet og ta en `composer update` der.

### 2.5. Process Manager

Process Manager lagrer ikke definisjonene sine som filer, så Process Manager-jobber må opprettes/oppdateres manuelt.

### 2.6. Advanced Object Search

Hvis Advanced Object Search ikke virker, prøv å gjenoppbygge søkeindeksen slik:

    bin/console advanced-object-search:update-mapping
    bin/console advanced-object-search:re-index

Innstillinger til modulen ligger i `app/config/pimcore/advancedobjectsearch`.

### 2.7. Oppgradere Pimcore

Bytt ut versjonsummeret for `pimcore/pimcore` i `composer.json`, sjekk om noen bundles også må oppdateres, og kjør

    composer upgrade

## 3. API-er

### 3.1. Catmandu

#### 3.1.1. `PUT /integrations/catmandu/product`



## 4. Feilsøking, kjente problemer m.m.

### 4.1. Slette cache

Symfonys motsats til "Har du prøvd å skru den av og på igjen?" er "Har du prøvd å slette cachen?"
Dette er ofte det første en bør prøve:

    bin/console cache:clear
    bin/console pimcore:cache:clear

### 4.2. Logger

Logging håndteres av [Monolog](https://seldaek.github.io/monolog/).
Vår egen konfigurasjon i `app/config` legges oppå
[Pimcore-konfigurasjonen](https://pimcore.com/docs/pimcore/current/Development_Documentation/Development_Tools_and_Details/Logging.html)
som igjen legges oppå
[Symfony-konfigurasjonen](https://symfony.com/doc/4.4/logging.html).
Hva resultatet blir til slutt kan en se fra:

    bin/console debug:config monolog

Vi har mer debug-logging i dev og stage enn prod, men prod har logging til Sentry.
Ved å endre `PIMCORE_ENVIRONMENT` kan du enkelt sjekke konfigurasjonen i et annet miljø:

    PIMCORE_ENVIRONMENT=prod bin/console debug:config monolog

Logger havner i `var/logs`, stack traces i `var/logs/php.log`.

### 4.3. 403-feil pga. symbolske lenker

Hvis en får 403-feil og `docker-compose logs php` viser

    AH00037: Symbolic link not allowed or link target not accessible.

skyldes det sannsynligvis ulikt eierskap på `web` og `vendor`-mappene (eller undermapper).
Ved installasjon av bundles, lages det symlenker fra mapper under
`web/bundles/` til mapper under `vendor/`,
Fordi Pimcore kommer med `SymLinksIfOwnerMatch` aktivert som default,
må disse eies av samme bruker.
Og siden førstnevnte mappe må kunne skrives til av `www-data`-brukeren,
betyr det at det er `www-data` som må stå som eier av begge mappene.
I praksis virker det egentlig som om Pimcore baserer seg litt på at `www-data`
skal stå som eier av hele pimcore-mappen, så enkleste fiks er:

    docker-compose exec php chown -R www-data:www-data /var/www/html

### 4.4. Docker for Mac er treg og spiser mye CPU

Hovedproblemet er at vi monterer hele prosjektmappen (bortsett fra cache-mappen) inn i php-containeren, 
og Docker for Mac bruker en ganske treg driver (osxfs) for fildeling mellom hostmaskinen og containere,
som også har en lei tendens til å bruke veldig mye CPU.
I versjon 2.4.0.0 av Docker Desktop ble det innført en ny driver (gRPC FUSE) som skal bruke mindre CPU,
men som er *enda* tregere enn den gamle.

Hva kan en gjøre?

1. Montere mappa med NFS i stedet. Se oppskrift under.
2. Utelate mapper fra sync. Noen kandidater: 
   1. `vendor`: veldig stor og trenger ikke å synkes fra host til container. Grunnen til at vi ikke har utelatt den som default, er at det er praktisk å ha den på hostmaskinen for å få IDE-støtte.
   Men hvis ting blir altfor tregt, er det en mulighet å ha en lokal kopi som ikke er synkronisert med Docker-containeren, siden filene bare endres når en kjører composer install/update.
   2. `var/logs`: praktisk å ha logger på hostmaskinen også, men en kan fint leve uten.

#### 4.4.1. Docker med NFS

Generelt er det to ulemper med NFS:

1. Ingen støtte for fs events (inotify). Dette er imidlertid ikke noe problem for *dette* prosjektet.
2. Ingen brukermapping, så filene får alltid samme UID/GID inni og utenfor containeren. 
   Vi har jobbet oss rundt dette ved å endre UID-en til `www-data`-brukeren i scriptet 
   `docker-entrypoint.sh` som kjører når containeren starter.

For å aktivere NFS på Mac, legg til følgende linje i `/etc/nfs.conf`:

    nfs.server.mount.require_resv_port = 0

og legg til følgende linje i `/etc/exports` for å dele hjemmeområdet ditt via NFS til localhost:

    /System/Volumes/Data/Users/DITTBRUKERNAVN -alldirs localhost

Siden vi bare deler med localhost, kan du svare nei hvis brannmuren din spør om du vil godta innkommende tilkoblinger til NFS-tjenestene (nfsd, portmap, statd, mountd, lockd, rquotad).
Da unngår en at eksterne utnytter svakheter i NFS.
Det går også an å eksportere en undermappe fremfor å eksportere hele hjemmeområdet,
hvis en er bekymret for sikkerheten ved NFS lokalt på maskinen.
For å aktivere endringene, kjør

    sudo nfsd restart

Hvis alt funker, skal `showmount -e` vise noe slikt:

    Exports list on localhost:
    /System/Volumes/Data/Users/danmichael localhost

Opprett så en `docker-compose.override.yml` i prosjektmappen (denne mappen) med dette innholdet:

    version: "3.7"

    services:
      php:
        volumes:
          - nfsmount:/var/www/html
        environment:
          - RUN_AS_UID=501  # Verdien fra: id -u

    volumes:
      nfsmount:
        driver: local
        driver_opts:
          type: nfs
          o: addr=host.docker.internal,rw,nolock,hard,nointr,nfsvers=3
          device: ":/System/Volumes/Data/${PWD}"

Sjekk at verdien i `RUN_AS_UID` stemmer med bruker-ID-en din, som du finner fra `id -u`.

### 4.5. Open upstream issues that affect us

- Changes from Calculated Values and Event Listeners to a Data Object are not reflected in the editing interface directly after saving, but requires reloading the object. <https://github.com/pimcore/pimcore/issues/4678>

- Renaming a Data Object Field in the class editor causes all data associated with that field to be deleted (without warning!). See <https://talk.pimcore.org/t/how-to-update-field-name/562> and <https://github.com/pimcore/pimcore/issues/798>. Info on how we can do more complex migrations below.

- The PHP PDO driver has a memory leak that affects Doctrine DBAL 2.X (https://github.com/doctrine/dbal/issues/3047),
  and can be seen in larger imports (see https://github.com/pimcore/pimcore/issues/3104).
  Should be fixed in DBAL 3, released 2020-11-15, but it may take some time before Pimcore updates to it.

- Versions of a Data Object that aren't compatible with the *current*
  class definition cannot be viewed.
  Pimcore logs an error "Version: cannot read version data from file system because of incompatible class." is logged
  and returns a generic 404 error:
  ![Screenshot](https://hostr.co/file/WWXplQf7ZEvw/Screenshot2021-01-05at15.32.21.png)
  The error message is a bit misleading, since (A) the problem was not that the version could not be found, and (B) there is nothing we can do to fix it.
  For these reasons, we could consider replacing it.

### 4.6. Upstream issues we've contributed to fixing

- 2021-02-06 Process Manager -  Fix test for failed child processes + More detailed progress for parallel imports
  <https://github.com/elements-at/ProcessManager/pull/90>

- 2021-02-06 Process Manager - Cancel child processes when cancelling parent process
  <https://github.com/elements-at/ProcessManager/pull/89/files>
  
- 2021-02-06 Process Manager - More detailed error messages
  <https://github.com/elements-at/ProcessManager/pull/86>

- 2021-02-06 Process Manager - Fix metadata data type
  <https://github.com/elements-at/ProcessManager/pull/85>
  
- 2021-01-17 Process Manager - Don't log empty message when resetting state
  <https://github.com/elements-at/ProcessManager/pull/84>
  
- 2021-01-07 Pimcore - Make all UserRole setters chainable
  <https://github.com/pimcore/pimcore/pull/7876>

- 2021-01-05 Pimcore - Improve error message when missing permission to create assets
  <https://github.com/pimcore/pimcore/pull/7846>

- 2020-12-05 Advanced Object Search - Deleted objects stuck in queue.
  <https://github.com/pimcore/advanced-object-search/pull/99>

- 2020-12-05 Form Builder - [Model overrides](https://pimcore.com/docs/pimcore/current/Development_Documentation/Extending_Pimcore/Overriding_Models.html) not supported.
  <https://github.com/dachcom-digital/pimcore-formbuilder/pull/260>

- 2020-11-24 Form Builder - Multi file widget overflows its container.
  <https://github.com/dachcom-digital/pimcore-formbuilder/pull/253>

- 2020-10-29 Advanced Object Search - Improve error message.
  <https://github.com/pimcore/advanced-object-search/pull/97>
