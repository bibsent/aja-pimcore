<?php 

/** 
Fields Summary: 
- localizedfields [localizedfields]
-- label [input]
- authorityId [input]
- vocabulary [select]
*/ 

namespace Pimcore\Model\DataObject\Fieldcollection\Data;

use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\PreGetValueHookInterface;

class Genre extends DataObject\Fieldcollection\Data\AbstractData {

protected $type = "Genre";
protected $localizedfields;
protected $authorityId;
protected $vocabulary;


/**
* Get localizedfields - 
* @return \Pimcore\Model\DataObject\Localizedfield
*/
public function getLocalizedfields () {
	$container = $this;
	$fd = $this->getDefinition()->getFieldDefinition("localizedfields");
	$data = $fd->preGetData($container);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Get label - Navn
* @return string|null
*/
public function getLabel ($language = null) {
	$data = $this->getLocalizedfields()->getLocalizedValue("label", $language);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		return $data->getPlain();
	}
	return $data;
}

/**
* Set localizedfields - 
* @param \Pimcore\Model\DataObject\Localizedfield $localizedfields
* @return \Pimcore\Model\DataObject\Fieldcollection\Data\Genre
*/
public function setLocalizedfields ($localizedfields) {
	$fd = $this->getDefinition()->getFieldDefinition("localizedfields");
	$hideUnpublished = \Pimcore\Model\DataObject\Concrete::getHideUnpublished();
	\Pimcore\Model\DataObject\Concrete::setHideUnpublished(false);
	$currentData = $this->getLocalizedfields();
	\Pimcore\Model\DataObject\Concrete::setHideUnpublished($hideUnpublished);
	$isEqual = $fd->isEqual($currentData, $localizedfields);
	if (!$isEqual) {
		$this->markFieldDirty("localizedfields", true);
	}
	$this->localizedfields = $localizedfields;
	return $this;
}

/**
* Set label - Navn
* @param string|null $label
* @return \Pimcore\Model\DataObject\Fieldcollection\Data\Genre
*/
public function setLabel ($label, $language = null) {
	$isEqual = false;
	$this->getLocalizedfields()->setLocalizedValue("label", $label, $language, !$isEqual);
	return $this;
}

/**
* Get authorityId - ID i eksternt vokabular
* @return string|null
*/
public function getAuthorityId () {
	$data = $this->authorityId;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set authorityId - ID i eksternt vokabular
* @param string|null $authorityId
* @return \Pimcore\Model\DataObject\Fieldcollection\Data\Genre
*/
public function setAuthorityId ($authorityId) {
	$fd = $this->getDefinition()->getFieldDefinition("authorityId");
	$this->authorityId = $authorityId;
	return $this;
}

/**
* Get vocabulary - Vokabular
* @return string|null
*/
public function getVocabulary () {
	$data = $this->vocabulary;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set vocabulary - Vokabular
* @param string|null $vocabulary
* @return \Pimcore\Model\DataObject\Fieldcollection\Data\Genre
*/
public function setVocabulary ($vocabulary) {
	$fd = $this->getDefinition()->getFieldDefinition("vocabulary");
	$this->vocabulary = $vocabulary;
	return $this;
}

}

