<?php 

/** 
Fields Summary: 
- creatorName [input]
- creatorType [select]
- creatorRole [multiselect]
- bibbiId [input]
- norafId [input]
*/ 

namespace Pimcore\Model\DataObject\Fieldcollection\Data;

use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\PreGetValueHookInterface;

class Creator extends DataObject\Fieldcollection\Data\AbstractData {

protected $type = "Creator";
protected $creatorName;
protected $creatorType;
protected $creatorRole;
protected $bibbiId;
protected $norafId;


/**
* Get creatorName - Navn
* @return string|null
*/
public function getCreatorName () {
	$data = $this->creatorName;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set creatorName - Navn
* @param string|null $creatorName
* @return \Pimcore\Model\DataObject\Fieldcollection\Data\Creator
*/
public function setCreatorName ($creatorName) {
	$fd = $this->getDefinition()->getFieldDefinition("creatorName");
	$this->creatorName = $creatorName;
	return $this;
}

/**
* Get creatorType - Type
* @return string|null
*/
public function getCreatorType () {
	$data = $this->creatorType;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set creatorType - Type
* @param string|null $creatorType
* @return \Pimcore\Model\DataObject\Fieldcollection\Data\Creator
*/
public function setCreatorType ($creatorType) {
	$fd = $this->getDefinition()->getFieldDefinition("creatorType");
	$this->creatorType = $creatorType;
	return $this;
}

/**
* Get creatorRole - Rolle
* @return array|null
*/
public function getCreatorRole () {
	$data = $this->creatorRole;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set creatorRole - Rolle
* @param array|null $creatorRole
* @return \Pimcore\Model\DataObject\Fieldcollection\Data\Creator
*/
public function setCreatorRole ($creatorRole) {
	$fd = $this->getDefinition()->getFieldDefinition("creatorRole");
	$this->creatorRole = $creatorRole;
	return $this;
}

/**
* Get bibbiId - Bibbi-ID
* @return string|null
*/
public function getBibbiId () {
	$data = $this->bibbiId;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set bibbiId - Bibbi-ID
* @param string|null $bibbiId
* @return \Pimcore\Model\DataObject\Fieldcollection\Data\Creator
*/
public function setBibbiId ($bibbiId) {
	$fd = $this->getDefinition()->getFieldDefinition("bibbiId");
	$this->bibbiId = $bibbiId;
	return $this;
}

/**
* Get norafId - Noraf-ID
* @return string|null
*/
public function getNorafId () {
	$data = $this->norafId;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set norafId - Noraf-ID
* @param string|null $norafId
* @return \Pimcore\Model\DataObject\Fieldcollection\Data\Creator
*/
public function setNorafId ($norafId) {
	$fd = $this->getDefinition()->getFieldDefinition("norafId");
	$this->norafId = $norafId;
	return $this;
}

}

