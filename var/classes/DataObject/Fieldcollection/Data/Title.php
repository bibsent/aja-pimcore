<?php 

/** 
Fields Summary: 
- title [input]
- subtitle [input]
*/ 

namespace Pimcore\Model\DataObject\Fieldcollection\Data;

use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\PreGetValueHookInterface;

class Title extends DataObject\Fieldcollection\Data\AbstractData {

protected $type = "Title";
protected $title;
protected $subtitle;


/**
* Get title - Tittel
* @return string|null
*/
public function getTitle () {
	$data = $this->title;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set title - Tittel
* @param string|null $title
* @return \Pimcore\Model\DataObject\Fieldcollection\Data\Title
*/
public function setTitle ($title) {
	$fd = $this->getDefinition()->getFieldDefinition("title");
	$this->title = $title;
	return $this;
}

/**
* Get subtitle - Undertittel
* @return string|null
*/
public function getSubtitle () {
	$data = $this->subtitle;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set subtitle - Undertittel
* @param string|null $subtitle
* @return \Pimcore\Model\DataObject\Fieldcollection\Data\Title
*/
public function setSubtitle ($subtitle) {
	$fd = $this->getDefinition()->getFieldDefinition("subtitle");
	$this->subtitle = $subtitle;
	return $this;
}

}

