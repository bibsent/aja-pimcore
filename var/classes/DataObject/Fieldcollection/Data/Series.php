<?php 

/** 
Fields Summary: 
- title [input]
- ordinal [input]
*/ 

namespace Pimcore\Model\DataObject\Fieldcollection\Data;

use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\PreGetValueHookInterface;

class Series extends DataObject\Fieldcollection\Data\AbstractData {

protected $type = "Series";
protected $title;
protected $ordinal;


/**
* Get title - seriesTitle
* @return string|null
*/
public function getTitle () {
	$data = $this->title;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set title - seriesTitle
* @param string|null $title
* @return \Pimcore\Model\DataObject\Fieldcollection\Data\Series
*/
public function setTitle ($title) {
	$fd = $this->getDefinition()->getFieldDefinition("title");
	$this->title = $title;
	return $this;
}

/**
* Get ordinal - Nummer i serien
* @return string|null
*/
public function getOrdinal () {
	$data = $this->ordinal;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set ordinal - Nummer i serien
* @param string|null $ordinal
* @return \Pimcore\Model\DataObject\Fieldcollection\Data\Series
*/
public function setOrdinal ($ordinal) {
	$fd = $this->getDefinition()->getFieldDefinition("ordinal");
	$this->ordinal = $ordinal;
	return $this;
}

}

