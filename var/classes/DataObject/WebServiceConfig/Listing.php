<?php 

namespace Pimcore\Model\DataObject\WebServiceConfig;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\WebServiceConfig current()
 * @method DataObject\WebServiceConfig[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "WSC";
protected $className = "WebServiceConfig";


/**
* Filter by filenameFilter (Filnavn-filter:)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByFilenameFilter ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("filenameFilter")->addListingFilter($this, $data, $operator);
	return $this;
}



}
