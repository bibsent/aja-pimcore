<?php 

namespace Pimcore\Model\DataObject\ForlagenesBokgruppe;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\ForlagenesBokgruppe current()
 * @method DataObject\ForlagenesBokgruppe[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "FBG";
protected $className = "ForlagenesBokgruppe";


/**
* Filter by bookgroup (Bokgruppe)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByBookgroup ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("bookgroup")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by descriptionPublisher (Beskrivelse)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByDescriptionPublisher ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("descriptionPublisher")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by descriptionBS (Beskrivelse (BS))
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByDescriptionBS ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("descriptionBS")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by parentBookgroup (Parent ID)
* @param Element\ElementInterface|array $data   comparison element or ['id' => <element ID>, 'type' => <element type>]
* @param string $operator  SQL comparison operator, currently only "=" possible
* @return static
*/
public function filterByParentBookgroup ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("parentBookgroup")->addListingFilter($this, $data, $operator);
	return $this;
}



}
