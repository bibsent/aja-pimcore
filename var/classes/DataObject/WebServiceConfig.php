<?php 

/** 
* Inheritance: no
* Variants: no


Fields Summary: 
- username [encryptedField]
- password [encryptedField]
- filenameFilter [textarea]
*/ 

namespace Pimcore\Model\DataObject;

use Pimcore\Model\DataObject\Exception\InheritanceParentNotFoundException;
use Pimcore\Model\DataObject\PreGetValueHookInterface;

/**
* @method static \Pimcore\Model\DataObject\WebServiceConfig\Listing|\Pimcore\Model\DataObject\WebServiceConfig getByFilenameFilter ($value, $limit = 0, $offset = 0) 
*/

class WebServiceConfig extends Concrete {

protected $o_classId = "WSC";
protected $o_className = "WebServiceConfig";
protected $username;
protected $password;
protected $filenameFilter;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\WebServiceConfig
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get username - Brukernavn
* @return 
*/
public function getUsername () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("username"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->username;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set username - Brukernavn
* @param  $username
* @return \Pimcore\Model\DataObject\WebServiceConfig
*/
public function setUsername ($username) {
	$fd = $this->getClass()->getFieldDefinition("username");
	$encryptedFd = $this->getClass()->getFieldDefinition("username");
	$delegate = $encryptedFd->getDelegate();
	if ($delegate && !($username instanceof \Pimcore\Model\DataObject\Data\EncryptedField)) {
		$username = new \Pimcore\Model\DataObject\Data\EncryptedField($delegate, $username);
	}
	$this->username = $username;
	return $this;
}

/**
* Get password - Passord eller API-nøkkel
* @return 
*/
public function getPassword () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("password"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->password;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set password - Passord eller API-nøkkel
* @param  $password
* @return \Pimcore\Model\DataObject\WebServiceConfig
*/
public function setPassword ($password) {
	$fd = $this->getClass()->getFieldDefinition("password");
	$encryptedFd = $this->getClass()->getFieldDefinition("password");
	$delegate = $encryptedFd->getDelegate();
	if ($delegate && !($password instanceof \Pimcore\Model\DataObject\Data\EncryptedField)) {
		$password = new \Pimcore\Model\DataObject\Data\EncryptedField($delegate, $password);
	}
	$this->password = $password;
	return $this;
}

/**
* Get filenameFilter - Filnavn-filter:
* @return string|null
*/
public function getFilenameFilter () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("filenameFilter"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->filenameFilter;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set filenameFilter - Filnavn-filter:
* @param string|null $filenameFilter
* @return \Pimcore\Model\DataObject\WebServiceConfig
*/
public function setFilenameFilter ($filenameFilter) {
	$fd = $this->getClass()->getFieldDefinition("filenameFilter");
	$this->filenameFilter = $filenameFilter;
	return $this;
}

}

