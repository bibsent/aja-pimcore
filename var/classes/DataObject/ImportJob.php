<?php 

/** 
* Inheritance: no
* Variants: no


Fields Summary: 
- uuid [input]
- integration [manyToOneRelation]
*/ 

namespace Pimcore\Model\DataObject;

use Pimcore\Model\DataObject\Exception\InheritanceParentNotFoundException;
use Pimcore\Model\DataObject\PreGetValueHookInterface;

/**
* @method static \Pimcore\Model\DataObject\ImportJob\Listing|\Pimcore\Model\DataObject\ImportJob getByUuid ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\ImportJob\Listing|\Pimcore\Model\DataObject\ImportJob getByIntegration ($value, $limit = 0, $offset = 0) 
*/

class ImportJob extends Concrete {

protected $o_classId = "IMP";
protected $o_className = "ImportJob";
protected $uuid;
protected $integration;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\ImportJob
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get uuid - uuid
* @return string|null
*/
public function getUuid () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("uuid"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->uuid;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set uuid - uuid
* @param string|null $uuid
* @return \Pimcore\Model\DataObject\ImportJob
*/
public function setUuid ($uuid) {
	$fd = $this->getClass()->getFieldDefinition("uuid");
	$this->uuid = $uuid;
	return $this;
}

/**
* Get integration - Integrasjon
* @return \Pimcore\Model\DataObject\FtpServer|null
*/
public function getIntegration () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("integration"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->getClass()->getFieldDefinition("integration")->preGetData($this);

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set integration - Integrasjon
* @param \Pimcore\Model\DataObject\FtpServer $integration
* @return \Pimcore\Model\DataObject\ImportJob
*/
public function setIntegration ($integration) {
	$fd = $this->getClass()->getFieldDefinition("integration");
	$hideUnpublished = \Pimcore\Model\DataObject\Concrete::getHideUnpublished();
	\Pimcore\Model\DataObject\Concrete::setHideUnpublished(false);
	$currentData = $this->getIntegration();
	\Pimcore\Model\DataObject\Concrete::setHideUnpublished($hideUnpublished);
	$isEqual = $fd->isEqual($currentData, $integration);
	if (!$isEqual) {
		$this->markFieldDirty("integration", true);
	}
	$this->integration = $fd->preSetData($this, $integration);
	return $this;
}

}

