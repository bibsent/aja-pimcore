<?php 

/** 
* Inheritance: no
* Variants: no


Fields Summary: 
- code [input]
- name [input]
*/ 

namespace Pimcore\Model\DataObject;

use Pimcore\Model\DataObject\Exception\InheritanceParentNotFoundException;
use Pimcore\Model\DataObject\PreGetValueHookInterface;

/**
* @method static \Pimcore\Model\DataObject\ProductForm\Listing|\Pimcore\Model\DataObject\ProductForm getByCode ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\ProductForm\Listing|\Pimcore\Model\DataObject\ProductForm getByName ($value, $limit = 0, $offset = 0) 
*/

class ProductForm extends Concrete {

protected $o_classId = "PF";
protected $o_className = "ProductForm";
protected $code;
protected $name;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\ProductForm
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get code - Kode
* @return string|null
*/
public function getCode () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("code"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->code;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set code - Kode
* @param string|null $code
* @return \Pimcore\Model\DataObject\ProductForm
*/
public function setCode ($code) {
	$fd = $this->getClass()->getFieldDefinition("code");
	$this->code = $code;
	return $this;
}

/**
* Get name - Navn
* @return string|null
*/
public function getName () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("name"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->name;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set name - Navn
* @param string|null $name
* @return \Pimcore\Model\DataObject\ProductForm
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

}

