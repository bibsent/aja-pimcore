<?php 

/** 
* Inheritance: no
* Variants: no


Fields Summary: 
- uuid [input]
- productId [input]
- isbn [input]
- originalSource [textarea]
- imageExternal [externalImage]
- title [input]
- publisherSummary [textarea]
- originalTitle [input]
- subTitle [input]
- documentType [select]
- literatureType [select]
- targetAudience [select]
- ageGroup [input]
- binding [select]
- edition [input]
- extent [input]
- biographicalContent [select]
- price [numeric]
- publicationYear [input]
- publicationDate [date]
- publicationDateString [input]
- publicationCountry [country]
- lang [multiselect]
- fileFormat [select]
- productForm [manyToOneRelation]
- weight [numeric]
- height [numeric]
- width [numeric]
- depth [numeric]
- pageCount [input]
- duration [input]
- filesize [input]
- seriesTitle [input]
- seriesNumber [input]
- creator [fieldcollections]
- sdCreatorInit [input]
- sdCreatorUpdate [input]
- imprint [input]
- place [input]
- publisher [input]
- publisherCode [input]
- publisherCategory [input]
- forlagenesBokgruppe [manyToOneRelation]
- bokhandelensVaregruppe [manyToOneRelation]
- genres [fieldcollections]
- subjects [fieldcollections]
- notes [block]
-- Noter [textarea]
- exportedToPromus [checkbox]
- exportedToPromusVersion [input]
- messageCode [input]
- messageText [input]
- messageDate [input]
- sdSeriesNumber [input]
- sdLastUpdate [date]
*/ 

namespace Pimcore\Model\DataObject;

use Pimcore\Model\DataObject\Exception\InheritanceParentNotFoundException;
use Pimcore\Model\DataObject\PreGetValueHookInterface;

/**
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByUuid ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByProductId ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByIsbn ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByOriginalSource ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByTitle ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByPublisherSummary ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByOriginalTitle ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getBySubTitle ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByDocumentType ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByLiteratureType ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByTargetAudience ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByAgeGroup ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByBinding ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByEdition ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByExtent ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByBiographicalContent ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByPrice ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByPublicationYear ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByPublicationDate ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByPublicationDateString ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByPublicationCountry ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByLang ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByFileFormat ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByProductForm ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByWeight ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByHeight ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByWidth ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByDepth ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByPageCount ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByDuration ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByFilesize ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getBySeriesTitle ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getBySeriesNumber ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getBySdCreatorInit ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getBySdCreatorUpdate ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByImprint ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByPlace ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByPublisher ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByPublisherCode ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByPublisherCategory ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByForlagenesBokgruppe ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByBokhandelensVaregruppe ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByExportedToPromus ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByExportedToPromusVersion ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByMessageCode ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByMessageText ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByMessageDate ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getBySdSeriesNumber ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getBySdLastUpdate ($value, $limit = 0, $offset = 0) 
*/

class Product extends Concrete {

protected $o_classId = "PRD";
protected $o_className = "Product";
protected $uuid;
protected $productId;
protected $isbn;
protected $originalSource;
protected $imageExternal;
protected $title;
protected $publisherSummary;
protected $originalTitle;
protected $subTitle;
protected $documentType;
protected $literatureType;
protected $targetAudience;
protected $ageGroup;
protected $binding;
protected $edition;
protected $extent;
protected $biographicalContent;
protected $price;
protected $publicationYear;
protected $publicationDate;
protected $publicationDateString;
protected $publicationCountry;
protected $lang;
protected $fileFormat;
protected $productForm;
protected $weight;
protected $height;
protected $width;
protected $depth;
protected $pageCount;
protected $duration;
protected $filesize;
protected $seriesTitle;
protected $seriesNumber;
protected $creator;
protected $sdCreatorInit;
protected $sdCreatorUpdate;
protected $imprint;
protected $place;
protected $publisher;
protected $publisherCode;
protected $publisherCategory;
protected $forlagenesBokgruppe;
protected $bokhandelensVaregruppe;
protected $genres;
protected $subjects;
protected $notes;
protected $exportedToPromus;
protected $exportedToPromusVersion;
protected $messageCode;
protected $messageText;
protected $messageDate;
protected $sdSeriesNumber;
protected $sdLastUpdate;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\Product
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get uuid - Uuid
* @return string|null
*/
public function getUuid () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("uuid"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->uuid;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set uuid - Uuid
* @param string|null $uuid
* @return \Pimcore\Model\DataObject\Product
*/
public function setUuid ($uuid) {
	$fd = $this->getClass()->getFieldDefinition("uuid");
	$this->uuid = $uuid;
	return $this;
}

/**
* Get productId - Vare-ID
* @return string|null
*/
public function getProductId () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("productId"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->productId;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set productId - Vare-ID
* @param string|null $productId
* @return \Pimcore\Model\DataObject\Product
*/
public function setProductId ($productId) {
	$fd = $this->getClass()->getFieldDefinition("productId");
	$this->productId = $productId;
	return $this;
}

/**
* Get isbn - ISBN
* @return string|null
*/
public function getIsbn () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("isbn"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->isbn;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set isbn - ISBN
* @param string|null $isbn
* @return \Pimcore\Model\DataObject\Product
*/
public function setIsbn ($isbn) {
	$fd = $this->getClass()->getFieldDefinition("isbn");
	$this->isbn = $isbn;
	return $this;
}

/**
* Get originalSource - Originalkilde
* @return string|null
*/
public function getOriginalSource () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("originalSource"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->originalSource;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set originalSource - Originalkilde
* @param string|null $originalSource
* @return \Pimcore\Model\DataObject\Product
*/
public function setOriginalSource ($originalSource) {
	$fd = $this->getClass()->getFieldDefinition("originalSource");
	$this->originalSource = $originalSource;
	return $this;
}

/**
* Get imageExternal - Eksternt omslagsbilde
* @return \Pimcore\Model\DataObject\Data\ExternalImage|null
*/
public function getImageExternal () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("imageExternal"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->imageExternal;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set imageExternal - Eksternt omslagsbilde
* @param \Pimcore\Model\DataObject\Data\ExternalImage|null $imageExternal
* @return \Pimcore\Model\DataObject\Product
*/
public function setImageExternal ($imageExternal) {
	$fd = $this->getClass()->getFieldDefinition("imageExternal");
	$this->imageExternal = $imageExternal;
	return $this;
}

/**
* Get title - Tittel
* @return string|null
*/
public function getTitle () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("title"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->title;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set title - Tittel
* @param string|null $title
* @return \Pimcore\Model\DataObject\Product
*/
public function setTitle ($title) {
	$fd = $this->getClass()->getFieldDefinition("title");
	$this->title = $title;
	return $this;
}

/**
* Get publisherSummary - Forlagets omtale
* @return string|null
*/
public function getPublisherSummary () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("publisherSummary"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->publisherSummary;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set publisherSummary - Forlagets omtale
* @param string|null $publisherSummary
* @return \Pimcore\Model\DataObject\Product
*/
public function setPublisherSummary ($publisherSummary) {
	$fd = $this->getClass()->getFieldDefinition("publisherSummary");
	$this->publisherSummary = $publisherSummary;
	return $this;
}

/**
* Get originalTitle - Originaltittel
* @return string|null
*/
public function getOriginalTitle () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("originalTitle"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->originalTitle;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set originalTitle - Originaltittel
* @param string|null $originalTitle
* @return \Pimcore\Model\DataObject\Product
*/
public function setOriginalTitle ($originalTitle) {
	$fd = $this->getClass()->getFieldDefinition("originalTitle");
	$this->originalTitle = $originalTitle;
	return $this;
}

/**
* Get subTitle - Undertittel
* @return string|null
*/
public function getSubTitle () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("subTitle"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->subTitle;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set subTitle - Undertittel
* @param string|null $subTitle
* @return \Pimcore\Model\DataObject\Product
*/
public function setSubTitle ($subTitle) {
	$fd = $this->getClass()->getFieldDefinition("subTitle");
	$this->subTitle = $subTitle;
	return $this;
}

/**
* Get documentType - Dokumenttype
* @return string|null
*/
public function getDocumentType () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("documentType"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->documentType;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set documentType - Dokumenttype
* @param string|null $documentType
* @return \Pimcore\Model\DataObject\Product
*/
public function setDocumentType ($documentType) {
	$fd = $this->getClass()->getFieldDefinition("documentType");
	$this->documentType = $documentType;
	return $this;
}

/**
* Get literatureType - Litteraturtype
* @return string|null
*/
public function getLiteratureType () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("literatureType"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->literatureType;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set literatureType - Litteraturtype
* @param string|null $literatureType
* @return \Pimcore\Model\DataObject\Product
*/
public function setLiteratureType ($literatureType) {
	$fd = $this->getClass()->getFieldDefinition("literatureType");
	$this->literatureType = $literatureType;
	return $this;
}

/**
* Get targetAudience - Målgruppe
* @return string|null
*/
public function getTargetAudience () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("targetAudience"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->targetAudience;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set targetAudience - Målgruppe
* @param string|null $targetAudience
* @return \Pimcore\Model\DataObject\Product
*/
public function setTargetAudience ($targetAudience) {
	$fd = $this->getClass()->getFieldDefinition("targetAudience");
	$this->targetAudience = $targetAudience;
	return $this;
}

/**
* Get ageGroup - Aldersgrupper
* @return string|null
*/
public function getAgeGroup () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("ageGroup"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->ageGroup;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set ageGroup - Aldersgrupper
* @param string|null $ageGroup
* @return \Pimcore\Model\DataObject\Product
*/
public function setAgeGroup ($ageGroup) {
	$fd = $this->getClass()->getFieldDefinition("ageGroup");
	$this->ageGroup = $ageGroup;
	return $this;
}

/**
* Get binding - Innbinding
* @return string|null
*/
public function getBinding () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("binding"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->binding;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set binding - Innbinding
* @param string|null $binding
* @return \Pimcore\Model\DataObject\Product
*/
public function setBinding ($binding) {
	$fd = $this->getClass()->getFieldDefinition("binding");
	$this->binding = $binding;
	return $this;
}

/**
* Get edition - Utgave
* @return string|null
*/
public function getEdition () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("edition"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->edition;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set edition - Utgave
* @param string|null $edition
* @return \Pimcore\Model\DataObject\Product
*/
public function setEdition ($edition) {
	$fd = $this->getClass()->getFieldDefinition("edition");
	$this->edition = $edition;
	return $this;
}

/**
* Get extent - Omfang
* @return string|null
*/
public function getExtent () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("extent"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->extent;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set extent - Omfang
* @param string|null $extent
* @return \Pimcore\Model\DataObject\Product
*/
public function setExtent ($extent) {
	$fd = $this->getClass()->getFieldDefinition("extent");
	$this->extent = $extent;
	return $this;
}

/**
* Get biographicalContent - Biografisk innhold
* @return string|null
*/
public function getBiographicalContent () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("biographicalContent"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->biographicalContent;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set biographicalContent - Biografisk innhold
* @param string|null $biographicalContent
* @return \Pimcore\Model\DataObject\Product
*/
public function setBiographicalContent ($biographicalContent) {
	$fd = $this->getClass()->getFieldDefinition("biographicalContent");
	$this->biographicalContent = $biographicalContent;
	return $this;
}

/**
* Get price - Pris
* @return float|null
*/
public function getPrice () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("price"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->price;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set price - Pris
* @param float|null $price
* @return \Pimcore\Model\DataObject\Product
*/
public function setPrice ($price) {
	$fd = $this->getClass()->getFieldDefinition("price");
	$this->price = $fd->preSetData($this, $price);
	return $this;
}

/**
* Get publicationYear - Utgivelsesår
* @return string|null
*/
public function getPublicationYear () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("publicationYear"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->publicationYear;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set publicationYear - Utgivelsesår
* @param string|null $publicationYear
* @return \Pimcore\Model\DataObject\Product
*/
public function setPublicationYear ($publicationYear) {
	$fd = $this->getClass()->getFieldDefinition("publicationYear");
	$this->publicationYear = $publicationYear;
	return $this;
}

/**
* Get publicationDate - Utgivelsesdato
* @return \Carbon\Carbon|null
*/
public function getPublicationDate () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("publicationDate"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->publicationDate;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set publicationDate - Utgivelsesdato
* @param \Carbon\Carbon|null $publicationDate
* @return \Pimcore\Model\DataObject\Product
*/
public function setPublicationDate ($publicationDate) {
	$fd = $this->getClass()->getFieldDefinition("publicationDate");
	$this->publicationDate = $publicationDate;
	return $this;
}

/**
* Get publicationDateString - Utgivelsesdato (streng)
* @return string|null
*/
public function getPublicationDateString () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("publicationDateString"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->publicationDateString;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set publicationDateString - Utgivelsesdato (streng)
* @param string|null $publicationDateString
* @return \Pimcore\Model\DataObject\Product
*/
public function setPublicationDateString ($publicationDateString) {
	$fd = $this->getClass()->getFieldDefinition("publicationDateString");
	$this->publicationDateString = $publicationDateString;
	return $this;
}

/**
* Get publicationCountry - Utgivelsesland
* @return string|null
*/
public function getPublicationCountry () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("publicationCountry"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->publicationCountry;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set publicationCountry - Utgivelsesland
* @param string|null $publicationCountry
* @return \Pimcore\Model\DataObject\Product
*/
public function setPublicationCountry ($publicationCountry) {
	$fd = $this->getClass()->getFieldDefinition("publicationCountry");
	$this->publicationCountry = $publicationCountry;
	return $this;
}

/**
* Get lang - Språk
* @return array|null
*/
public function getLang () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("lang"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->lang;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set lang - Språk
* @param array|null $lang
* @return \Pimcore\Model\DataObject\Product
*/
public function setLang ($lang) {
	$fd = $this->getClass()->getFieldDefinition("lang");
	$this->lang = $lang;
	return $this;
}

/**
* Get fileFormat - Fileformat
* @return string|null
*/
public function getFileFormat () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("fileFormat"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->fileFormat;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set fileFormat - Fileformat
* @param string|null $fileFormat
* @return \Pimcore\Model\DataObject\Product
*/
public function setFileFormat ($fileFormat) {
	$fd = $this->getClass()->getFieldDefinition("fileFormat");
	$this->fileFormat = $fileFormat;
	return $this;
}

/**
* Get productForm - Vareformat
* @return \Pimcore\Model\DataObject\ProductForm|null
*/
public function getProductForm () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("productForm"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->getClass()->getFieldDefinition("productForm")->preGetData($this);

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set productForm - Vareformat
* @param \Pimcore\Model\DataObject\ProductForm $productForm
* @return \Pimcore\Model\DataObject\Product
*/
public function setProductForm ($productForm) {
	$fd = $this->getClass()->getFieldDefinition("productForm");
	$hideUnpublished = \Pimcore\Model\DataObject\Concrete::getHideUnpublished();
	\Pimcore\Model\DataObject\Concrete::setHideUnpublished(false);
	$currentData = $this->getProductForm();
	\Pimcore\Model\DataObject\Concrete::setHideUnpublished($hideUnpublished);
	$isEqual = $fd->isEqual($currentData, $productForm);
	if (!$isEqual) {
		$this->markFieldDirty("productForm", true);
	}
	$this->productForm = $fd->preSetData($this, $productForm);
	return $this;
}

/**
* Get weight - Vekt
* @return float|null
*/
public function getWeight () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("weight"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->weight;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set weight - Vekt
* @param float|null $weight
* @return \Pimcore\Model\DataObject\Product
*/
public function setWeight ($weight) {
	$fd = $this->getClass()->getFieldDefinition("weight");
	$this->weight = $fd->preSetData($this, $weight);
	return $this;
}

/**
* Get height - Høyde
* @return float|null
*/
public function getHeight () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("height"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->height;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set height - Høyde
* @param float|null $height
* @return \Pimcore\Model\DataObject\Product
*/
public function setHeight ($height) {
	$fd = $this->getClass()->getFieldDefinition("height");
	$this->height = $fd->preSetData($this, $height);
	return $this;
}

/**
* Get width - Bredde
* @return float|null
*/
public function getWidth () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("width"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->width;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set width - Bredde
* @param float|null $width
* @return \Pimcore\Model\DataObject\Product
*/
public function setWidth ($width) {
	$fd = $this->getClass()->getFieldDefinition("width");
	$this->width = $fd->preSetData($this, $width);
	return $this;
}

/**
* Get depth - Dybde
* @return float|null
*/
public function getDepth () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("depth"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->depth;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set depth - Dybde
* @param float|null $depth
* @return \Pimcore\Model\DataObject\Product
*/
public function setDepth ($depth) {
	$fd = $this->getClass()->getFieldDefinition("depth");
	$this->depth = $fd->preSetData($this, $depth);
	return $this;
}

/**
* Get pageCount - Sidetall
* @return string|null
*/
public function getPageCount () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("pageCount"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->pageCount;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set pageCount - Sidetall
* @param string|null $pageCount
* @return \Pimcore\Model\DataObject\Product
*/
public function setPageCount ($pageCount) {
	$fd = $this->getClass()->getFieldDefinition("pageCount");
	$this->pageCount = $pageCount;
	return $this;
}

/**
* Get duration - Spilletid
* @return string|null
*/
public function getDuration () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("duration"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->duration;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set duration - Spilletid
* @param string|null $duration
* @return \Pimcore\Model\DataObject\Product
*/
public function setDuration ($duration) {
	$fd = $this->getClass()->getFieldDefinition("duration");
	$this->duration = $duration;
	return $this;
}

/**
* Get filesize - Filstørrelse
* @return string|null
*/
public function getFilesize () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("filesize"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->filesize;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set filesize - Filstørrelse
* @param string|null $filesize
* @return \Pimcore\Model\DataObject\Product
*/
public function setFilesize ($filesize) {
	$fd = $this->getClass()->getFieldDefinition("filesize");
	$this->filesize = $filesize;
	return $this;
}

/**
* Get seriesTitle - Serietittel
* @return string|null
*/
public function getSeriesTitle () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("seriesTitle"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->seriesTitle;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set seriesTitle - Serietittel
* @param string|null $seriesTitle
* @return \Pimcore\Model\DataObject\Product
*/
public function setSeriesTitle ($seriesTitle) {
	$fd = $this->getClass()->getFieldDefinition("seriesTitle");
	$this->seriesTitle = $seriesTitle;
	return $this;
}

/**
* Get seriesNumber - Serienummer
* @return string|null
*/
public function getSeriesNumber () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("seriesNumber"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->seriesNumber;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set seriesNumber - Serienummer
* @param string|null $seriesNumber
* @return \Pimcore\Model\DataObject\Product
*/
public function setSeriesNumber ($seriesNumber) {
	$fd = $this->getClass()->getFieldDefinition("seriesNumber");
	$this->seriesNumber = $seriesNumber;
	return $this;
}

/**
* @return \Pimcore\Model\DataObject\Fieldcollection|null
*/
public function getCreator () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("creator"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->getClass()->getFieldDefinition("creator")->preGetData($this);
	 return $data;
}

/**
* Set creator - Ansvarshaver
* @param \Pimcore\Model\DataObject\Fieldcollection|null $creator
* @return \Pimcore\Model\DataObject\Product
*/
public function setCreator ($creator) {
	$fd = $this->getClass()->getFieldDefinition("creator");
	$this->creator = $fd->preSetData($this, $creator);
	return $this;
}

/**
* Get sdCreatorInit - Ansvarshaver (opprinnelig)
* @return string|null
*/
public function getSdCreatorInit () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("sdCreatorInit"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->sdCreatorInit;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set sdCreatorInit - Ansvarshaver (opprinnelig)
* @param string|null $sdCreatorInit
* @return \Pimcore\Model\DataObject\Product
*/
public function setSdCreatorInit ($sdCreatorInit) {
	$fd = $this->getClass()->getFieldDefinition("sdCreatorInit");
	$this->sdCreatorInit = $sdCreatorInit;
	return $this;
}

/**
* Get sdCreatorUpdate - Ansvarshaver (oppdatert)
* @return string|null
*/
public function getSdCreatorUpdate () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("sdCreatorUpdate"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->sdCreatorUpdate;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set sdCreatorUpdate - Ansvarshaver (oppdatert)
* @param string|null $sdCreatorUpdate
* @return \Pimcore\Model\DataObject\Product
*/
public function setSdCreatorUpdate ($sdCreatorUpdate) {
	$fd = $this->getClass()->getFieldDefinition("sdCreatorUpdate");
	$this->sdCreatorUpdate = $sdCreatorUpdate;
	return $this;
}

/**
* Get imprint - Imprint
* @return string|null
*/
public function getImprint () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("imprint"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->imprint;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set imprint - Imprint
* @param string|null $imprint
* @return \Pimcore\Model\DataObject\Product
*/
public function setImprint ($imprint) {
	$fd = $this->getClass()->getFieldDefinition("imprint");
	$this->imprint = $imprint;
	return $this;
}

/**
* Get place - Sted
* @return string|null
*/
public function getPlace () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("place"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->place;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set place - Sted
* @param string|null $place
* @return \Pimcore\Model\DataObject\Product
*/
public function setPlace ($place) {
	$fd = $this->getClass()->getFieldDefinition("place");
	$this->place = $place;
	return $this;
}

/**
* Get publisher - Forlag
* @return string|null
*/
public function getPublisher () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("publisher"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->publisher;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set publisher - Forlag
* @param string|null $publisher
* @return \Pimcore\Model\DataObject\Product
*/
public function setPublisher ($publisher) {
	$fd = $this->getClass()->getFieldDefinition("publisher");
	$this->publisher = $publisher;
	return $this;
}

/**
* Get publisherCode - Forlagskode
* @return string|null
*/
public function getPublisherCode () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("publisherCode"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->publisherCode;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set publisherCode - Forlagskode
* @param string|null $publisherCode
* @return \Pimcore\Model\DataObject\Product
*/
public function setPublisherCode ($publisherCode) {
	$fd = $this->getClass()->getFieldDefinition("publisherCode");
	$this->publisherCode = $publisherCode;
	return $this;
}

/**
* Get publisherCategory - Forlagets kategori
* @return string|null
*/
public function getPublisherCategory () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("publisherCategory"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->publisherCategory;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set publisherCategory - Forlagets kategori
* @param string|null $publisherCategory
* @return \Pimcore\Model\DataObject\Product
*/
public function setPublisherCategory ($publisherCategory) {
	$fd = $this->getClass()->getFieldDefinition("publisherCategory");
	$this->publisherCategory = $publisherCategory;
	return $this;
}

/**
* Get forlagenesBokgruppe - Forlagenes bokgruppe
* @return \Pimcore\Model\DataObject\ForlagenesBokgruppe|null
*/
public function getForlagenesBokgruppe () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("forlagenesBokgruppe"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->getClass()->getFieldDefinition("forlagenesBokgruppe")->preGetData($this);

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set forlagenesBokgruppe - Forlagenes bokgruppe
* @param \Pimcore\Model\DataObject\ForlagenesBokgruppe $forlagenesBokgruppe
* @return \Pimcore\Model\DataObject\Product
*/
public function setForlagenesBokgruppe ($forlagenesBokgruppe) {
	$fd = $this->getClass()->getFieldDefinition("forlagenesBokgruppe");
	$hideUnpublished = \Pimcore\Model\DataObject\Concrete::getHideUnpublished();
	\Pimcore\Model\DataObject\Concrete::setHideUnpublished(false);
	$currentData = $this->getForlagenesBokgruppe();
	\Pimcore\Model\DataObject\Concrete::setHideUnpublished($hideUnpublished);
	$isEqual = $fd->isEqual($currentData, $forlagenesBokgruppe);
	if (!$isEqual) {
		$this->markFieldDirty("forlagenesBokgruppe", true);
	}
	$this->forlagenesBokgruppe = $fd->preSetData($this, $forlagenesBokgruppe);
	return $this;
}

/**
* Get bokhandelensVaregruppe - Bokhandelens varegruppe
* @return \Pimcore\Model\DataObject\BokhandelensVaregruppe|null
*/
public function getBokhandelensVaregruppe () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("bokhandelensVaregruppe"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->getClass()->getFieldDefinition("bokhandelensVaregruppe")->preGetData($this);

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set bokhandelensVaregruppe - Bokhandelens varegruppe
* @param \Pimcore\Model\DataObject\BokhandelensVaregruppe $bokhandelensVaregruppe
* @return \Pimcore\Model\DataObject\Product
*/
public function setBokhandelensVaregruppe ($bokhandelensVaregruppe) {
	$fd = $this->getClass()->getFieldDefinition("bokhandelensVaregruppe");
	$hideUnpublished = \Pimcore\Model\DataObject\Concrete::getHideUnpublished();
	\Pimcore\Model\DataObject\Concrete::setHideUnpublished(false);
	$currentData = $this->getBokhandelensVaregruppe();
	\Pimcore\Model\DataObject\Concrete::setHideUnpublished($hideUnpublished);
	$isEqual = $fd->isEqual($currentData, $bokhandelensVaregruppe);
	if (!$isEqual) {
		$this->markFieldDirty("bokhandelensVaregruppe", true);
	}
	$this->bokhandelensVaregruppe = $fd->preSetData($this, $bokhandelensVaregruppe);
	return $this;
}

/**
* @return \Pimcore\Model\DataObject\Fieldcollection|null
*/
public function getGenres () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("genres"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->getClass()->getFieldDefinition("genres")->preGetData($this);
	 return $data;
}

/**
* Set genres - Sjangre
* @param \Pimcore\Model\DataObject\Fieldcollection|null $genres
* @return \Pimcore\Model\DataObject\Product
*/
public function setGenres ($genres) {
	$fd = $this->getClass()->getFieldDefinition("genres");
	$this->genres = $fd->preSetData($this, $genres);
	return $this;
}

/**
* @return \Pimcore\Model\DataObject\Fieldcollection|null
*/
public function getSubjects () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("subjects"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->getClass()->getFieldDefinition("subjects")->preGetData($this);
	 return $data;
}

/**
* Set subjects - Emner
* @param \Pimcore\Model\DataObject\Fieldcollection|null $subjects
* @return \Pimcore\Model\DataObject\Product
*/
public function setSubjects ($subjects) {
	$fd = $this->getClass()->getFieldDefinition("subjects");
	$this->subjects = $fd->preSetData($this, $subjects);
	return $this;
}

/**
* Get notes - Noter
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getNotes () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("notes"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->getClass()->getFieldDefinition("notes")->preGetData($this);

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set notes - Noter
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $notes
* @return \Pimcore\Model\DataObject\Product
*/
public function setNotes ($notes) {
	$fd = $this->getClass()->getFieldDefinition("notes");
	$this->notes = $fd->preSetData($this, $notes);
	return $this;
}

/**
* Get exportedToPromus - Eksportert til Promus
* @return bool|null
*/
public function getExportedToPromus () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("exportedToPromus"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->exportedToPromus;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set exportedToPromus - Eksportert til Promus
* @param bool|null $exportedToPromus
* @return \Pimcore\Model\DataObject\Product
*/
public function setExportedToPromus ($exportedToPromus) {
	$fd = $this->getClass()->getFieldDefinition("exportedToPromus");
	$this->exportedToPromus = $exportedToPromus;
	return $this;
}

/**
* Get exportedToPromusVersion - Versjon eksportert til Promus
* @return string|null
*/
public function getExportedToPromusVersion () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("exportedToPromusVersion"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->exportedToPromusVersion;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set exportedToPromusVersion - Versjon eksportert til Promus
* @param string|null $exportedToPromusVersion
* @return \Pimcore\Model\DataObject\Product
*/
public function setExportedToPromusVersion ($exportedToPromusVersion) {
	$fd = $this->getClass()->getFieldDefinition("exportedToPromusVersion");
	$this->exportedToPromusVersion = $exportedToPromusVersion;
	return $this;
}

/**
* Get messageCode - Meldingskode
* @return string|null
*/
public function getMessageCode () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("messageCode"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->messageCode;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set messageCode - Meldingskode
* @param string|null $messageCode
* @return \Pimcore\Model\DataObject\Product
*/
public function setMessageCode ($messageCode) {
	$fd = $this->getClass()->getFieldDefinition("messageCode");
	$this->messageCode = $messageCode;
	return $this;
}

/**
* Get messageText - Meldingstekst
* @return string|null
*/
public function getMessageText () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("messageText"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->messageText;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set messageText - Meldingstekst
* @param string|null $messageText
* @return \Pimcore\Model\DataObject\Product
*/
public function setMessageText ($messageText) {
	$fd = $this->getClass()->getFieldDefinition("messageText");
	$this->messageText = $messageText;
	return $this;
}

/**
* Get messageDate - Meldingsdato
* @return string|null
*/
public function getMessageDate () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("messageDate"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->messageDate;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set messageDate - Meldingsdato
* @param string|null $messageDate
* @return \Pimcore\Model\DataObject\Product
*/
public function setMessageDate ($messageDate) {
	$fd = $this->getClass()->getFieldDefinition("messageDate");
	$this->messageDate = $messageDate;
	return $this;
}

/**
* Get sdSeriesNumber - Serienummer (SD)
* @return string|null
*/
public function getSdSeriesNumber () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("sdSeriesNumber"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->sdSeriesNumber;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set sdSeriesNumber - Serienummer (SD)
* @param string|null $sdSeriesNumber
* @return \Pimcore\Model\DataObject\Product
*/
public function setSdSeriesNumber ($sdSeriesNumber) {
	$fd = $this->getClass()->getFieldDefinition("sdSeriesNumber");
	$this->sdSeriesNumber = $sdSeriesNumber;
	return $this;
}

/**
* Get sdLastUpdate - Siste oppdatering fra SD
* @return \Carbon\Carbon|null
*/
public function getSdLastUpdate () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("sdLastUpdate"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->sdLastUpdate;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set sdLastUpdate - Siste oppdatering fra SD
* @param \Carbon\Carbon|null $sdLastUpdate
* @return \Pimcore\Model\DataObject\Product
*/
public function setSdLastUpdate ($sdLastUpdate) {
	$fd = $this->getClass()->getFieldDefinition("sdLastUpdate");
	$this->sdLastUpdate = $sdLastUpdate;
	return $this;
}

}

