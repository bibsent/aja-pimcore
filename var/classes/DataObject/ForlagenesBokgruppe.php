<?php 

/** 
* Inheritance: no
* Variants: no


Fields Summary: 
- bookgroup [input]
- descriptionPublisher [input]
- descriptionBS [input]
- parentBookgroup [manyToOneRelation]
*/ 

namespace Pimcore\Model\DataObject;

use Pimcore\Model\DataObject\Exception\InheritanceParentNotFoundException;
use Pimcore\Model\DataObject\PreGetValueHookInterface;

/**
* @method static \Pimcore\Model\DataObject\ForlagenesBokgruppe\Listing|\Pimcore\Model\DataObject\ForlagenesBokgruppe getByBookgroup ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\ForlagenesBokgruppe\Listing|\Pimcore\Model\DataObject\ForlagenesBokgruppe getByDescriptionPublisher ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\ForlagenesBokgruppe\Listing|\Pimcore\Model\DataObject\ForlagenesBokgruppe getByDescriptionBS ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\ForlagenesBokgruppe\Listing|\Pimcore\Model\DataObject\ForlagenesBokgruppe getByParentBookgroup ($value, $limit = 0, $offset = 0) 
*/

class ForlagenesBokgruppe extends Concrete {

protected $o_classId = "FBG";
protected $o_className = "ForlagenesBokgruppe";
protected $bookgroup;
protected $descriptionPublisher;
protected $descriptionBS;
protected $parentBookgroup;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\ForlagenesBokgruppe
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get bookgroup - Bokgruppe
* @return string|null
*/
public function getBookgroup () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("bookgroup"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->bookgroup;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set bookgroup - Bokgruppe
* @param string|null $bookgroup
* @return \Pimcore\Model\DataObject\ForlagenesBokgruppe
*/
public function setBookgroup ($bookgroup) {
	$fd = $this->getClass()->getFieldDefinition("bookgroup");
	$this->bookgroup = $bookgroup;
	return $this;
}

/**
* Get descriptionPublisher - Beskrivelse
* @return string|null
*/
public function getDescriptionPublisher () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("descriptionPublisher"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->descriptionPublisher;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set descriptionPublisher - Beskrivelse
* @param string|null $descriptionPublisher
* @return \Pimcore\Model\DataObject\ForlagenesBokgruppe
*/
public function setDescriptionPublisher ($descriptionPublisher) {
	$fd = $this->getClass()->getFieldDefinition("descriptionPublisher");
	$this->descriptionPublisher = $descriptionPublisher;
	return $this;
}

/**
* Get descriptionBS - Beskrivelse (BS)
* @return string|null
*/
public function getDescriptionBS () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("descriptionBS"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->descriptionBS;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set descriptionBS - Beskrivelse (BS)
* @param string|null $descriptionBS
* @return \Pimcore\Model\DataObject\ForlagenesBokgruppe
*/
public function setDescriptionBS ($descriptionBS) {
	$fd = $this->getClass()->getFieldDefinition("descriptionBS");
	$this->descriptionBS = $descriptionBS;
	return $this;
}

/**
* Get parentBookgroup - Parent ID
* @return \Pimcore\Model\DataObject\ForlagenesBokgruppe|null
*/
public function getParentBookgroup () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("parentBookgroup"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->getClass()->getFieldDefinition("parentBookgroup")->preGetData($this);

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set parentBookgroup - Parent ID
* @param \Pimcore\Model\DataObject\ForlagenesBokgruppe $parentBookgroup
* @return \Pimcore\Model\DataObject\ForlagenesBokgruppe
*/
public function setParentBookgroup ($parentBookgroup) {
	$fd = $this->getClass()->getFieldDefinition("parentBookgroup");
	$hideUnpublished = \Pimcore\Model\DataObject\Concrete::getHideUnpublished();
	\Pimcore\Model\DataObject\Concrete::setHideUnpublished(false);
	$currentData = $this->getParentBookgroup();
	\Pimcore\Model\DataObject\Concrete::setHideUnpublished($hideUnpublished);
	$isEqual = $fd->isEqual($currentData, $parentBookgroup);
	if (!$isEqual) {
		$this->markFieldDirty("parentBookgroup", true);
	}
	$this->parentBookgroup = $fd->preSetData($this, $parentBookgroup);
	return $this;
}

}

