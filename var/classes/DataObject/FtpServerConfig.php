<?php 

/** 
* Inheritance: no
* Variants: no


Fields Summary: 
- serverType [select]
- host [input]
- port [input]
- ssl [checkbox]
- username [input]
- password [encryptedField]
- remotePaths [textarea]
- filenameFilter [textarea]
- deleteAfterDownload [booleanSelect]
*/ 

namespace Pimcore\Model\DataObject;

use Pimcore\Model\DataObject\Exception\InheritanceParentNotFoundException;
use Pimcore\Model\DataObject\PreGetValueHookInterface;

/**
* @method static \Pimcore\Model\DataObject\FtpServerConfig\Listing|\Pimcore\Model\DataObject\FtpServerConfig getByServerType ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\FtpServerConfig\Listing|\Pimcore\Model\DataObject\FtpServerConfig getByHost ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\FtpServerConfig\Listing|\Pimcore\Model\DataObject\FtpServerConfig getByPort ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\FtpServerConfig\Listing|\Pimcore\Model\DataObject\FtpServerConfig getBySsl ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\FtpServerConfig\Listing|\Pimcore\Model\DataObject\FtpServerConfig getByUsername ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\FtpServerConfig\Listing|\Pimcore\Model\DataObject\FtpServerConfig getByRemotePaths ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\FtpServerConfig\Listing|\Pimcore\Model\DataObject\FtpServerConfig getByFilenameFilter ($value, $limit = 0, $offset = 0) 
*/

class FtpServerConfig extends Concrete {

protected $o_classId = "FTP";
protected $o_className = "FtpServerConfig";
protected $serverType;
protected $host;
protected $port;
protected $ssl;
protected $username;
protected $password;
protected $remotePaths;
protected $filenameFilter;
protected $deleteAfterDownload;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\FtpServerConfig
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get serverType - Type
* @return string|null
*/
public function getServerType () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("serverType"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->serverType;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set serverType - Type
* @param string|null $serverType
* @return \Pimcore\Model\DataObject\FtpServerConfig
*/
public function setServerType ($serverType) {
	$fd = $this->getClass()->getFieldDefinition("serverType");
	$this->serverType = $serverType;
	return $this;
}

/**
* Get host - Server
* @return string|null
*/
public function getHost () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("host"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->host;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set host - Server
* @param string|null $host
* @return \Pimcore\Model\DataObject\FtpServerConfig
*/
public function setHost ($host) {
	$fd = $this->getClass()->getFieldDefinition("host");
	$this->host = $host;
	return $this;
}

/**
* Get port - Port
* @return string|null
*/
public function getPort () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("port"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->port;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set port - Port
* @param string|null $port
* @return \Pimcore\Model\DataObject\FtpServerConfig
*/
public function setPort ($port) {
	$fd = $this->getClass()->getFieldDefinition("port");
	$this->port = $port;
	return $this;
}

/**
* Get ssl - SSL
* @return bool|null
*/
public function getSsl () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("ssl"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->ssl;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set ssl - SSL
* @param bool|null $ssl
* @return \Pimcore\Model\DataObject\FtpServerConfig
*/
public function setSsl ($ssl) {
	$fd = $this->getClass()->getFieldDefinition("ssl");
	$this->ssl = $ssl;
	return $this;
}

/**
* Get username - Brukernavn
* @return string|null
*/
public function getUsername () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("username"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->username;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set username - Brukernavn
* @param string|null $username
* @return \Pimcore\Model\DataObject\FtpServerConfig
*/
public function setUsername ($username) {
	$fd = $this->getClass()->getFieldDefinition("username");
	$this->username = $username;
	return $this;
}

/**
* Get password - Passord
* @return 
*/
public function getPassword () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("password"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->password;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set password - Passord
* @param  $password
* @return \Pimcore\Model\DataObject\FtpServerConfig
*/
public function setPassword ($password) {
	$fd = $this->getClass()->getFieldDefinition("password");
	$encryptedFd = $this->getClass()->getFieldDefinition("password");
	$delegate = $encryptedFd->getDelegate();
	if ($delegate && !($password instanceof \Pimcore\Model\DataObject\Data\EncryptedField)) {
		$password = new \Pimcore\Model\DataObject\Data\EncryptedField($delegate, $password);
	}
	$this->password = $password;
	return $this;
}

/**
* Get remotePaths - Stier til filer som skal hentes
* @return string|null
*/
public function getRemotePaths () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("remotePaths"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->remotePaths;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set remotePaths - Stier til filer som skal hentes
* @param string|null $remotePaths
* @return \Pimcore\Model\DataObject\FtpServerConfig
*/
public function setRemotePaths ($remotePaths) {
	$fd = $this->getClass()->getFieldDefinition("remotePaths");
	$this->remotePaths = $remotePaths;
	return $this;
}

/**
* Get filenameFilter - Filnavn-filter:
* @return string|null
*/
public function getFilenameFilter () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("filenameFilter"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->filenameFilter;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set filenameFilter - Filnavn-filter:
* @param string|null $filenameFilter
* @return \Pimcore\Model\DataObject\FtpServerConfig
*/
public function setFilenameFilter ($filenameFilter) {
	$fd = $this->getClass()->getFieldDefinition("filenameFilter");
	$this->filenameFilter = $filenameFilter;
	return $this;
}

/**
* Get deleteAfterDownload - Slett filer fra server etter nedlasting?
* @return bool|null
*/
public function getDeleteAfterDownload () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("deleteAfterDownload"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->deleteAfterDownload;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set deleteAfterDownload - Slett filer fra server etter nedlasting?
* @param bool|null $deleteAfterDownload
* @return \Pimcore\Model\DataObject\FtpServerConfig
*/
public function setDeleteAfterDownload ($deleteAfterDownload) {
	$fd = $this->getClass()->getFieldDefinition("deleteAfterDownload");
	$this->deleteAfterDownload = $deleteAfterDownload;
	return $this;
}

}

