<?php 

/** 
Fields Summary: 
- fdsfdsf [input]
- fdsfsdf [input]
*/ 

namespace Pimcore\Model\DataObject\Objectbrick\Data;

use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\Exception\InheritanceParentNotFoundException;
use Pimcore\Model\DataObject\PreGetValueHookInterface;


class Afd extends DataObject\Objectbrick\Data\AbstractData {

protected $type = "afd";
protected $fdsfdsf;
protected $fdsfsdf;


/**
* Afd constructor.
* @param DataObject\Concrete $object
*/
public function __construct(DataObject\Concrete $object) {
	parent::__construct($object);
	$this->markFieldDirty("_self");
}


/**
* Get fdsfdsf - fdsfdsf
* @return string|null
*/
public function getFdsfdsf () {
	$data = $this->fdsfdsf;
	if(\Pimcore\Model\DataObject::doGetInheritedValues($this->getObject()) && $this->getDefinition()->getFieldDefinition("fdsfdsf")->isEmpty($data)) {
		try {
			return $this->getValueFromParent("fdsfdsf");
		} catch (InheritanceParentNotFoundException $e) {
			// no data from parent available, continue ... 
		}
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		return $data->getPlain();
	}
	 return $data;
}

/**
* Set fdsfdsf - fdsfdsf
* @param string|null $fdsfdsf
* @return \Pimcore\Model\DataObject\Objectbrick\Data\Afd
*/
public function setFdsfdsf ($fdsfdsf) {
	$fd = $this->getDefinition()->getFieldDefinition("fdsfdsf");
	$this->fdsfdsf = $fdsfdsf;
	return $this;
}

/**
* Get fdsfsdf - fdsfsdf
* @return string|null
*/
public function getFdsfsdf () {
	$data = $this->fdsfsdf;
	if(\Pimcore\Model\DataObject::doGetInheritedValues($this->getObject()) && $this->getDefinition()->getFieldDefinition("fdsfsdf")->isEmpty($data)) {
		try {
			return $this->getValueFromParent("fdsfsdf");
		} catch (InheritanceParentNotFoundException $e) {
			// no data from parent available, continue ... 
		}
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		return $data->getPlain();
	}
	 return $data;
}

/**
* Set fdsfsdf - fdsfsdf
* @param string|null $fdsfsdf
* @return \Pimcore\Model\DataObject\Objectbrick\Data\Afd
*/
public function setFdsfsdf ($fdsfsdf) {
	$fd = $this->getDefinition()->getFieldDefinition("fdsfsdf");
	$this->fdsfsdf = $fdsfsdf;
	return $this;
}

}

