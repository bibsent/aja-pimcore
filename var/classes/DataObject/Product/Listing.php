<?php 

namespace Pimcore\Model\DataObject\Product;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Product current()
 * @method DataObject\Product[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "PRD";
protected $className = "Product";


/**
* Filter by uuid (Uuid)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByUuid ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("uuid")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by productId (Vare-ID)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByProductId ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("productId")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by isbn (ISBN)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByIsbn ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("isbn")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by originalSource (Originalkilde)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByOriginalSource ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("originalSource")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by title (Tittel)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByTitle ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("title")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by publisherSummary (Forlagets omtale)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByPublisherSummary ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("publisherSummary")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by originalTitle (Originaltittel)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByOriginalTitle ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("originalTitle")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by subTitle (Undertittel)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterBySubTitle ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("subTitle")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by documentType (Dokumenttype)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByDocumentType ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("documentType")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by literatureType (Litteraturtype)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByLiteratureType ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("literatureType")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by targetAudience (Målgruppe)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByTargetAudience ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("targetAudience")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by ageGroup (Aldersgrupper)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByAgeGroup ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("ageGroup")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by binding (Innbinding)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByBinding ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("binding")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by edition (Utgave)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByEdition ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("edition")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by extent (Omfang)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByExtent ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("extent")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by biographicalContent (Biografisk innhold)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByBiographicalContent ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("biographicalContent")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by price (Pris)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByPrice ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("price")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by publicationYear (Utgivelsesår)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByPublicationYear ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("publicationYear")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by publicationDate (Utgivelsesdato)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByPublicationDate ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("publicationDate")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by publicationDateString (Utgivelsesdato (streng))
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByPublicationDateString ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("publicationDateString")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by publicationCountry (Utgivelsesland)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByPublicationCountry ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("publicationCountry")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by lang (Språk)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByLang ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("lang")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by fileFormat (Fileformat)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByFileFormat ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("fileFormat")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by productForm (Vareformat)
* @param Element\ElementInterface|array $data   comparison element or ['id' => <element ID>, 'type' => <element type>]
* @param string $operator  SQL comparison operator, currently only "=" possible
* @return static
*/
public function filterByProductForm ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("productForm")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by weight (Vekt)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByWeight ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("weight")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by height (Høyde)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByHeight ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("height")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by width (Bredde)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByWidth ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("width")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by depth (Dybde)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByDepth ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("depth")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by pageCount (Sidetall)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByPageCount ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("pageCount")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by duration (Spilletid)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByDuration ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("duration")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by filesize (Filstørrelse)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByFilesize ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("filesize")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by seriesTitle (Serietittel)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterBySeriesTitle ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("seriesTitle")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by seriesNumber (Serienummer)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterBySeriesNumber ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("seriesNumber")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by sdCreatorInit (Ansvarshaver (opprinnelig))
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterBySdCreatorInit ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("sdCreatorInit")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by sdCreatorUpdate (Ansvarshaver (oppdatert))
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterBySdCreatorUpdate ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("sdCreatorUpdate")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by imprint (Imprint)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByImprint ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("imprint")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by place (Sted)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByPlace ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("place")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by publisher (Forlag)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByPublisher ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("publisher")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by publisherCode (Forlagskode)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByPublisherCode ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("publisherCode")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by publisherCategory (Forlagets kategori)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByPublisherCategory ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("publisherCategory")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by forlagenesBokgruppe (Forlagenes bokgruppe)
* @param Element\ElementInterface|array $data   comparison element or ['id' => <element ID>, 'type' => <element type>]
* @param string $operator  SQL comparison operator, currently only "=" possible
* @return static
*/
public function filterByForlagenesBokgruppe ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("forlagenesBokgruppe")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by bokhandelensVaregruppe (Bokhandelens varegruppe)
* @param Element\ElementInterface|array $data   comparison element or ['id' => <element ID>, 'type' => <element type>]
* @param string $operator  SQL comparison operator, currently only "=" possible
* @return static
*/
public function filterByBokhandelensVaregruppe ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("bokhandelensVaregruppe")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by exportedToPromus (Eksportert til Promus)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByExportedToPromus ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("exportedToPromus")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by exportedToPromusVersion (Versjon eksportert til Promus)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByExportedToPromusVersion ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("exportedToPromusVersion")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by messageCode (Meldingskode)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByMessageCode ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("messageCode")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by messageText (Meldingstekst)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByMessageText ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("messageText")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by messageDate (Meldingsdato)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByMessageDate ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("messageDate")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by sdSeriesNumber (Serienummer (SD))
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterBySdSeriesNumber ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("sdSeriesNumber")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by sdLastUpdate (Siste oppdatering fra SD)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterBySdLastUpdate ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("sdLastUpdate")->addListingFilter($this, $data, $operator);
	return $this;
}



}
