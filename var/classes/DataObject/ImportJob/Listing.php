<?php 

namespace Pimcore\Model\DataObject\ImportJob;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\ImportJob current()
 * @method DataObject\ImportJob[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "IMP";
protected $className = "ImportJob";


/**
* Filter by uuid (uuid)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByUuid ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("uuid")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by integration (Integrasjon)
* @param Element\ElementInterface|array $data   comparison element or ['id' => <element ID>, 'type' => <element type>]
* @param string $operator  SQL comparison operator, currently only "=" possible
* @return static
*/
public function filterByIntegration ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("integration")->addListingFilter($this, $data, $operator);
	return $this;
}



}
