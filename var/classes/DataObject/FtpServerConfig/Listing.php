<?php 

namespace Pimcore\Model\DataObject\FtpServerConfig;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\FtpServerConfig current()
 * @method DataObject\FtpServerConfig[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "FTP";
protected $className = "FtpServerConfig";


/**
* Filter by serverType (Type)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByServerType ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("serverType")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by host (Server)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByHost ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("host")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by port (Port)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByPort ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("port")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by ssl (SSL)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterBySsl ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("ssl")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by username (Brukernavn)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByUsername ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("username")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by remotePaths (Stier til filer som skal hentes)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByRemotePaths ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("remotePaths")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by filenameFilter (Filnavn-filter:)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByFilenameFilter ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("filenameFilter")->addListingFilter($this, $data, $operator);
	return $this;
}



}
