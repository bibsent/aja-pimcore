<?php 

namespace Pimcore\Model\DataObject\BokhandelensVaregruppe;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BokhandelensVaregruppe current()
 * @method DataObject\BokhandelensVaregruppe[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "BVG";
protected $className = "BokhandelensVaregruppe";


/**
* Filter by code (Kode)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByCode ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("code")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by name (Navn)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByName ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("name")->addListingFilter($this, $data, $operator);
	return $this;
}



}
