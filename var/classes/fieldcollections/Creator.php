<?php 

/** 
Fields Summary: 
- creatorName [input]
- creatorType [select]
- creatorRole [multiselect]
- bibbiId [input]
- norafId [input]
*/ 


return Pimcore\Model\DataObject\Fieldcollection\Definition::__set_state(array(
   'dao' => NULL,
   'key' => 'Creator',
   'parentClass' => '',
   'implementsInterfaces' => '',
   'title' => '',
   'group' => '',
   'layoutDefinitions' => 
  Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
     'fieldtype' => 'panel',
     'labelWidth' => 100,
     'layout' => NULL,
     'border' => false,
     'name' => NULL,
     'type' => NULL,
     'region' => NULL,
     'title' => NULL,
     'width' => NULL,
     'height' => NULL,
     'collapsible' => false,
     'collapsed' => false,
     'bodyStyle' => NULL,
     'datatype' => 'layout',
     'permissions' => NULL,
     'childs' => 
    array (
      0 => 
      Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
         'fieldtype' => 'panel',
         'labelWidth' => 100,
         'layout' => NULL,
         'border' => false,
         'name' => 'Layout',
         'type' => NULL,
         'region' => NULL,
         'title' => '',
         'width' => NULL,
         'height' => NULL,
         'collapsible' => false,
         'collapsed' => false,
         'bodyStyle' => '',
         'datatype' => 'layout',
         'permissions' => NULL,
         'childs' => 
        array (
          0 => 
          Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
             'fieldtype' => 'input',
             'width' => NULL,
             'defaultValue' => NULL,
             'columnLength' => 190,
             'phpdocType' => 'string',
             'regex' => '',
             'unique' => false,
             'showCharCount' => false,
             'name' => 'creatorName',
             'title' => 'Navn',
             'tooltip' => '',
             'mandatory' => false,
             'noteditable' => false,
             'index' => false,
             'locked' => false,
             'style' => '',
             'permissions' => NULL,
             'datatype' => 'data',
             'relationType' => false,
             'invisible' => false,
             'visibleGridView' => false,
             'visibleSearch' => false,
             'blockedVarsForExport' => 
            array (
            ),
             'defaultValueGenerator' => '',
          )),
          1 => 
          Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
             'fieldtype' => 'select',
             'options' => 
            array (
              0 => 
              array (
                'key' => 'Person',
                'value' => 'Person',
              ),
              1 => 
              array (
                'key' => 'Korporasjon',
                'value' => 'Korporasjon',
              ),
              2 => 
              array (
                'key' => 'Konferanse',
                'value' => 'Konferanse',
              ),
              3 => 
              array (
                'key' => 'Standardtittel',
                'value' => 'Standardtittel',
              ),
            ),
             'width' => '',
             'defaultValue' => '',
             'optionsProviderClass' => '',
             'optionsProviderData' => '',
             'columnLength' => 190,
             'phpdocType' => 'string',
             'dynamicOptions' => false,
             'name' => 'creatorType',
             'title' => 'Type',
             'tooltip' => '',
             'mandatory' => false,
             'noteditable' => false,
             'index' => false,
             'locked' => false,
             'style' => '',
             'permissions' => NULL,
             'datatype' => 'data',
             'relationType' => false,
             'invisible' => false,
             'visibleGridView' => false,
             'visibleSearch' => false,
             'blockedVarsForExport' => 
            array (
            ),
             'defaultValueGenerator' => '',
          )),
          2 => 
          Pimcore\Model\DataObject\ClassDefinition\Data\Multiselect::__set_state(array(
             'fieldtype' => 'multiselect',
             'options' => 
            array (
              0 => 
              array (
                'key' => 'Arrangør',
                'value' => 'arrangør',
              ),
              1 => 
              array (
                'key' => 'Beæret person',
                'value' => 'beæret person',
              ),
              2 => 
              array (
                'key' => 'Dirigent',
                'value' => 'dirigent',
              ),
              3 => 
              array (
                'key' => 'Forfatter',
                'value' => 'forfatter',
              ),
              4 => 
              array (
                'key' => 'Fotograf',
                'value' => 'fotograf',
              ),
              5 => 
              array (
                'key' => 'Illustratør',
                'value' => 'illustratør',
              ),
              6 => 
              array (
                'key' => 'Innleser',
                'value' => 'innleser',
              ),
              7 => 
              array (
                'key' => 'Kommentator',
                'value' => 'kommentator',
              ),
              8 => 
              array (
                'key' => 'Komponist',
                'value' => 'komponist',
              ),
              9 => 
              array (
                'key' => 'Manusforfatter',
                'value' => 'manusforfatter',
              ),
              10 => 
              array (
                'key' => 'Medarbeider',
                'value' => 'medarbeider',
              ),
              11 => 
              array (
                'key' => 'Opprinnelig forfatter',
                'value' => 'opprinnelig forfatter',
              ),
              12 => 
              array (
                'key' => 'Oversetter',
                'value' => 'oversetter',
              ),
              13 => 
              array (
                'key' => 'Redaktør',
                'value' => 'redaktør',
              ),
              14 => 
              array (
                'key' => 'Regissør',
                'value' => 'regissør',
              ),
              15 => 
              array (
                'key' => 'Skuespiller',
                'value' => 'skuespiller',
              ),
              16 => 
              array (
                'key' => 'Utgiver',
                'value' => 'utgiver',
              ),
              17 => 
              array (
                'key' => 'Utøver',
                'value' => 'utøver',
              ),
            ),
             'width' => '',
             'height' => '',
             'maxItems' => '',
             'renderType' => 'tags',
             'optionsProviderClass' => '',
             'optionsProviderData' => '',
             'phpdocType' => 'array',
             'dynamicOptions' => false,
             'name' => 'creatorRole',
             'title' => 'Rolle',
             'tooltip' => '',
             'mandatory' => false,
             'noteditable' => false,
             'index' => false,
             'locked' => false,
             'style' => '',
             'permissions' => NULL,
             'datatype' => 'data',
             'relationType' => false,
             'invisible' => false,
             'visibleGridView' => false,
             'visibleSearch' => false,
             'blockedVarsForExport' => 
            array (
            ),
          )),
          3 => 
          Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
             'fieldtype' => 'input',
             'width' => NULL,
             'defaultValue' => NULL,
             'columnLength' => 190,
             'phpdocType' => 'string',
             'regex' => '',
             'unique' => false,
             'showCharCount' => false,
             'name' => 'bibbiId',
             'title' => 'Bibbi-ID',
             'tooltip' => '',
             'mandatory' => false,
             'noteditable' => false,
             'index' => false,
             'locked' => false,
             'style' => '',
             'permissions' => NULL,
             'datatype' => 'data',
             'relationType' => false,
             'invisible' => false,
             'visibleGridView' => false,
             'visibleSearch' => false,
             'blockedVarsForExport' => 
            array (
            ),
             'defaultValueGenerator' => '',
          )),
          4 => 
          Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
             'fieldtype' => 'input',
             'width' => NULL,
             'defaultValue' => NULL,
             'columnLength' => 190,
             'phpdocType' => 'string',
             'regex' => '',
             'unique' => false,
             'showCharCount' => false,
             'name' => 'norafId',
             'title' => 'Noraf-ID',
             'tooltip' => '',
             'mandatory' => false,
             'noteditable' => false,
             'index' => false,
             'locked' => false,
             'style' => '',
             'permissions' => NULL,
             'datatype' => 'data',
             'relationType' => false,
             'invisible' => false,
             'visibleGridView' => false,
             'visibleSearch' => false,
             'blockedVarsForExport' => 
            array (
            ),
             'defaultValueGenerator' => '',
          )),
        ),
         'locked' => false,
         'blockedVarsForExport' => 
        array (
        ),
         'icon' => '',
      )),
    ),
     'locked' => false,
     'blockedVarsForExport' => 
    array (
    ),
     'icon' => NULL,
  )),
   'generateTypeDeclarations' => false,
   'blockedVarsForExport' => 
  array (
  ),
));
