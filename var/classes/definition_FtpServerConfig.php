<?php 

/** 
* Inheritance: no
* Variants: no


Fields Summary: 
- serverType [select]
- host [input]
- port [input]
- ssl [checkbox]
- username [input]
- password [encryptedField]
- remotePaths [textarea]
- filenameFilter [textarea]
- deleteAfterDownload [booleanSelect]
*/ 


return Pimcore\Model\DataObject\ClassDefinition::__set_state(array(
   'id' => 'FTP',
   'name' => 'FtpServerConfig',
   'description' => '',
   'creationDate' => 0,
   'modificationDate' => 1622574428,
   'userOwner' => 2,
   'userModification' => 2,
   'parentClass' => '',
   'implementsInterfaces' => '',
   'listingParentClass' => '',
   'useTraits' => '',
   'listingUseTraits' => '',
   'encryption' => false,
   'encryptedTables' => 
  array (
  ),
   'allowInherit' => false,
   'allowVariants' => false,
   'showVariants' => false,
   'layoutDefinitions' => 
  Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
     'fieldtype' => 'panel',
     'labelWidth' => 100,
     'layout' => NULL,
     'border' => false,
     'name' => 'pimcore_root',
     'type' => NULL,
     'region' => NULL,
     'title' => NULL,
     'width' => NULL,
     'height' => NULL,
     'collapsible' => false,
     'collapsed' => false,
     'bodyStyle' => NULL,
     'datatype' => 'layout',
     'permissions' => NULL,
     'childs' => 
    array (
      0 => 
      Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
         'fieldtype' => 'panel',
         'labelWidth' => 100,
         'layout' => NULL,
         'border' => false,
         'name' => 'Layout',
         'type' => NULL,
         'region' => NULL,
         'title' => '',
         'width' => NULL,
         'height' => NULL,
         'collapsible' => false,
         'collapsed' => false,
         'bodyStyle' => '',
         'datatype' => 'layout',
         'permissions' => NULL,
         'childs' => 
        array (
          0 => 
          Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldset::__set_state(array(
             'fieldtype' => 'fieldset',
             'labelWidth' => 100,
             'name' => 'connection',
             'type' => NULL,
             'region' => NULL,
             'title' => 'Tilkobling',
             'width' => 600,
             'height' => NULL,
             'collapsible' => false,
             'collapsed' => false,
             'bodyStyle' => '',
             'datatype' => 'layout',
             'permissions' => NULL,
             'childs' => 
            array (
              0 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'fieldtype' => 'select',
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'FTP',
                    'value' => 'FTP',
                  ),
                  1 => 
                  array (
                    'key' => 'SFTP',
                    'value' => 'SFTP',
                  ),
                ),
                 'width' => '',
                 'defaultValue' => '',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'columnLength' => 190,
                 'phpdocType' => 'string',
                 'dynamicOptions' => false,
                 'name' => 'serverType',
                 'title' => 'Type',
                 'tooltip' => '',
                 'mandatory' => true,
                 'noteditable' => false,
                 'index' => false,
                 'locked' => false,
                 'style' => 'width: 100%;',
                 'permissions' => NULL,
                 'datatype' => 'data',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'defaultValueGenerator' => '',
              )),
              1 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                 'fieldtype' => 'input',
                 'width' => NULL,
                 'defaultValue' => NULL,
                 'columnLength' => 190,
                 'phpdocType' => 'string',
                 'regex' => '',
                 'unique' => false,
                 'showCharCount' => false,
                 'name' => 'host',
                 'title' => 'Server',
                 'tooltip' => '',
                 'mandatory' => true,
                 'noteditable' => false,
                 'index' => false,
                 'locked' => false,
                 'style' => 'width: 100%;',
                 'permissions' => NULL,
                 'datatype' => 'data',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'defaultValueGenerator' => '',
              )),
              2 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                 'fieldtype' => 'input',
                 'width' => NULL,
                 'defaultValue' => '21',
                 'columnLength' => 190,
                 'phpdocType' => 'string',
                 'regex' => '',
                 'unique' => false,
                 'showCharCount' => false,
                 'name' => 'port',
                 'title' => 'Port',
                 'tooltip' => '',
                 'mandatory' => true,
                 'noteditable' => false,
                 'index' => false,
                 'locked' => false,
                 'style' => 'width: 100%;',
                 'permissions' => NULL,
                 'datatype' => 'data',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'defaultValueGenerator' => '',
              )),
              3 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Checkbox::__set_state(array(
                 'fieldtype' => 'checkbox',
                 'defaultValue' => NULL,
                 'phpdocType' => 'bool',
                 'name' => 'ssl',
                 'title' => 'SSL',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => false,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'datatype' => 'data',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'defaultValueGenerator' => '',
              )),
              4 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                 'fieldtype' => 'input',
                 'width' => NULL,
                 'defaultValue' => NULL,
                 'columnLength' => 190,
                 'phpdocType' => 'string',
                 'regex' => '',
                 'unique' => false,
                 'showCharCount' => false,
                 'name' => 'username',
                 'title' => 'Brukernavn',
                 'tooltip' => '',
                 'mandatory' => true,
                 'noteditable' => false,
                 'index' => false,
                 'locked' => false,
                 'style' => 'width: 100%;',
                 'permissions' => NULL,
                 'datatype' => 'data',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'defaultValueGenerator' => '',
              )),
              5 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\EncryptedField::__set_state(array(
                 'fieldtype' => 'encryptedField',
                 'delegateDatatype' => 'input',
                 'delegate' => 
                Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                   'fieldtype' => 'input',
                   'width' => NULL,
                   'defaultValue' => NULL,
                   'queryColumnType' => 'varchar',
                   'columnType' => 'varchar',
                   'columnLength' => 190,
                   'phpdocType' => 'string',
                   'regex' => '',
                   'unique' => NULL,
                   'showCharCount' => NULL,
                   'name' => 'password',
                   'title' => 'Passord',
                   'tooltip' => '',
                   'mandatory' => true,
                   'noteditable' => false,
                   'index' => false,
                   'locked' => false,
                   'style' => 'width: 100%',
                   'permissions' => NULL,
                   'datatype' => 'data',
                   'relationType' => false,
                   'invisible' => false,
                   'visibleGridView' => false,
                   'visibleSearch' => false,
                   'blockedVarsForExport' => 
                  array (
                  ),
                   'defaultValueGenerator' => '',
                )),
                 'phpdocType' => NULL,
                 'name' => 'password',
                 'title' => 'Passord',
                 'tooltip' => '',
                 'mandatory' => true,
                 'noteditable' => false,
                 'index' => false,
                 'locked' => false,
                 'style' => 'width: 100%',
                 'permissions' => NULL,
                 'datatype' => 'data',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
              )),
            ),
             'locked' => false,
             'blockedVarsForExport' => 
            array (
            ),
          )),
          1 => 
          Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldset::__set_state(array(
             'fieldtype' => 'fieldset',
             'labelWidth' => 100,
             'name' => 'import',
             'type' => NULL,
             'region' => NULL,
             'title' => 'Import',
             'width' => 600,
             'height' => NULL,
             'collapsible' => false,
             'collapsed' => false,
             'bodyStyle' => '',
             'datatype' => 'layout',
             'permissions' => NULL,
             'childs' => 
            array (
              0 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Textarea::__set_state(array(
                 'fieldtype' => 'textarea',
                 'width' => '',
                 'height' => '',
                 'maxLength' => NULL,
                 'showCharCount' => false,
                 'excludeFromSearchIndex' => true,
                 'phpdocType' => 'string',
                 'name' => 'remotePaths',
                 'title' => 'Stier til filer som skal hentes',
                 'tooltip' => '',
                 'mandatory' => true,
                 'noteditable' => false,
                 'index' => false,
                 'locked' => false,
                 'style' => 'width: 100%; height: 80px;',
                 'permissions' => NULL,
                 'datatype' => 'data',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
              )),
              1 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Text::__set_state(array(
                 'fieldtype' => 'text',
                 'html' => '<div><ul><li>En eller flere absolutte stier til filene som skal hentes, adskilt av linjeskift.</li><li>Stier kan inneholde jokertegn (*). Eksempel: <samp>/test/*.xml</samp> vil matche alle filer i test-mappa som slutter med <samp>.xml</samp></li><li>Stier kan inneholde datomønstre i form av malen <samp>[[date:FORMAT]]</samp>, der FORMAT er en ISO 8601-kompatibel datomal – se <a href="https://carbon.nesbot.com/docs/#iso-format-available-replacements">denne listen</a> for alle muligheter. Når jobben kjøres, vil datomønsteret utvides med dagens dato som standard, men det kan også overstyres med en annen dato hvis en har behov for å hente inn eldre filer. Eksempel: <samp>/test/cover_[[date:WW]].zip</samp> i uke 3 føre til at fila <samp>/test/cover_03.zip</samp> blir hentet.</li></ul></div>',
                 'renderingClass' => '',
                 'renderingData' => '',
                 'border' => false,
                 'name' => 'remotePathsHelp',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                ),
                 'locked' => false,
                 'blockedVarsForExport' => 
                array (
                ),
              )),
              2 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Textarea::__set_state(array(
                 'fieldtype' => 'textarea',
                 'width' => '',
                 'height' => '',
                 'maxLength' => NULL,
                 'showCharCount' => false,
                 'excludeFromSearchIndex' => true,
                 'phpdocType' => 'string',
                 'name' => 'filenameFilter',
                 'title' => 'Filnavn-filter:',
                 'tooltip' => 'Ett eller flere regulære uttrykk adskilt av linjeskift, f.eks. «^97» for å importere alle filer som begynner med «97». Hvis feltet er tomt, importeres alle filer. Linjer som begynner med # ignoreres.',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => false,
                 'locked' => false,
                 'style' => 'width: 100%; height: 120px;',
                 'permissions' => NULL,
                 'datatype' => 'data',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
              )),
              3 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\BooleanSelect::__set_state(array(
                 'fieldtype' => 'booleanSelect',
                 'yesLabel' => 'yes',
                 'noLabel' => 'no',
                 'emptyLabel' => 'empty',
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'empty',
                    'value' => 0,
                  ),
                  1 => 
                  array (
                    'key' => 'yes',
                    'value' => 1,
                  ),
                  2 => 
                  array (
                    'key' => 'no',
                    'value' => -1,
                  ),
                ),
                 'width' => '',
                 'phpdocType' => 'bool',
                 'name' => 'deleteAfterDownload',
                 'title' => 'Slett filer fra server etter nedlasting?',
                 'tooltip' => 'Skal filer slettes fra serveren etter at vi har lastet dem ned?',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => false,
                 'locked' => NULL,
                 'style' => '',
                 'permissions' => NULL,
                 'datatype' => 'data',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
              )),
            ),
             'locked' => false,
             'blockedVarsForExport' => 
            array (
            ),
          )),
        ),
         'locked' => false,
         'blockedVarsForExport' => 
        array (
        ),
         'icon' => '',
      )),
    ),
     'locked' => false,
     'blockedVarsForExport' => 
    array (
    ),
     'icon' => NULL,
  )),
   'icon' => '/bundles/pimcoreadmin/img/object-icons/04_orange.svg',
   'previewUrl' => '',
   'group' => '',
   'showAppLoggerTab' => true,
   'linkGeneratorReference' => '',
   'compositeIndices' => 
  array (
  ),
   'generateTypeDeclarations' => false,
   'showFieldLookup' => false,
   'propertyVisibility' => 
  array (
    'grid' => 
    array (
      'id' => true,
      'key' => false,
      'path' => true,
      'published' => true,
      'modificationDate' => true,
      'creationDate' => true,
    ),
    'search' => 
    array (
      'id' => true,
      'key' => false,
      'path' => true,
      'published' => true,
      'modificationDate' => true,
      'creationDate' => true,
    ),
  ),
   'enableGridLocking' => false,
   'dao' => NULL,
   'blockedVarsForExport' => 
  array (
  ),
));
