<?php 

/** 
* Inheritance: no
* Variants: no


Fields Summary: 
- username [encryptedField]
- password [encryptedField]
- filenameFilter [textarea]
*/ 


return Pimcore\Model\DataObject\ClassDefinition::__set_state(array(
   'id' => 'WSC',
   'name' => 'WebServiceConfig',
   'description' => '',
   'creationDate' => 0,
   'modificationDate' => 1622023227,
   'userOwner' => 2,
   'userModification' => 2,
   'parentClass' => '',
   'implementsInterfaces' => '',
   'listingParentClass' => '',
   'useTraits' => '',
   'listingUseTraits' => '',
   'encryption' => false,
   'encryptedTables' => 
  array (
  ),
   'allowInherit' => false,
   'allowVariants' => false,
   'showVariants' => false,
   'layoutDefinitions' => 
  Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
     'fieldtype' => 'panel',
     'labelWidth' => 100,
     'layout' => NULL,
     'border' => false,
     'name' => 'pimcore_root',
     'type' => NULL,
     'region' => NULL,
     'title' => NULL,
     'width' => NULL,
     'height' => NULL,
     'collapsible' => false,
     'collapsed' => false,
     'bodyStyle' => NULL,
     'datatype' => 'layout',
     'permissions' => NULL,
     'childs' => 
    array (
      0 => 
      Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
         'fieldtype' => 'panel',
         'labelWidth' => 100,
         'layout' => NULL,
         'border' => false,
         'name' => 'Layout',
         'type' => NULL,
         'region' => NULL,
         'title' => '',
         'width' => NULL,
         'height' => NULL,
         'collapsible' => false,
         'collapsed' => false,
         'bodyStyle' => '',
         'datatype' => 'layout',
         'permissions' => NULL,
         'childs' => 
        array (
          0 => 
          Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldset::__set_state(array(
             'fieldtype' => 'fieldset',
             'labelWidth' => 100,
             'name' => 'connection',
             'type' => NULL,
             'region' => NULL,
             'title' => 'Tilkobling',
             'width' => 600,
             'height' => NULL,
             'collapsible' => false,
             'collapsed' => false,
             'bodyStyle' => '',
             'datatype' => 'layout',
             'permissions' => NULL,
             'childs' => 
            array (
              0 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\EncryptedField::__set_state(array(
                 'fieldtype' => 'encryptedField',
                 'delegateDatatype' => 'input',
                 'delegate' => 
                Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                   'fieldtype' => 'input',
                   'width' => NULL,
                   'defaultValue' => NULL,
                   'queryColumnType' => 'varchar',
                   'columnType' => 'LONGBLOB',
                   'columnLength' => 190,
                   'phpdocType' => 'string',
                   'regex' => '',
                   'unique' => NULL,
                   'showCharCount' => NULL,
                   'name' => 'username',
                   'title' => 'Brukernavn',
                   'tooltip' => '',
                   'mandatory' => false,
                   'noteditable' => false,
                   'index' => false,
                   'locked' => false,
                   'style' => 'width: 100%;',
                   'permissions' => NULL,
                   'datatype' => 'data',
                   'relationType' => false,
                   'invisible' => false,
                   'visibleGridView' => false,
                   'visibleSearch' => false,
                   'blockedVarsForExport' => 
                  array (
                  ),
                   'defaultValueGenerator' => '',
                )),
                 'phpdocType' => NULL,
                 'name' => 'username',
                 'title' => 'Brukernavn',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => false,
                 'locked' => false,
                 'style' => 'width: 100%;',
                 'permissions' => NULL,
                 'datatype' => 'data',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
              )),
              1 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\EncryptedField::__set_state(array(
                 'fieldtype' => 'encryptedField',
                 'delegateDatatype' => 'input',
                 'delegate' => 
                Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                   'fieldtype' => 'input',
                   'width' => NULL,
                   'defaultValue' => NULL,
                   'queryColumnType' => 'varchar',
                   'columnType' => 'LONGBLOB',
                   'columnLength' => 190,
                   'phpdocType' => 'string',
                   'regex' => '',
                   'unique' => NULL,
                   'showCharCount' => false,
                   'name' => 'password',
                   'title' => 'Passord eller API-nøkkel',
                   'tooltip' => '',
                   'mandatory' => true,
                   'noteditable' => false,
                   'index' => false,
                   'locked' => false,
                   'style' => 'width: 100%;',
                   'permissions' => NULL,
                   'datatype' => 'data',
                   'relationType' => false,
                   'invisible' => false,
                   'visibleGridView' => false,
                   'visibleSearch' => false,
                   'blockedVarsForExport' => 
                  array (
                  ),
                   'defaultValueGenerator' => '',
                )),
                 'phpdocType' => NULL,
                 'name' => 'password',
                 'title' => 'Passord eller API-nøkkel',
                 'tooltip' => '',
                 'mandatory' => true,
                 'noteditable' => false,
                 'index' => false,
                 'locked' => false,
                 'style' => 'width: 100%;',
                 'permissions' => NULL,
                 'datatype' => 'data',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
              )),
            ),
             'locked' => false,
             'blockedVarsForExport' => 
            array (
            ),
          )),
          1 => 
          Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldset::__set_state(array(
             'fieldtype' => 'fieldset',
             'labelWidth' => 100,
             'name' => 'import',
             'type' => NULL,
             'region' => NULL,
             'title' => 'Import',
             'width' => 600,
             'height' => NULL,
             'collapsible' => false,
             'collapsed' => false,
             'bodyStyle' => '',
             'datatype' => 'layout',
             'permissions' => NULL,
             'childs' => 
            array (
              0 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Textarea::__set_state(array(
                 'fieldtype' => 'textarea',
                 'width' => '',
                 'height' => '',
                 'maxLength' => NULL,
                 'showCharCount' => false,
                 'excludeFromSearchIndex' => true,
                 'phpdocType' => 'string',
                 'name' => 'filenameFilter',
                 'title' => 'Filnavn-filter:',
                 'tooltip' => 'Ett eller flere regulære uttrykk adskilt av linjeskift, f.eks. «^97» for å importere alle filer som begynner med «97». Hvis feltet er tomt, importeres alle filer. Linjer som begynner med # ignoreres.',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => false,
                 'locked' => false,
                 'style' => 'width: 100%; height: 200px;',
                 'permissions' => NULL,
                 'datatype' => 'data',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
              )),
            ),
             'locked' => false,
             'blockedVarsForExport' => 
            array (
            ),
          )),
        ),
         'locked' => false,
         'blockedVarsForExport' => 
        array (
        ),
         'icon' => '',
      )),
    ),
     'locked' => false,
     'blockedVarsForExport' => 
    array (
    ),
     'icon' => NULL,
  )),
   'icon' => '/bundles/pimcoreadmin/img/object-icons/06_yellow.svg',
   'previewUrl' => '',
   'group' => '',
   'showAppLoggerTab' => false,
   'linkGeneratorReference' => '',
   'compositeIndices' => 
  array (
  ),
   'generateTypeDeclarations' => false,
   'showFieldLookup' => false,
   'propertyVisibility' => 
  array (
    'grid' => 
    array (
      'id' => true,
      'key' => false,
      'path' => true,
      'published' => true,
      'modificationDate' => true,
      'creationDate' => true,
    ),
    'search' => 
    array (
      'id' => true,
      'key' => false,
      'path' => true,
      'published' => true,
      'modificationDate' => true,
      'creationDate' => true,
    ),
  ),
   'enableGridLocking' => false,
   'dao' => NULL,
   'blockedVarsForExport' => 
  array (
  ),
));
