<?php 

return [
    "folders" => [

    ],
    "list" => [
        "products" => [
            "general" => [
                "active" => TRUE,
                "type" => "graphql",
                "name" => "products",
                "description" => "",
                "sqlObjectCondition" => "",
                "modificationDate" => 1623877276,
                "path" => NULL
            ],
            "schema" => [
                "queryEntities" => [
                    "Product" => [
                        "id" => "Product",
                        "name" => "Product",
                        "columnConfig" => [
                            "columns" => [
                                [
                                    "attributes" => [
                                        "attribute" => "isbn",
                                        "label" => "ISBN",
                                        "dataType" => "input",
                                        "layout" => [
                                            "fieldtype" => "input",
                                            "width" => NULL,
                                            "defaultValue" => NULL,
                                            "queryColumnType" => "varchar",
                                            "columnType" => "varchar",
                                            "columnLength" => 190,
                                            "phpdocType" => "string",
                                            "regex" => "",
                                            "unique" => FALSE,
                                            "showCharCount" => FALSE,
                                            "name" => "isbn",
                                            "title" => "ISBN",
                                            "tooltip" => "",
                                            "mandatory" => FALSE,
                                            "noteditable" => FALSE,
                                            "index" => FALSE,
                                            "locked" => FALSE,
                                            "style" => "",
                                            "permissions" => NULL,
                                            "datatype" => "data",
                                            "relationType" => FALSE,
                                            "invisible" => FALSE,
                                            "visibleGridView" => FALSE,
                                            "visibleSearch" => FALSE,
                                            "defaultValueGenerator" => ""
                                        ]
                                    ],
                                    "isOperator" => FALSE
                                ],
                                [
                                    "attributes" => [
                                        "attribute" => "creator",
                                        "label" => "Ansvarshaver",
                                        "dataType" => "fieldcollections",
                                        "layout" => [
                                            "fieldtype" => "fieldcollections",
                                            "phpdocType" => "\\Pimcore\\Model\\DataObject\\Fieldcollection",
                                            "allowedTypes" => [
                                                "Creator"
                                            ],
                                            "lazyLoading" => TRUE,
                                            "maxItems" => "",
                                            "disallowAddRemove" => FALSE,
                                            "disallowReorder" => FALSE,
                                            "collapsed" => FALSE,
                                            "collapsible" => FALSE,
                                            "border" => FALSE,
                                            "name" => "creator",
                                            "title" => "Ansvarshaver",
                                            "tooltip" => "",
                                            "mandatory" => FALSE,
                                            "noteditable" => FALSE,
                                            "index" => FALSE,
                                            "locked" => FALSE,
                                            "style" => "",
                                            "permissions" => NULL,
                                            "datatype" => "data",
                                            "relationType" => FALSE,
                                            "invisible" => FALSE,
                                            "visibleGridView" => FALSE,
                                            "visibleSearch" => FALSE
                                        ]
                                    ],
                                    "isOperator" => FALSE
                                ],
                                [
                                    "attributes" => [
                                        "attribute" => "title",
                                        "label" => "Tittel",
                                        "dataType" => "input",
                                        "layout" => [
                                            "fieldtype" => "input",
                                            "width" => NULL,
                                            "defaultValue" => NULL,
                                            "queryColumnType" => "varchar",
                                            "columnType" => "varchar",
                                            "columnLength" => 190,
                                            "phpdocType" => "string",
                                            "regex" => "",
                                            "unique" => FALSE,
                                            "showCharCount" => FALSE,
                                            "name" => "title",
                                            "title" => "Tittel",
                                            "tooltip" => "",
                                            "mandatory" => FALSE,
                                            "noteditable" => FALSE,
                                            "index" => FALSE,
                                            "locked" => FALSE,
                                            "style" => "",
                                            "permissions" => NULL,
                                            "datatype" => "data",
                                            "relationType" => FALSE,
                                            "invisible" => FALSE,
                                            "visibleGridView" => FALSE,
                                            "visibleSearch" => FALSE,
                                            "defaultValueGenerator" => ""
                                        ]
                                    ],
                                    "isOperator" => FALSE
                                ],
                                [
                                    "attributes" => [
                                        "attribute" => "imageExternal",
                                        "label" => "Eksternt omslagsbilde",
                                        "dataType" => "externalImage",
                                        "layout" => [
                                            "fieldtype" => "externalImage",
                                            "previewWidth" => "",
                                            "inputWidth" => 600,
                                            "previewHeight" => "",
                                            "queryColumnType" => "longtext",
                                            "columnType" => "longtext",
                                            "phpdocType" => "\\Pimcore\\Model\\DataObject\\Data\\ExternalImage",
                                            "name" => "imageExternal",
                                            "title" => "Eksternt omslagsbilde",
                                            "tooltip" => "",
                                            "mandatory" => FALSE,
                                            "noteditable" => FALSE,
                                            "index" => FALSE,
                                            "locked" => FALSE,
                                            "style" => "",
                                            "permissions" => NULL,
                                            "datatype" => "data",
                                            "relationType" => FALSE,
                                            "invisible" => FALSE,
                                            "visibleGridView" => FALSE,
                                            "visibleSearch" => FALSE
                                        ]
                                    ],
                                    "isOperator" => FALSE
                                ]
                            ]
                        ]
                    ]
                ],
                "mutationEntities" => [
                    "Product" => [
                        "id" => "Product",
                        "name" => "Product",
                        "update" => TRUE,
                        "columnConfig" => [
                            "columns" => [
                                [
                                    "attributes" => [
                                        "attribute" => "isbn",
                                        "label" => "ISBN",
                                        "dataType" => "input",
                                        "layout" => [
                                            "fieldtype" => "input",
                                            "width" => NULL,
                                            "defaultValue" => NULL,
                                            "queryColumnType" => "varchar",
                                            "columnType" => "varchar",
                                            "columnLength" => 190,
                                            "phpdocType" => "string",
                                            "regex" => "",
                                            "unique" => FALSE,
                                            "showCharCount" => FALSE,
                                            "name" => "isbn",
                                            "title" => "ISBN",
                                            "tooltip" => "",
                                            "mandatory" => FALSE,
                                            "noteditable" => FALSE,
                                            "index" => FALSE,
                                            "locked" => FALSE,
                                            "style" => "",
                                            "permissions" => NULL,
                                            "datatype" => "data",
                                            "relationType" => FALSE,
                                            "invisible" => FALSE,
                                            "visibleGridView" => FALSE,
                                            "visibleSearch" => FALSE,
                                            "defaultValueGenerator" => ""
                                        ]
                                    ],
                                    "isOperator" => FALSE
                                ],
                                [
                                    "attributes" => [
                                        "attribute" => "title",
                                        "label" => "Tittel",
                                        "dataType" => "input",
                                        "layout" => [
                                            "fieldtype" => "input",
                                            "width" => NULL,
                                            "defaultValue" => NULL,
                                            "queryColumnType" => "varchar",
                                            "columnType" => "varchar",
                                            "columnLength" => 190,
                                            "phpdocType" => "string",
                                            "regex" => "",
                                            "unique" => FALSE,
                                            "showCharCount" => FALSE,
                                            "name" => "title",
                                            "title" => "Tittel",
                                            "tooltip" => "",
                                            "mandatory" => FALSE,
                                            "noteditable" => FALSE,
                                            "index" => FALSE,
                                            "locked" => FALSE,
                                            "style" => "",
                                            "permissions" => NULL,
                                            "datatype" => "data",
                                            "relationType" => FALSE,
                                            "invisible" => FALSE,
                                            "visibleGridView" => FALSE,
                                            "visibleSearch" => FALSE,
                                            "defaultValueGenerator" => ""
                                        ]
                                    ],
                                    "isOperator" => FALSE
                                ],
                                [
                                    "attributes" => [
                                        "attribute" => "creator",
                                        "label" => "Ansvarshaver",
                                        "dataType" => "fieldcollections",
                                        "layout" => [
                                            "fieldtype" => "fieldcollections",
                                            "phpdocType" => "\\Pimcore\\Model\\DataObject\\Fieldcollection",
                                            "allowedTypes" => [
                                                "Creator"
                                            ],
                                            "lazyLoading" => TRUE,
                                            "maxItems" => "",
                                            "disallowAddRemove" => FALSE,
                                            "disallowReorder" => FALSE,
                                            "collapsed" => FALSE,
                                            "collapsible" => FALSE,
                                            "border" => FALSE,
                                            "name" => "creator",
                                            "title" => "Ansvarshaver",
                                            "tooltip" => "",
                                            "mandatory" => FALSE,
                                            "noteditable" => FALSE,
                                            "index" => FALSE,
                                            "locked" => FALSE,
                                            "style" => "",
                                            "permissions" => NULL,
                                            "datatype" => "data",
                                            "relationType" => FALSE,
                                            "invisible" => FALSE,
                                            "visibleGridView" => FALSE,
                                            "visibleSearch" => FALSE
                                        ]
                                    ],
                                    "isOperator" => FALSE
                                ],
                                [
                                    "attributes" => [
                                        "attribute" => "imageExternal",
                                        "label" => "Eksternt omslagsbilde",
                                        "dataType" => "externalImage",
                                        "layout" => [
                                            "fieldtype" => "externalImage",
                                            "previewWidth" => "",
                                            "inputWidth" => 600,
                                            "previewHeight" => "",
                                            "queryColumnType" => "longtext",
                                            "columnType" => "longtext",
                                            "phpdocType" => "\\Pimcore\\Model\\DataObject\\Data\\ExternalImage",
                                            "name" => "imageExternal",
                                            "title" => "Eksternt omslagsbilde",
                                            "tooltip" => "",
                                            "mandatory" => FALSE,
                                            "noteditable" => FALSE,
                                            "index" => FALSE,
                                            "locked" => FALSE,
                                            "style" => "",
                                            "permissions" => NULL,
                                            "datatype" => "data",
                                            "relationType" => FALSE,
                                            "invisible" => FALSE,
                                            "visibleGridView" => FALSE,
                                            "visibleSearch" => FALSE
                                        ]
                                    ],
                                    "isOperator" => FALSE
                                ],
                                [
                                    "attributes" => [
                                        "attribute" => "key",
                                        "label" => "key",
                                        "dataType" => "system",
                                        "layout" => [
                                            "title" => "key",
                                            "name" => "key",
                                            "datatype" => "data",
                                            "fieldtype" => "system"
                                        ]
                                    ],
                                    "isOperator" => FALSE
                                ],
                                [
                                    "attributes" => [
                                        "attribute" => "id",
                                        "label" => "id",
                                        "dataType" => "system",
                                        "layout" => [
                                            "title" => "id",
                                            "name" => "id",
                                            "datatype" => "data",
                                            "fieldtype" => "system"
                                        ]
                                    ],
                                    "isOperator" => FALSE
                                ],
                                [
                                    "attributes" => [
                                        "attribute" => "originalSource",
                                        "label" => "Originalkilde",
                                        "dataType" => "textarea",
                                        "layout" => [
                                            "fieldtype" => "textarea",
                                            "width" => 600,
                                            "height" => "",
                                            "maxLength" => NULL,
                                            "showCharCount" => FALSE,
                                            "excludeFromSearchIndex" => FALSE,
                                            "queryColumnType" => "longtext",
                                            "columnType" => "longtext",
                                            "phpdocType" => "string",
                                            "name" => "originalSource",
                                            "title" => "Originalkilde",
                                            "tooltip" => "",
                                            "mandatory" => FALSE,
                                            "noteditable" => FALSE,
                                            "index" => FALSE,
                                            "locked" => FALSE,
                                            "style" => "",
                                            "permissions" => NULL,
                                            "datatype" => "data",
                                            "relationType" => FALSE,
                                            "invisible" => FALSE,
                                            "visibleGridView" => FALSE,
                                            "visibleSearch" => FALSE
                                        ]
                                    ],
                                    "isOperator" => FALSE
                                ],
                                [
                                    "attributes" => [
                                        "attribute" => "publisherSummary",
                                        "label" => "Forlagets omtale",
                                        "dataType" => "textarea",
                                        "layout" => [
                                            "fieldtype" => "textarea",
                                            "width" => 600,
                                            "height" => "",
                                            "maxLength" => NULL,
                                            "showCharCount" => TRUE,
                                            "excludeFromSearchIndex" => FALSE,
                                            "queryColumnType" => "longtext",
                                            "columnType" => "longtext",
                                            "phpdocType" => "string",
                                            "name" => "publisherSummary",
                                            "title" => "Forlagets omtale",
                                            "tooltip" => "",
                                            "mandatory" => FALSE,
                                            "noteditable" => FALSE,
                                            "index" => FALSE,
                                            "locked" => FALSE,
                                            "style" => "",
                                            "permissions" => NULL,
                                            "datatype" => "data",
                                            "relationType" => FALSE,
                                            "invisible" => FALSE,
                                            "visibleGridView" => FALSE,
                                            "visibleSearch" => FALSE
                                        ]
                                    ],
                                    "isOperator" => FALSE
                                ]
                            ]
                        ],
                        "create" => TRUE
                    ]
                ],
                "specialEntities" => [
                    "document" => [
                        "read" => FALSE,
                        "create" => FALSE,
                        "update" => FALSE,
                        "delete" => FALSE
                    ],
                    "document_folder" => [
                        "read" => FALSE,
                        "create" => FALSE,
                        "update" => FALSE,
                        "delete" => FALSE
                    ],
                    "asset" => [
                        "read" => FALSE,
                        "create" => FALSE,
                        "update" => FALSE,
                        "delete" => FALSE
                    ],
                    "asset_folder" => [
                        "read" => FALSE,
                        "create" => FALSE,
                        "update" => FALSE,
                        "delete" => FALSE
                    ],
                    "asset_listing" => [
                        "read" => FALSE,
                        "create" => FALSE,
                        "update" => FALSE,
                        "delete" => FALSE
                    ],
                    "object_folder" => [
                        "read" => TRUE,
                        "create" => TRUE,
                        "update" => TRUE,
                        "delete" => FALSE
                    ]
                ]
            ],
            "security" => [
                "method" => "datahub_apikey",
                "apikey" => "1f9a26fd52715767950344255bffdfff",
                "skipPermissionCheck" => FALSE
            ],
            "workspaces" => [
                "asset" => [

                ],
                "document" => [

                ],
                "object" => [
                    [
                        "read" => TRUE,
                        "cpath" => "/Test",
                        "create" => TRUE,
                        "update" => TRUE,
                        "delete" => FALSE,
                        "id" => "extModel7468-1"
                    ],
                    [
                        "read" => TRUE,
                        "cpath" => "/Test/Gui",
                        "create" => TRUE,
                        "update" => TRUE,
                        "delete" => FALSE,
                        "id" => "extModel2200-1"
                    ]
                ]
            ]
        ]
    ]
];
