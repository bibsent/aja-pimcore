<?php 

return [
    "bundle" => [
        "Pimcore\\Bundle\\DataHubBundle\\PimcoreDataHubBundle" => TRUE,
        "Pimcore\\Bundle\\EcommerceFrameworkBundle\\PimcoreEcommerceFrameworkBundle" => FALSE,
        "Wvision\\Bundle\\DataDefinitionsBundle\\DataDefinitionsBundle" => FALSE,
        "FormBuilderBundle\\FormBuilderBundle" => TRUE,
        "ExportBundle\\ExportBundle" => TRUE,
        "ToolboxBundle\\ToolboxBundle" => TRUE,
        "Blackbit\\DataDirectorBundle\\BlackbitDataDirectorBundle" => TRUE,
        "Elements\\Bundle\\ProcessManagerBundle\\ElementsProcessManagerBundle" => TRUE,
        "GardnersBundle\\GardnersBundle" => TRUE,
        "AdvancedObjectSearchBundle\\AdvancedObjectSearchBundle" => TRUE
    ]
];
