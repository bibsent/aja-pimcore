<?php 

return [
    "large" => [
        "items" => [
            [
                "method" => "resize",
                "arguments" => [
                    "width" => NULL,
                    "height" => 600
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "large",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1601663489,
        "creationDate" => 1601451030,
        "forcePictureTag" => FALSE,
        "id" => "large"
    ],
    "medium" => [
        "items" => [
            [
                "method" => "resize",
                "arguments" => [
                    "width" => NULL,
                    "height" => 300
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "medium",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1601663472,
        "creationDate" => 1601451041,
        "forcePictureTag" => FALSE,
        "id" => "medium"
    ],
    "small" => [
        "items" => [
            [
                "method" => "resize",
                "arguments" => [
                    "width" => NULL,
                    "height" => 150
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "small",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1601451071,
        "creationDate" => 1601451049,
        "forcePictureTag" => FALSE,
        "id" => "small"
    ],
    "original-jpeg" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "original-jpeg",
        "description" => "Original for eksport til Promus",
        "group" => "",
        "format" => "JPEG",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1605778624,
        "creationDate" => 1605778530,
        "forcePictureTag" => FALSE,
        "id" => "original-jpeg"
    ],
    "thumbnail-jpeg" => [
        "items" => [
            [
                "method" => "resize",
                "arguments" => [
                    "width" => NULL,
                    "height" => 450
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "thumbnail-jpeg",
        "description" => "Thumbnail for eksport til Promus",
        "group" => "",
        "format" => "JPEG",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1605778646,
        "creationDate" => 1605778559,
        "forcePictureTag" => FALSE,
        "id" => "thumbnail-jpeg"
    ]
];
