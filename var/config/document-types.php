<?php 

return [
    1 => [
        "id" => 1,
        "name" => "Formbuilder Email",
        "group" => NULL,
        "module" => "FormBuilderBundle",
        "controller" => "@FormBuilderBundle\\Controller\\EmailController",
        "action" => "email",
        "template" => "FormBuilderBundle:Email:email.html.twig",
        "type" => "email",
        "priority" => 0,
        "creationDate" => 1603872559,
        "modificationDate" => 1603872559
    ],
    2 => [
        "id" => 2,
        "name" => "Teaser Snippet",
        "group" => NULL,
        "module" => "ToolboxBundle",
        "controller" => "Snippet",
        "action" => "teaser",
        "template" => "",
        "type" => "snippet",
        "priority" => 0,
        "creationDate" => 1603872614,
        "modificationDate" => 1603872614
    ]
];
