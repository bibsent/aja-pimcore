#!/bin/bash
#
# CURRENT_UID settes til UID-en til den lokale brukeren, 501 f.eks.
#
# Inspirasjon: 
# - https://github.com/jupyter/docker-stacks/pull/455/files
# - https://github.com/parente/docker-stacks/blob/master/base-notebook/start.sh
# - https://success.mirantis.com/article/use-a-script-to-initialize-stateful-container-data

set -euo pipefail

CURRENT_UID=$(id -u www-data)
WANTED_UID=${RUN_AS_UID:-}
APACHE_SERVER_NAME=${APACHE_SERVER_NAME:-localhost}

# echo "Starting container, current UID: $CURRENT_UID, wanted UID: $WANTED_UID"

# Handle special flags if we're root
if [ "$(whoami)" == "root" ] ; then

    # Switch to the root of the container in case we start adjusting paths
    # that impact the default working directory
    cd /

    # echo "Running as root, fixing permissions"

    # check if UID need to be updated
    # OBS: Under NFS; er ikke home-mappa nødv. eid av riktig bruker
    if [[ -n $WANTED_UID && $CURRENT_UID -ne $WANTED_UID ]]; then
        echo "[Changing www-data UID from $CURRENT_UID to $WANTED_UID]"
        # usermod --uid $WANTED_UID www-data

        userdel www-data
        useradd --home /home/www-data -m -u $WANTED_UID www-data

        # Fix permissions for any unmounted directories
        chown -R www-data:www-data /home/www-data /var/www/.cache
        chown www-data:www-data /var/www /var/www/cache  # -R should not be needed here
    fi

    # chown -R www-data:www-data /var/www/html
fi

cd /var/www/html

if [[ -z "$*" ]]; then
    echo "[Starting server for: $APACHE_SERVER_NAME, env: $APP_ENV, www-data UID: $(id -u www-data)]"

    # Run startup script as www-data
    gosu www-data ./docker/startup.sh

    # Apache will fork, so it must be started as root
    echo Starting Apache

    echo "Setting ServerName: $APACHE_SERVER_NAME"
    sed -i -n -e '/^ServerName /!p' -e '$a'"ServerName $APACHE_SERVER_NAME" /etc/apache2/apache2.conf

    # Apache starts as root, but forks as www-data
    exec /usr/local/bin/docker-php-entrypoint "apache2-foreground"
else
    # Other tasks should run as www-data directly
    echo "[Starting task as www-data (UID: $(id -u www-data))]"
    exec gosu www-data /usr/local/bin/docker-php-entrypoint "$@"
fi
