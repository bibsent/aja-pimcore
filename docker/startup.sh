#!/usr/bin/env bash

set -euo pipefail

export COMPOSER_MEMORY_LIMIT=-1

if [ ! -z "${GIT_TAG+x}" ]; then

    echo "Waiting for Elasticsearch: $ELASTICSEARCH_HOST"
    ./docker/wait-for-http $ELASTICSEARCH_HOST

    echo "Waiting for Redis: $REDIS_HOST:6379"
    ./docker/wait-for-it.sh $REDIS_HOST:6379

    CURRENT_VERSION_FILE=/var/www/current_version

    if [[ -f "$CURRENT_VERSION_FILE" ]]; then
        CURRENT_VERSION="$(cat "$CURRENT_VERSION_FILE" | xargs echo -n )"  # Note: File might exist, but be empty
    else
        CURRENT_VERSION=""
    fi

    if [[ -z "$CURRENT_VERSION" ]]; then

        echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        echo "🚀✨ Pimcore is not yet installed. Starting install script in 5 seconds..."
        echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        sleep 5

        if [[ -f /run/secrets/installation_config ]]; then
            echo "🔐 Loading installation config"
            . /run/secrets/installation_config
            echo
        fi

        composer run-script post-create-project-cmd

        ./bin/install

        echo "${GIT_TAG}" >| $CURRENT_VERSION_FILE

    else

        echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        echo "🚀✨ Running composer post-update tasks"
        echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        CURRENT_VERSION="$(cat "$CURRENT_VERSION_FILE" | xargs echo -n )"

        # Need to run post-update-cmd when the container exists to ensure all symlinks are in place
        composer run-script post-update-cmd

    fi

    if [[ "$CURRENT_VERSION" != "$GIT_TAG" ]]; then

        echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        echo "🚀✨ Pimcore upgraded from ${CURRENT_VERSION:-(none)} to $GIT_TAG, running post-upgrade tasks"
        echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        # Update class definitions, then run data migrations (which may require updated class definitions)
        bin/console pimcore:deployment:classes-rebuild -c

        bin/console pimcore:migrations:migrate  # Pimcore <= 6.9
        # - bin/console doctrine:migrations:migrate  # Pimcore >= 10.0

        # Update Data Director job definitions
        bin/console data-director:deployment:dataport-rebuild
        # Bootstrap folders and roles/permissions
        bin/console aja:install

        echo "${GIT_TAG}" >| $CURRENT_VERSION_FILE

    fi

    echo "Optimize env ($APP_ENV)"
    composer dump-env $APP_ENV

    echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    echo "🚀✨ Starting Pimcore @ $GIT_TAG"
    echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

fi
