<?php

namespace AppBundle\Job\DataSource;

use Carbon\Carbon;
use Elements\Bundle\ProcessManagerBundle\MetaDataFile;
use Elements\Bundle\ProcessManagerBundle\Model\MonitoringItem;
use Generator;
use Pimcore\Model\DataObject\WebServiceConfig;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\HttpClient;

class BokbasenApi extends DataSource implements CachableDataSourceInterface
{
    private $monitor;
    private $config;
    private $destinationDir;
    private $jobKey;
    private $http;
    private $token;
    private $cursor;
    private $params = [];
    private $stateFile;

    public function __construct(
        MonitoringItem $monitor,
        LoggerInterface $logger,
        WebServiceConfig $config,
        string $destinationDir,
        Carbon $startDate = null,
        HttpClient $http = null
    )
    {
        parent::__construct($logger);
        $this->monitor = $monitor;
        $this->config = $config;
        $this->http = $http ?: HttpClient::create();

        $this->stateFile = MetaDataFile::getById('bokbasen-state');
        $stateData = $this->stateFile->getData();
        $this->destinationDir = $destinationDir;

        if ($startDate) {
            // Fetch objects since a specific date (manual run)
            $this->params = ['after' => $startDate->isoFormat('YYYYMMDD000000')];
            $this->jobKey = sha1($this->params['after']);
        } elseif ($stateData['cursor']) {
            // Fetch new objects since last import
            $this->cursor = $stateData['cursor'];
            $this->params = ['next' => $this->cursor];
            $this->jobKey = sha1($this->cursor);
        } else {
            $this->jobKey = 'error';
        }
    }

    /**
     * @inheritDoc
     */
    public function getJobDir(): string
    {
        return $this->destinationDir . '/' . $this->jobKey;
    }

    /**
     * @inheritDoc
     */
    public function getFiles(): array
    {
        if (empty($this->params)) {
            $this->monitor
                ->setMessage('Importen har ikke blitt kjørt før. ' .
                    'Vennligst angi en startdato for når vi skal hente poster fra første gang.')
                ->setStatus(MonitoringItem::STATUS_FAILED)
                ->save();
            return [];
        }

        $this->ensureDirExists($this->getJobDir());

        $this->monitor->setMessage('Locating files to download...')->save();
        $this->log('[BokbasenApi] Starting import from:' . json_encode($this->params), 'notice');

        $this->login();
        $queue = iterator_to_array($this->getReport($this->params));
        $objectCount = count($queue);

        $this->log("[BokbasenApi] Downloading $objectCount files", 'notice');
        $this->monitor
            ->setTotalWorkload($objectCount)
            ->setCurrentWorkload(0)
            ->setMessage("Downloading $objectCount files")
            ->save();

        $n = 0;
        $files = [];
        foreach ($queue as $item) {
            $this->monitor->setCurrentWorkload(++$n)->save();
            $files[] = $this->downloadObject($item);
        }

        // Store current cursor, so we can continue from this value on next run
        $this->stateFile->setData(['cursor' => $this->cursor])->save();

        $this->log("[BokbasenApi] Downloaded $objectCount files. Cursor saved: " . $this->cursor, 'notice');

        return $files;
    }

    public function getName(): string
    {
        return $this->config->getKey();
    }

    private function login()
    {
        $response = $this->http->request('POST', 'https://login.boknett.no/v1/tickets', [
            'body' => [
                'username' => $this->config->getUsername(),
                'password' => $this->config->getPassword(),
            ],
        ]);

        if ($response->getStatusCode() != 201) {
            throw new \Exception('Login failed, got response with status code ' . $response->getStatusCode());
        }

        $headers = $response->getHeaders();
        $this->token =  $headers['boknett-tgt'][0];
        $this->log('[BokbasenApi] Login OK', 'info');
    }

    private function getDate(): string
    {
        $date = (new Carbon('now', 'GMT'))->toRfc1123String();
        return  str_replace('+0000', 'GMT', $date); // Bokbasen skriver at de støtter RFC 1123, stemmer ikke heelt
    }

    private function getReport(array $params): Generator
    {
        $moreResults = true;
        $n = 0;
        $m = 0;
        $r = 0;

        while ($moreResults === true) {
            $r++;
            $response = $this->http->request('GET', 'https://api.boknett.no/metadata/export/object', [
                'headers' => [
                    'Authorization' => "Boknett {$this->token}",
                    'Date' => $this->getDate(),
                ],
                'query' => $params,
            ]);
            $headers = $response->getHeaders();
            $moreResults = isset($headers['link']);

            $this->cursor = $headers['next'][0];
            $params = ['next' => $this->cursor];

            $dom = simplexml_load_string($response->getContent());


            foreach ($dom->OBJECT as $obj) {
                // Tre typer: 'org', 'ol' og 'os'
                if ($obj->TYPE != 'org') {
                    continue;
                }

                $filename = (string)$obj->ISBN13. '.jpg';
                if ($this->filterFilename($filename)) {
                    $n++;
                    yield [
                        'filename' => $filename,
                        'url' => (string)$obj->REFERENCE
                    ];
                } else {
                    $m++;
                }
            }
        }

        $this->log("[BokbasenApi] Found $n matching files, $m non-matching", 'notice');
    }

    private function getFilename(array $headers): string {
        $contentDisp = $headers['content-disposition'][0];
        if (preg_match('/filename="(.+?)"/', $contentDisp, $matches)) {
            return $matches[1];
        }
        if (preg_match('/filename=([^; ]+)/', $contentDisp, $matches)) {
            return rawurldecode($matches[1]);
        }
        throw new \RuntimeException(__FUNCTION__ .": Filename not found in Content-Disposition header: " . $contentDisp);
    }

    private function downloadObject(array $object): string
    {
        $response = $this->http->request('GET', $object['url'], [
            'headers' => [
                'Authorization' => "Boknett {$this->token}",
                'Date' => $this->getDate(),
                'Accept' => 'application/octet-stream',
            ],
        ]);
        // $filename = $this->getFilename($response->getHeaders());

        // Vi får tre forskjellige filer for hver bok: org.jpg (original), ol.jpg (large), os.jpg (small).
        // Lagrer bare org.jpg
        $path = $this->getJobDir() . '/' . $object['filename'];
        file_put_contents($path, $response->toStream());

        return $path;
    }
}
