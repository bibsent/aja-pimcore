<?php

namespace AppBundle\Job\DataSource;

interface CachableDataSourceInterface extends DataSourceInterface
{
    /**
     * Get the destination dir.
     *
     * @return string
     */
    public function getJobDir(): string;
}
