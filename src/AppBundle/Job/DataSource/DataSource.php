<?php

namespace AppBundle\Job\DataSource;

use AppBundle\Util\Utils;
use Psr\Log\LoggerInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

abstract class DataSource
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var string
     */
    private $filenameFilter;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    protected function ensureDirExists(string $path): string
    {
        $path = rtrim($path, '/');

        // Check that local dir exists
        if (!is_dir($path)) {
            $this->log('Attempting to create: ' . $path);
            if (!mkdir($path, 0755, true)) {
                throw new \RuntimeException('Could not create dir: ' . $path);
            }
        }

        return $path;
    }

    protected function listFiles(string $dir): array
    {
        $finder = new Finder();

        return Utils::map(function (SplFileInfo $file) {
            return $file->getRealPath();
        }, $finder->in($dir)->files());
    }

    protected function log(string $msg, $level = 'info')
    {
        if (isset($this->logger)) {
            $this->logger->log($level, $msg);
        }
    }

    public function setFilenameFilter(?string $value): void
    {
        if (is_null($value)) {
            $this->filenameFilter = null;
            return;
        }
        $values = empty($value) ? [] : explode('\n', $value);
        # Remove comment lines
        $values = array_filter($values, function($line) {
            return strpos($line, '#') !== 0 && !empty($line);
        });
        if (!count($values)) {
            $this->filenameFilter = null;
        } else {
            // Prepare regex
            $this->filenameFilter = '/(' . implode('|', $values) . ')/';
        }
    }

    public function filterFilename(string $basename): bool
    {
        if (is_null($this->filenameFilter)) {
            return true;
        }

        return preg_match($this->filenameFilter, $basename) === 1;
    }
}
