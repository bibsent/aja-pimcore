<?php

namespace AppBundle\Job\DataSource;

interface DataSourceInterface
{
    /**
     * Get files from the data source.
     *
     * @return array
     */
    public function getFiles(): array;

    /**
     * Get a human-readable name of the data source.
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Set filename filters as a multi-line string, one per line.
     *
     * @param string $filters
     */
    public function setFilenameFilter(string $filters): void;
}
