<?php

namespace AppBundle\Job\DataSource;

use AppBundle\Util\Utils;
use Carbon\Carbon;
use Elements\Bundle\ProcessManagerBundle\Model\MonitoringItem;
use FtpClient\FtpClient;
use Monolog\Logger;
use Pimcore\Model\Asset;
use Pimcore\Model\DataObject\FtpServerConfig;
use Psr\Log\LoggerInterface;

class FtpServer extends DataSource implements DataSourceInterface
{
    private $monitor;
    private $config;
    private $date;
    private $destinationDir;

    public function __construct(
        MonitoringItem $monitor,
        LoggerInterface $logger,
        FtpServerConfig $config,
        Carbon $date,
        Asset $destinationDir
    )
    {
        parent::__construct($logger);
        $this->monitor = $monitor;
        $this->config = $config;
        $this->date = $date;
        $this->destinationDir = $destinationDir;
    }

    /**
     * Error handler
     * @param string $msg
     */
    protected function fail(string $msg): void
    {
        $this->monitor
            ->setStatus(MonitoringItem::STATUS_FAILED)
            ->setMessage($msg, Logger::ERROR)
            ->save();

        throw new \RuntimeException($msg);
    }

    /**
     * @return Asset[] List of assets
     */
    public function getFiles(): array
    {
        // Connect
        $conn = (new FtpClient())
            ->connect($this->config->getHost(), $this->config->getSsl(), $this->config->getPort())
            ->login($this->config->getUsername(), $this->config->getPassword());
        $conn->pasv(true);

        // Locate files to download
        $files = $this->locateFiles($conn);

        $fileCount = count($files);
        if (!$fileCount) {
            $this->monitor
                ->setStatus(MonitoringItem::STATUS_FAILED)
                ->setMessage('Found no files to download', Logger::WARNING)
                ->save();
            throw new \RuntimeException('Found no files to download');
        }

        $downloaded = 0;
        $totalSize = array_sum(array_map(function ($file) {
            return $file['size'];
        }, $files));
        $totalSizeMb = $totalSize / 1024 / 1024;

        $this->monitor
            ->setMessage(sprintf(
                'Downloading %d file(s) (%.1f MB)', count($files), $totalSizeMb
            ))
            ->setTotalWorkload($totalSize)
            ->save();

        // Download files
        $out = [];
        foreach ($files as $file) {
            $out[] = $this->downloadFile($conn, $file, $downloaded);
            $downloaded += $file['size'];
        }

        return $out;
    }

    /**
     * @return string[] List of non-empty remote path patterns.
     */
    private function getRemotePaths(): array
    {
        $paths = [];
        foreach (explode('\n', $this->config->getRemotePaths()) as $path) {
            if (!empty($path)) {
                // Expand [[date:*]] template
                $paths[] = Utils::expandDateTemplate($path, $this->date);
            }
        }
        return $paths;
    }

    /**
     * @param FtpClient $conn
     * @return array[] List of files matching the remote path patterns
     */
    private function locateFiles(FtpClient $conn): array
    {
        $files = [];
        foreach ($this->getRemotePaths() as $path) {
            $n = 0;
            $pathinfo = pathinfo($path);
            foreach ($conn->scanDir($pathinfo['dirname']) as $item) {
                if (fnmatch($pathinfo['basename'], $item['name'])) {
                    $n++;
                    $this->logger->notice('[FtpServer] Planning to download: ' . $item['name']);
                    $files[] = [
                        'remote' => $pathinfo['dirname'] . '/' . $item['name'],
                        'local' => rtrim($this->destinationDir->getFullPath(), '/') . '/'. $item['name'],
                        'size' => $item['size'],
                    ];
                }
            }
            $this->logger->notice("[FtpServer] $n file(s) matched pattern: $path");
        }
        return $files;
    }

    /**
     * @param FtpClient $conn
     * @param array $file
     * @param int $downloaded
     */
    protected function downloadFile(FtpClient $conn, array $file, int $downloaded): Asset
    {
        $basename = basename($file['remote']);

        $asset = Asset::getByPath($file['local']);
        if (!is_null($asset)) {
            $this->logger->notice('[FtpServer] File has already been downloaded: ' . $file['local']);
            return $asset;
        }

        $this->logger->notice(sprintf('[FtpServer] Downloading %s (%.1f MB)', $basename, $file['size'] / 1024 / 1024));

        $tmpfile = tmpfile();
        $tmpfile_path = stream_get_meta_data($tmpfile)['uri']; // eg: /tmp/phpFx0513a

        $ret = $conn->nb_get($tmpfile_path, $file['remote'], FTP_BINARY, FTP_AUTORESUME);
        $n = 0;
        $t0 = microtime(true);
        while ($ret == FTP_MOREDATA) {
            if ($n++ % 500 == 0) {
                clearstatcache();
                $this->monitor
                    ->setCurrentWorkload($downloaded + filesize($tmpfile_path))
                    ->save();
            }
            $ret = $conn->nb_continue();
        }
        if ($ret != FTP_FINISHED) {
            $err = error_get_last();
            if (!is_null($err)) {
                $this->fail("Download failed: " . $err['message']);
            } else {
                $this->fail("Download failed with unknown error");
            }
        }

        $asset = new Asset();
        $asset->setFilename($basename)
            ->setParent($this->destinationDir)
            ->setData(file_get_contents($tmpfile_path))
            ->save();

        fclose($tmpfile);

        $dt = microtime(true) - $t0;
        $this->logger->notice(sprintf(
            '[FtpServer] Downloaded %s (%.1f MB) in %.1f secs',
            $basename, $file['size'] / 1024 / 1024, $dt
        ));

        if ($this->config->getDeleteAfterDownload()) {
            $conn->delete($file['remote']);
        }

        return $asset;
    }

    public function getName(): string
    {
        return $this->config->getKey();
    }
}
