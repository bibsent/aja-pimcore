<?php

namespace AppBundle\Job\DataSource;

use Elements\Bundle\ProcessManagerBundle\Model\MonitoringItem;
use Psr\Log\LoggerInterface;
use Symfony\Component\Process\Process;

class Unzip extends DataSource implements CachableDataSourceInterface
{
    private $monitor;
    private $files;
    private $destinationDir;

    public function __construct(MonitoringItem $monitor, LoggerInterface $logger, array $files, $destinationDir)
    {
        parent::__construct($logger);
        $this->monitor = $monitor;
        $this->files = $files;
        $this->destinationDir = $this->ensureDirExists($destinationDir);
    }

    /**
     * @inheritDoc
     */
    public function getJobDir(): string
    {
        return $this->destinationDir;
    }

    /**
     * @inheritDoc
     */
    public function getFiles(): array
    {
        $this->monitor
            ->setMessage('Inspecting zip files...')
            ->setCurrentWorkload(1)
            ->setTotalWorkload(1000)
            ->save();
        $unpacked = 0;
        $fileCount = $this->getFileCount();

        $this->monitor
            ->setMessage("Unpacking $fileCount files")
            ->setTotalWorkload($fileCount)
            ->save();

        $t0 = microtime(true);
        foreach ($this->files as $zipFile) {
            $unpacked += $this->unpackZipFile($zipFile, $unpacked);
        }
        $dt = microtime(true) - $t0;
        $this->log(sprintf('Unpacked %d zip file(s) in %.1f secs', count($this->files), $dt), 'notice');

        $files = $this->listFiles($this->destinationDir);
        $unfilteredCount = count($files);
        $files = array_filter(
            $files,
            function ($path) {
                return $this->filterFilename(basename($path));
            }
        );

        $this->log(sprintf('%d of %d file(s) remain aftering filtering', count($files), $unfilteredCount), 'notice');
        return $files;
    }

    /**
     * Count the number of files in the zip files. Only takes a second or so.
     * @return int
     */
    protected function getFileCount(): int
    {
        $fileCount = 0;
        // Note: We don't use PHP's ZipArchive, since it fails with "Multi-disk zip archives not supported" for the zip files from Ingram
        foreach ($this->files as $file) {
            $process = new Process(['unzip', '-l', $file]);
            $process->mustRun();
            $fileCount += count(explode("\n", $process->getOutput())) - 5; // 3 header lines, 2 footer lines
        }
        return $fileCount;
    }

    /**
     * @param string $path Path to zip file
     * @param int $baseline Progress bar baseline
     * @return int  Number of files unpacked
     */
    protected function unpackZipFile(string $path, int $baseline): int
    {
        $t0 = microtime(true);
        $basename = basename($path);

        $this->logger->notice("Unpacking $basename");

        $process = new Process(['unzip', '-o', '-j', $path, '-d', $this->destinationDir]);
        $process->setTimeout(300); // seconds
        $process->start();

        $unpacked = 0;
        while ($process->isRunning()) {
            $out = $process->getIncrementalOutput();
            $unpacked += empty($out) ? 0 : substr_count($out, 'inflating:');
            $this->monitor->setCurrentWorkload($baseline + $unpacked)->save();
            usleep(300);
        }

        $out = $process->getIncrementalOutput();
        $unpacked += empty($out) ? 0 : substr_count($out, 'inflating:');

        $dt = microtime(true) - $t0;
        $this->logger->notice(sprintf(
            'Unpacked %d files from %s in %.1f secs',
            $unpacked, $basename, $dt
        ));

        $this->logger->notice("Removing $basename");
        if (!@unlink($path)) {
            $this->logger->warning("Could not unlink $basename");
        }

        return $unpacked;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'ziparchive';
    }
}
