<?php

namespace AppBundle\Job\DataSource;

use Elements\Bundle\ProcessManagerBundle\Model\MonitoringItem;
use Psr\Log\LoggerInterface;

class Filesystem extends DataSource implements DataSourceInterface
{
    private $monitor;
    private $paths;

    public function __construct(MonitoringItem $monitor, LoggerInterface $logger, array $paths)
    {
        parent::__construct($logger);
        $this->monitor = $monitor;
        $this->paths = $paths;
    }

    public function getFiles(): array
    {
        $files = [];

        foreach ($this->paths as $path) {
            if (is_dir($path)) {
                foreach ($this->listFiles($path) as $filePath) {
                    $files[] = $filePath;
                }
            } elseif (is_file($path)) {
                $files[] = $path;
            } else {
                $this->log('Not a file or dir: ' . $path, 'ERROR');
            }
        }

        return $files;
    }

    public function getName(): string
    {
        return 'local_files';
    }
}
