<?php


namespace AppBundle\Job;


use Blackbit\DataDirectorBundle\lib\Pim\Item\ImporterInterface;
use Blackbit\DataDirectorBundle\lib\Pim\Item\Importmanager;
use Blackbit\DataDirectorBundle\lib\Pim\RawData\Importmanager as RawDataImportmanager;
use Blackbit\DataDirectorBundle\model\ImportStatus;
use Elements\Bundle\ProcessManagerBundle\Model\MonitoringItem;

class DataDirectorImportJob
{
    private $rawDataImporter;
    private $itemImporter;
    private $dataportId;

    public function __construct(
        ImporterInterface $itemImporter,
        MonitoringItem $monitor,
        int $dataportId,
        string $filename
    )
    {
        $this->dataportId = $dataportId;
        $logger = $monitor->getLogger();

        $this->rawDataImporter = new RawDataImportmanager($logger);
        $this->rawDataImporter->setOverrideFile($filename);
        $this->rawDataImporter->setMonitoringItem($monitor);
        // $this->rawDataImporter->setRemoveFileAfterImport(true);

        $this->itemImporter = new Importmanager($itemImporter, $logger, true); // ???
        $this->itemImporter->setMonitoringItem($monitor);
    }

    /**
     * This is basically just a simple version of Blackbit\DataDirectorBundle\Command\ImportCompleteCommand
     */
    public function start()
    {
        // 1. RawData import
        $dataportResourceId = $this->rawDataImporter->importDataport(
            $this->dataportId,
            ImportStatus::TYPE_COMPLETE_RAWDATA
        );

        // 2. Item import
        $this->itemImporter->importDataport(
            [$dataportResourceId],
            ImportStatus::TYPE_COMPLETE_PIM
        );
    }
}
