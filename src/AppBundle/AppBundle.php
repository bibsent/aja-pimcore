<?php

namespace AppBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;
use Pimcore\Routing\RouteReferenceInterface;

class AppBundle extends AbstractPimcoreBundle
{
    /**
     * Get javascripts to include in admin interface
     *
     * @return string[]|RouteReferenceInterface[]
     */
    public function getJsPaths()
    {
        return [
            '/bundles/app/js/startup.js',
            '/bundles/app/js/importSdArgumentsCallback.js',
        ];
    }

    /**
     * Get stylesheets to include in admin interface
     *
     * @return string[]|RouteReferenceInterface[]
     */
    public function getCssPaths()
    {
        return [
            '/bundles/app/css/admin.css'
        ];
    }
}
