<?php

namespace AppBundle\Service;

use AppBundle\Model\Product;
use Pimcore\Model\Element\Note;
use Psr\Log\LoggerInterface;
use SimpleXMLElement;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;

class ExportService
{
    const EventContext = 'ExportToPromusService';
    private $logger;
    private $fs;
    private $exportDir;
    private $archiveDir;

    public function __construct(LoggerInterface $logger, Filesystem $fs, string $exportDir, string $archiveDir)
    {
        $this->logger = $logger;
        $this->fs = $fs;
        $this->exportDir = rtrim($exportDir, '/');
        $this->archiveDir = rtrim($archiveDir, '/');
    }

    /**
     * Generate XML data and its hash.
     *
     * @param Product $product
     * @return string[]|null[]
     */
    public function generateXml(Product $product): array
    {
        $imageUrl = $product->getCoverUrlOriginal();
        $thumbnailUrl = $product->getCoverUrlThumbnail();
        if (!$imageUrl) {
            $this->logger->info('Won\'t export XML because product has no image.', [
                'Service' => 'PromusExport',
                'ProductKey' => $product->getKey(),
            ]);
            return [null, null];
        }
        $root = new SimpleXMLElement('<root/>');
        $node = $root->addChild('item');
        $node->addChild('id', $product->getProductId());
        $node->addChild('uuid', $product->getUuid());
        $node->addChild('cover_url_original', $imageUrl);
        if ($thumbnailUrl) {
            $node->addChild('cover_url_thumbnail', $thumbnailUrl);
        }
        $xml = $root->asXML();
        return [$xml, sha1($xml)];
    }

    /**
     * Export XML to Promus, if needed.
     * @param Product $product
     * @param bool $force Force export even if XML is unchanged.
     */
    public function exportToPromus(Product $product, bool $force = false)
    {
        list($xml, $xmlVersion) = $this->generateXml($product);
        if (is_null($xml)) {
            return;
        }
        if ($xmlVersion === $product->getExportedToPromusVersion() && !$force) {
            $this->logger->info('XML is unchanged, no need to re-export.', [
                'Service' => 'PromusExport',
                'ProductKey' => $product->getKey(),
            ]);
            return;
        }

        // Shorter hash for display
        $shortVersion = substr($xmlVersion, 0, 7);

        $filename = $product->getProductId() . '.xml';
        try {
            $this->fs->dumpFile("{$this->exportDir}/{$filename}", $xml);
            $this->fs->dumpFile("{$this->archiveDir}/{$filename}", $xml);
        } catch (IOExceptionInterface $exception) {
            $this->logger->error('Failed to export XML to Promus', [
                'Service' => 'PromusExport',
                'ProductKey' => $product->getKey(),
            ]);
            return;
        }

        $this->logger->notice('Exported XML to Promus', [
            'Service' => 'PromusExport',
            'ProductKey' => $product->getKey(),
            'XmlVersion' => $shortVersion,
        ]);

        $product->setExportedToPromus(true)
            ->setExportedToPromusVersion($xmlVersion)
            ->save([
                'versionNote' => 'Eksportert til Promus (' . $shortVersion . ')',
                'context' => self::EventContext,
            ]);

        (new Note())
            ->setElement($product)
            ->setType('notice')
            ->setDate(time())
            ->setTitle('Omslagsbilde eksportert til Promus (' . $shortVersion . ')')
            ->setDescription($xml)
            ->save();
    }
}
