<?php

namespace AppBundle\Service\OptionsProvider;

use Blackbit\DataDirectorBundle\model\Dataport;
use Pimcore\Model\DataObject\ClassDefinition\Data;
use Pimcore\Model\DataObject\ClassDefinition\DynamicOptionsProvider\SelectOptionsProviderInterface;

class DataDirectorDataportProvider implements SelectOptionsProviderInterface
{
    /**
     * @var Dataport
     */
    private $repository;

    public function __construct()
    {
        // Note: Would normally use dependency injection here to inject Dataport into the constructor, but it's not
        // available as a public service. In fact it's not available as a service at all, since Blackbit doesn't use
        // dependency injection internally.
        $this->repository = new Dataport();
    }

    /**
     * @param array $context
     * @param Data $fieldDefinition
     *
     * @return array
     */
    public function getOptions($context, $fieldDefinition)
    {
        //$object = isset($context["object"]) ? $context["object"] : null;
        // $fieldname = "id: " . ($object ? $object->getId() : "unknown") . " - " .$context["fieldname"];

        $results = [];

        foreach ($this->repository->find() as $row) {
            $results[] = [
                'key' => $row['name'],
                'value' =>  $row['id'],
            ];
        }

        return $results;
    }

    /**
     * @param array $context
     * @param Data $fieldDefinition
     *
     * @return bool
     */
    public function hasStaticOptions($context, $fieldDefinition)
    {
        return true; // Options do not depend on the object context
    }

    /**
     * @param array $context
     * @param Data $fieldDefinition
     *
     * @return string|null
     */
    public function getDefaultValue($context, $fieldDefinition)
    {
        return $fieldDefinition->getDefaultValue();
    }
}
