<?php

namespace AppBundle\Service;

use AppBundle\Crawler\AschehougCrawler;
use AppBundle\Crawler\CappelenDammCrawler;
use AppBundle\Crawler\Crawler;
use AppBundle\Exception\CrawlFailed;
use AppBundle\Model\CrawlerResponse;
use AppBundle\Model\Product;
use GuzzleHttp\Exception\RequestException;
use Pimcore\Log\ApplicationLogger;
use Pimcore\Model\DataObject\Data\ExternalImage;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CrawlerService
{
    private $container;
    private $logger;

    protected $crawlers = [
        '/^978820[24].*/' => CappelenDammCrawler::class,
        '/^9788203.*/' => AschehougCrawler::class,
    ];

    public function __construct(
        ContainerInterface $container,
        ApplicationLogger $logger
    )
    {
        $this->container = $container;
        $this->logger = $logger;
    }

    public function crawl(string $isbn): ?CrawlerResponse
    {
        $crawler = $this->getCrawler($isbn);
        if (!$crawler) {
            return null;
        }
        try {
            return $crawler->crawl($isbn);
        } catch (RequestException $err) {
            throw new CrawlFailed("Crawler failed to get <{$err->getRequest()->getUri()}>: {$err->getMessage()}", $crawler);
        } catch (\Exception $err) {
            $errType = get_class($err);
            throw new CrawlFailed(
                "Crawler failed for {$isbn}: {$errType}: {$err->getMessage()}\n\n" . $err->getTraceAsString(),
                $crawler
            );
        }
    }

    public function updateProduct(Product $product): bool
    {
        if (!$product->getIsbn()) {
            return false;
        }
        $response = $this->crawl($product->getIsbn());
        if (!$response) {
            return false;
        }
        $modified = false;

        // ------------------------------------------------------------------
        // Set cover URL

        if ($response->get('imageExternal')) {
            $this->logger->info("Found cover for product: {$product->getKey()}", [
                'component' => $response->crawlerName,
            ]);
            $product->setImageExternal(new ExternalImage($response->get('imageExternal')));
            $modified = true;
        } else {
            $this->logger->warning("Crawler didn't find cover for {$product->getIsbn()}", [
                'component' => $response->crawlerName,
            ]);
        }

        // ------------------------------------------------------------------
        // Set other properties, but don't overwrite existing values

        $properties = [
            'title',
            'pageCount',
            'publicationDate',
            'publisherSummary',
        ];

        foreach ($properties as $property) {
            if (!$product->{'get' . ucfirst( $property)}() && $response->get($property)) {
                $product->{'set' . ucfirst( $property)}($response->get($property));
                $modified = true;
            }
        }

        return $modified;
    }

    private function getCrawler(string $isbn): ?Crawler
    {
        foreach ($this->crawlers as $isbnPattern => $crawlerClass) {
            if (preg_match($isbnPattern, $isbn)) {
                return $this->container->get($crawlerClass);
            }
        }
        return null;
    }
}
