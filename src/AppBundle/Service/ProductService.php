<?php

namespace AppBundle\Service;

use AppBundle\Model\Product;
use Pimcore\Log\ApplicationLogger;
use Pimcore\Model\DataObject;
use Pimcore\Model\Element\Service;

class ProductService
{
    private $productDir;
    private $logger;

    public function __construct(ApplicationLogger $logger, string $productDir)
    {
        $this->logger = $logger;
        $this->productDir = $productDir;
    }

    /**
     * Get the data object directory for products.
     *
     * @return DataObject
     */
    public function getProductDir(): DataObject
    {
        return DataObject::getByPath($this->productDir);
    }

    /**
     * Get Product by product key.
     *
     * @param string $key
     * @return Product|null
     */
    public function getByKey(string $key): ?Product
    {
        return Product::getByProductId($key, 1);
    }

    /**
     * Get Product by UUID.
     *
     * @param string $uuid
     * @return Product|null
     */
    public function getByUuid(string $uuid): ?Product
    {
        return Product::getByUuid($uuid, 1);
    }

    /**
     * Get Product by ISBN.
     *
     * @param string $isbn
     * @return Product|null
     */
    public function getByIsbn(string $isbn): ?Product
    {
        return Product::getByISBN($isbn,1);
    }

    /**
     * Make a new Product, set init values and return it, but don't persist to DB.
     *
     * @param string $key
     * @return Product
     */
    public function make(string $key): Product
    {
        $product = new Product();
        $product->setKey(Service::getValidKey($key, 'object'));
        $product->setParent($this->getProductDir());

        return $product;
    }
}
