<?php

namespace AppBundle\Command;

use AppBundle\Model\Product;
use Pimcore\Console\AbstractCommand;
use Pimcore\Db;
use Pimcore\Model\Asset;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;


class MassDelete extends AbstractCommand
{
    public function __construct(LoggerInterface $logger)
    {
        parent::__construct();
        $this->logger = $logger;
    }

    protected function configure()
    {
        $this
            ->setName('aja:mass-delete')
            ->setDescription('Delete objects and/or assets en masse')
            ->addArgument('name', InputArgument::REQUIRED, 'What do you want to delete? "objects" or "assets"');
    }

    protected function deleteUnusedObjects()
    {
        $this->output->writeln('Looking');

        $rows = Db::get()->fetchAll(
            'SELECT uuid, isbn FROM object_PRD WHERE o_userOwner = ? AND originalSource IS NULL AND title IS NULL AND o_modificationDate < ?',
            [0, time() - 86400 * 1]
        );

        $delete = 0;
        foreach ($rows as $row) {
            Product::getByUuid($row['uuid'], 1)->delete();
            $this->logger->notice('Deleted product with UUID=' . $row['uuid'] . ' ISBN=' . $row['isbn']);
            $delete++;
        }

        $this->output->writeln("<info>Deleted $delete objects</info>");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('Husket å ta backup først? Klar for å slette?', false);
        if (!$helper->ask($input, $output, $question)) {
            return 0;
        }

        switch($input->getArgument('name')) {
            case 'objects':
                $this->deleteUnusedObjects();
                break;
            case 'assets':
                $this->deleteAssets();
                break;
            default:
                $this->output->writeln("<error>Name must be one of: {objects, assets}</error>");
        }
    }

    protected function deleteAssets()
    {
        $rows = Db::get()->fetchAll(
            'SELECT id, filename FROM assets WHERE parentId = ?',
            [3]
        );
        print(count($rows) . " assets \n");
        $delete = 0;
        $t0 = time();
        foreach ($rows as $row) {
            Asset::getById($row['id'])->delete();
            $this->logger->notice('Deleted asset with id=' . $row['id'] . ' filename=' . $row['filename']);
            $delete++;
            if ($delete % 100 == 0) {
                $dt = time() - $t0;
                $this->output->writeln("<info>Deleted $delete assets in $dt secs</info>");
            }
        }
        $this->output->writeln("<info>Deleted $delete assets</info>");
    }
}
