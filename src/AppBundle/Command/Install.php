<?php

namespace AppBundle\Command;

use Pimcore\Config;
use Pimcore\Console\AbstractCommand;
use Pimcore\Model\Element;
use Pimcore\Model\Asset;
use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\AbstractObject;
use Pimcore\Model\User\Role;
use Pimcore\Model\User\Workspace\Asset as WorkspaceAsset;
use Pimcore\Model\User\Workspace\DataObject as WorkspaceDataObject;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Install extends AbstractCommand
{
    private $container;
    private $pimcoreConfig;
    private $poolDir;

    /**
     * Bootstrap constructor.
     *
     * @param Config $pimcoreConfig
     * @param ContainerInterface $container
     * @param string $poolDir
     */
    public function __construct(Config $pimcoreConfig, ContainerInterface $container, string $poolDir)
    {
        parent::__construct();
        $this->pimcoreConfig = $pimcoreConfig;
        $this->container = $container;
        $this->poolDir = $poolDir;
    }

    /**
     * Configure command name, arguments and options.
     */
    protected function configure()
    {
        $this
            ->setName('aja:install')
            ->setDescription('Create folders, populate/update roles and permissions.')
            ->setHelp(
                <<<EOF
The <info>%command.name%</info> command creates the standard folders and roles that we use in Aja.
Since the set of standard folders and roles can change over the time, the command is
designed to be idempotent, so that it can be run after each deploy.
EOF
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->configureFolders();

        // List of independent tasks, we can split these out in separate classes if this file grows to big
        $roles = [
            'admin',
            'productEditor',
        ];
        foreach ($roles as $role) {
            $this->{'configure' . ucfirst($role) . 'Role'}($output);
        }

        return 0;
    }

    // ----

    protected function configureFolders()
    {
        $pools = [
            'import' => [
               'omslagsbilder-fra-forlag',
               'omslagsbilder-scannet',
               'omslagsbilder-ingram',
               'omslagsbilder-gardners',
               'omslagsbilder-bokbasen',
               'onix-fra-forlag',
               'excel-cappelen-damm',
               'excel-aschehoug',
               'excel-favola',
               'excel-gyldendal',
               'excel-standardmal',
            ],
            'export' => [
                'promus',
            ]
        ];

        foreach ($pools as $poolType => $poolDirs) {
            foreach ($poolDirs as $poolDir) {
                $path = "{$this->poolDir}/{$poolType}/{$poolDir}";
                if (!file_exists($path)) {
                    mkdir($path, 0755, true);
                }
                $path = "{$this->poolDir}/archive/{$poolType}-{$poolDir}";
                if (!file_exists($path)) {
                    mkdir($path, 0755, true);
                }
            }
        }
    }


    protected function configureAdminRole()
    {
        $this->createDataObjectFolders([
            '/Integrasjoner',
            '/Integrasjoner/Import',
            '/Integrasjoner/Eksport',
        ]);

        $this->createAssetFolders([
            'import',
            'import/sd',
            'import/sd/inbox',
            'import/sd/archive',
        ]);

        // No need to configure permissions, since admin users have access to everything.

    }

    protected function configureProductEditorRole(OutputInterface $output)
    {
        $dataObjectFolders = $this->createDataObjectFolders([
            $this->container->getParameter('aja.product_dir'),
            '/Kategorier/Innbindinger',
        ]);

        $role = $this->createRole('productEditor');

        $role->setPermissions([
            'application_logging',
            'assets',
            'bundle_advancedsearch_search',
            'notes_events',
            'objects',
            'plugin_pm_permission_view',
        ]);

        $role->setClasses(['PRD','BND']); // Allowed to create products and Bindings

        $role->setWorkspacesObject(array_map(function(DataObject\Folder $folder) {
            return (new WorkspaceDataObject())
                ->setCid($folder->getId())
                ->setCpath($folder->getRealFullPath())
                ->setValues([
                    'list' => true,
                    'view' => true,
                    'save' => true,
                    'publish' => true,
                    'unpublish' => true,
                    'create' => true,
                    'settings' => true,
                    'versions' => true,
                    'properties' => true,
                ]);
        }, $dataObjectFolders));

        $role->save();
    }

    // ----

    /**
     * @param string[] $paths
     * @return Asset\Folder[]
     */
    protected function createAssetFolders(array $paths): array
    {
        return array_map(function($path) {
            return Asset\Service::createFolderByPath($path);
        }, $paths);
    }

    /**
     * @param string[] $paths
     * @return DataObject\Folder[]
     */
    protected function createDataObjectFolders(array $paths): array
    {
        return array_map(function($path) {
            return DataObject\Service::createFolderByPath($path);
        }, $paths);
    }

    /**
     * @param string $name
     * @param int $parentId
     * @return Role
     */
    protected function createRole(string $name, int $parentId = 0): Role
    {
        $role = Role::getByName($name);
        if (is_null($role)) {
            $role = (new Role())
                ->setParentId($parentId)
                ->setName($name);
        }
        return $role;
    }
}
