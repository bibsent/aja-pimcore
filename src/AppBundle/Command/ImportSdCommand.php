<?php

namespace AppBundle\Command;

use AppBundle\Job\DataDirectorImportJob;
use AppBundle\Job\DataSource\DataSourceInterface;
use AppBundle\Job\DataSource\FtpServer;
use AppBundle\Util\Utils;
use Blackbit\DataDirectorBundle\lib\Pim\Item\ImporterInterface;
use Carbon\Carbon;
use Elements\Bundle\ProcessManagerBundle\ExecutionTrait;
use Elements\Bundle\ProcessManagerBundle\Model\MonitoringItem;
use ForceUTF8\Encoding;
use Pimcore\Model\Asset;
use Pimcore\Model\Asset\Folder;
use Pimcore\Model\DataObject\AbstractObject;
use Pimcore\Model\DataObject\FtpServerConfig;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

class ImportSdCommand extends ProcessManagerCommand
{
    use ExecutionTrait;
    private $projectDir;
    private $configPath = '/Integrasjoner/Import/SD';
    private $logger;
    private $importer;
    private $inboxPath;
    private $archivePath;

    /**
     * Configure asset import arguments and options
     */
    protected function configure()
    {
        $this
            ->setName('aja:import:sd')
            ->setDescription('Fetch and import data from SD')
            ->addOption(
                'date',
                null,
                InputOption::VALUE_REQUIRED,
                'Date to download data for. Format: YYYY-MM-DD. Default: the current date.'
            )
            ->addOption(
                'full',
                null,
                InputOption::VALUE_NONE,
                'Download the full version instead of the diff file.'
            )
            ->addOption(
                'monitoring-item-id',
                null,
                InputOption::VALUE_REQUIRED,
                'Contains the monitoring item if executed via the Pimcore backend'
            )
            ->addOption(
                'config-id',
                null,
                InputOption::VALUE_REQUIRED,
                'Process Manager configuration ID'
            )
        ;
    }

    public function __construct(string $projectDir, LoggerInterface $logger, ImporterInterface $importer)
    {
        parent::__construct();
        $this->projectDir = $projectDir;
        $this->logger = $logger;
        $this->importer = $importer;

        $this->inboxPath = "/import/sd/inbox";
        $this->archivePath = "/import/sd/archive";
    }

    /**
     * @param InputInterface $input
     * @param MonitoringItem $monitor
     * @param Carbon|null $default
     * @return Carbon|null
     */
    public function getDate(InputInterface $input, MonitoringItem $monitor, Carbon $default = null): ?Carbon
    {
        $callbackSettings = $monitor->getCallbackSettings();

        // Priority for choosing the date
        // 1. Date entered when manually exeuting the process from Process Manager
        // 2. Date entered using the command-line option --date
        // 3. Current date
        if (isset($callbackSettings['date']) && !empty($callbackSettings['date'])) {
            $date = Carbon::createFromTimestamp((int)$callbackSettings['date']);
        } elseif ($input->getOption("date")) {
            $date = new Carbon($input->getOption("date"));
        } else {
            $date = $default ?: new Carbon();
        }
        if (!is_null($date)) {
            $this->putMetadata($monitor, [
                'date' => $date->isoFormat('YYYY-MM-DD'),
            ]);
        }

        return $date;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var MonitoringItem $monitor */
        $monitor = $this->initProcessManager(
            $input->getOption('monitoring-item-id'), [
                'autoCreate' => true,
                'ConfigurationId' => $input->getOption('config-id'),
            ]
        );

        $this->logger = $monitor->getLogger();

        $callbackSettings = $monitor->getCallbackSettings();
        if (isset($callbackSettings['full'])) {
            $full = (bool) $callbackSettings['full'];
        } else {
            $full = (bool) $input->getOption('full');
        }

        $date = $this->getDate($input, $monitor);

        // If started from command line, the MonitoringItem won't have a name.
        $jobName = $monitor->getName() ?? 'Manual run';
        $monitor->setName($jobName)->save();

        $this->logger->info('[ImportSdCommand] Starting ' . ($full ? 'full import' : 'daily diff import'));

        $config = AbstractObject::getByPath($this->configPath);

        if (is_null($config)) {
            $output->writeln("<error>Could not find FtpServerConfig object at: {$this->configPath}</error>");
            return;
        }

        if ($full) {
            $config->setRemotePaths(str_replace('_diff', '', $config->getRemotePaths()));
        }

        if (is_null($config) || !$config instanceof FtpServerConfig) {
            $monitor
                ->setStatus(MonitoringItem::STATUS_FAILED)
                ->setMessage("Configuration object not found, or wrong type (not FtpServerConfig): $this->configPath")
                ->save();
            return 1;
        }
        $inbox = Folder::getByPath($this->inboxPath);
        $archive = Folder::getByPath($this->archivePath);
        if (is_null($inbox)) {
            $monitor
                ->setStatus(MonitoringItem::STATUS_FAILED)
                ->setMessage("{$this->inboxPath} does not exist")
                ->save();
            return 1;
        }
        if (is_null($archive)) {
            $monitor
                ->setStatus(MonitoringItem::STATUS_FAILED)
                ->setMessage("{$this->archivePath} does not exist")
                ->save();
            return 1;
        }

        $server = new FtpServer($monitor, $this->logger, $config, $date, $inbox);

        $server->setFilenameFilter($config->getFilenameFilter());
        foreach ($server->getFiles() as $asset) {

            // Normalize encoding
            $data = $asset->getData();
            if (substr($data, 0, 2) == chr(0xFF) . chr(0xFE)) {
                // UTF-16 Little Endian BOM. Note that iconv fails to remove the BOM, so we remove it manually
                $data = iconv("UTF-16LE", "UTF-8", substr($data, 2));
                $this->logger->info('[ImportSdCommand] Converted "' . $asset->getFilename() .'" from UTF-16LE to UTF-8 using iconv');
            } else {
                // Most likely Windows-1252
                $data = Encoding::toUTF8($data);
                $this->logger->info('[ImportSdCommand] Converted "' . $asset->getFilename() .'" to UTF-8');
            }
            $data = str_replace("\r\n", "\n", $data);    // Windows linebreaks -> Unix
            $asset->setData($data);
            $asset->save();

            if (!str_starts_with($data, '"ISBN"')) {
                // Add header if it's missing (currently it's missing for the diff files)
                $headerRow = '"ISBN";"Tittel";"Orginaltittel";"Undertittel";"Forfatter";"Serie";"Serie nummer";"Forlag";"Forlagsnavn";"UtgDato";"Meldingskode";"Meldingstekst";"Meldingsdato";"Innbindingstype";"Innbindings tekst";"Språk";"Bokgruppe";"Pris";"Mva";"Vekt";"Hoyde";"Bredde";"Tykkelse";';
                $data = $headerRow . "\n" . $data;
                $asset->setData($data);
                $asset->save();

                $this->logger->info('[ImportSdCommand] Added header row to "' . $asset->getFilename() .'"');
            }

            $rowCount = count(explode("\n", $data)) - 1;

            // Import
            $this->logger->info('[ImportSdCommand] Importing file: ' . $asset->getFullPath() . " ($rowCount rows)");
            $job = new DataDirectorImportJob($this->importer, $monitor, 13, $asset->getFullPath());
            $job->start();
            $this->logger->info('[ImportSdCommand] Imported file: ' . $asset->getFullPath());

            // Archive
            $existingAsset = Asset::getByPath($archive->getFullPath() . '/' . $asset->getFilename());
            if (!is_null($existingAsset)) {
                $existingAsset->delete();
            }
            $asset->setParent($archive);
            $asset->save();
            $this->logger->info('[ImportSdCommand] Archived file: ' . $asset->getFullPath());
        }

        $monitor->setMessage('[ImportSdCommand] Import finished')->setCompleted();
        return 0;
    }
}
