<?php

namespace AppBundle\Command;

use Elements\Bundle\ProcessManagerBundle\Model\MonitoringItem;
use Pimcore\Console\AbstractCommand;

abstract class ProcessManagerCommand extends AbstractCommand
{
    protected function listMetadata(MonitoringItem $monitor): array
    {
        $metadata = $monitor->getMetaData();
        if (empty($metadata)) {
            return [];
        }
        return json_decode($metadata, true);
    }

    protected function putMetadata(MonitoringItem $monitor, array $values): void
    {
        $metadata = $this->listMetadata($monitor);
        foreach ($values as $key => $value) {
            $metadata[$key] = $value;
        }
        $monitor
            ->setMetaData(json_encode($metadata, JSON_PRETTY_PRINT || JSON_UNESCAPED_UNICODE))
            ->save();
    }

    protected function getMetadata(MonitoringItem $monitor, string $key): ?string
    {
        $metadata = $this->listMetadata($monitor);
        return $metadata[$key] ?? null;
    }
}
