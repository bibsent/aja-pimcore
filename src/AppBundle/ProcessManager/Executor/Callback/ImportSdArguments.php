<?php

namespace AppBundle\ProcessManager\Executor\Callback;

use Elements\Bundle\ProcessManagerBundle\Executor\Callback\AbstractCallback;

class ImportSdArguments extends AbstractCallback
{
    public $extJsClass = 'pimcore.plugin.processmanager.executor.callback.importSdArguments';

    public $name = 'importSdArguments';
}
