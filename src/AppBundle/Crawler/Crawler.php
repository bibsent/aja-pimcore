<?php

namespace AppBundle\Crawler;

use AppBundle\Model\CrawlerPage;
use GuzzleHttp\ClientInterface;
use JsonPath\JsonObject;
use Pimcore\Log\ApplicationLogger;
use ReflectionClass;

abstract class Crawler
{
    protected $http;
    protected $logger;
    protected $startUrl;
    protected $bingApiKey;

    public function __construct(
        ClientInterface $http,
        ApplicationLogger $logger,
        string $bingApiKey
    )
    {
        $this->http = $http;
        $this->logger = $logger;
        $this->bingApiKey = $bingApiKey;
    }

    public function shortName()
    {
        return (new ReflectionClass($this))->getShortName();
    }

    protected function start($isbn)
    {
        return $this->get(
            str_replace('{isbn}', $isbn, $this->startUrl)
        );
    }

    protected function get(string $url): CrawlerPage
    {
        return new CrawlerPage(
            $this->logger,
            $this,
            $url,
            $this->http->request('GET', $url)->getBody()->getContents()
        );
    }

    protected function searchBing(string $query)
    {
        $json = new JsonObject($this->http->request(
            'GET',
            'https://api.bing.microsoft.com/v7.0/search',
            [
                'query' => ['q' => $query],
                'headers' => ['Ocp-Apim-Subscription-Key' => $this->bingApiKey],
            ]
        )->getBody()->getContents());

        $results = $json->get('$.webPages.value[*].url') ?: [];

        $this->logger->debug("Query \"$query\": " . count($results) . " results: " . implode("\n", $results));
        return $results;
    }
}
