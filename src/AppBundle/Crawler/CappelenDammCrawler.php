<?php

namespace AppBundle\Crawler;

use AppBundle\Model\CrawlerResponse;

class CappelenDammCrawler extends Crawler implements CoverCrawlerInterface
{
    protected $startUrl = 'https://www.cappelendamm.no/sek-asset/external-resources/highres/product/{isbn}.jpg';

    public function crawl(string $isbn): CrawlerResponse
    {
        $imageUrl = str_replace('{isbn}', $isbn, $this->startUrl);

        $response = $this->http->request('GET', $imageUrl);

        if (!$response->hasHeader('Content-Length') || (int) $response->getHeader('Content-Length')[0] < 5000) {
            // Server doesn't return 404, but instead a small GIF
            return CrawlerResponse::make($this, $imageUrl, false);
        }

        return CrawlerResponse::make($this, $imageUrl)
            ->set('imageExternal', $imageUrl);
    }
}
