<?php

namespace AppBundle\Crawler;

use AppBundle\Model\CrawlerResponse;

interface CoverCrawlerInterface
{
    /**
     * Crawl a product.
     *
     * @param string $isbn
     * @return CrawlerResponse
     */
    public function crawl(string $isbn): CrawlerResponse;
}
