<?php

namespace AppBundle\Crawler;

use Carbon\Carbon;
use AppBundle\Model\CrawlerPage;
use AppBundle\Model\CrawlerResponse;

class AschehougCrawler extends Crawler implements CoverCrawlerInterface
{
    protected $startUrl = 'https://aschehoug.no/catalogsearch/result/?q={isbn}';

    /**
     * Crawl a product.
     *
     * @param string $isbn
     * @return CrawlerResponse
     */
    public function crawl(string $isbn): CrawlerResponse
    {
        $searchPage = $this->start($isbn);
        $productUrl = $searchPage->dom->filter('a.product-item-link')->link()->getUri();
        $this->logger->debug("Found product url for $isbn: $productUrl", ['component' => 'AschehougCrawler']);

        return $this->get($productUrl)
            ->extract([
                // Aschehoug ønsker ikke at bildene skal brukes av bibliotek
                // 'imageExternal' => function(CrawlerPage $page) {
                //     return $page->dom->filter('div.product-download-image a')->link()->getUri();
                // },
                'pageCount' => function(CrawlerPage $page) {
                    return $page->dom->filterXPath('//span[@data-th="Sider"]')->text();
                },
                'publicationDate' => function(CrawlerPage $page) {
                    return Carbon::createFromFormat(
                        'd.m.Y',
                        $page->dom->filterXPath('//span[@data-th="Salgsdato"]')->text()
                    );
                },
                'publisherSummary' => function(CrawlerPage $page) {
                    return $page->dom->filterXPath('//*[@id="product.info.description.simple"]//*[@class="value"]')->text();
                },
                'title' => function(CrawlerPage $page) {
                    return (string) $page->microdata->getProperty('name', 'http://schema.org/', 0);
                },
            ]);
    }
}
