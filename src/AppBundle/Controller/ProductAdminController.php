<?php

namespace AppBundle\Controller;

use AppBundle\Service\ExportService;
use AppBundle\Model\Product;
use Pimcore\Bundle\AdminBundle\Controller\AdminController;
use Pimcore\Bundle\AdminBundle\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductAdminController extends AdminController
{
    /**
     * @Route("/admin/aja/export-to-promus/{id}", methods={"POST"}, name="aja.export_to_promus")
     * @param int $id
     * @return Response
     */
    public function exportToPromusAction(ExportService $exporter, int $id): Response
    {
        $product = Product::getById($id);

        if (!$product->getCoverUrlOriginal()) {
            return new JsonResponse([
                'success' => false,
                'message' => 'XML ble ikke sendt til Promus fordi varen mangler omslagsbilde.',
            ]);
        }

        $exporter->exportToPromus($product, true);

        return new JsonResponse([
            'success' => true,
            'message' => 'XML ble sendt til Promus',
        ]);
    }
}
