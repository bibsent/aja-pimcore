<?php

namespace AppBundle\Controller;

use AppBundle\Service\ProductService;
use Pimcore\Controller\FrontendController;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

class CatmanduApiController extends FrontendController
{
    use JsonRequestTrait;

    /**
     * Endpoint for Catmandu to PUT records to, as it receives them from Promus.
     *
     * @Route("/integrations/catmandu/products/{productKey}", methods={"PUT"})
     */
    public function putProduct(
        Request $request,
        LoggerInterface $logger,
        ProductService $products,
        string $productKey
    ): Response
    {
        $this->assertApiKeyIsValid($request);

        $json = $this->decodeJsonRequest($request);

        $logger->notice('Vare mottatt fra Catmandu', [
            'product' => $productKey,
        ]);

        $product = $products->getByKey($productKey);
        if (is_null($product)) {
            $product = $products->make($productKey);
            $logMsg = 'Ny vare opprettet fra Catmandu';
        } else {
            $logMsg = 'Vare oppdatert fra Catmandu';
        }

        // Update product
        foreach (['title'] as $prop) {
            if ($json->has($prop) && empty($product->{'get' . ucfirst($prop)}())) {
                $value = $json->get($prop);
                if (is_array($value)) {
                    if (!count($value)) {
                        continue; // empty array
                    }
                    $value = $value[0];
                }
                $product->{'set' . ucfirst($prop)}($value);
            }
        }

        $product->save(['versionNote' => $logMsg]);

        return $this->json(['success' => true]);
    }

    private function assertApiKeyIsValid(Request $request)
    {
        $givenKey = $request->headers->get('apikey');
        $validKey = $this->getParameter('aja.catmandu_api_key');

        if (empty($validKey)) {
            throw new HttpException (500, 'Catmandu API key not configured');
        }

        if ($givenKey !== $validKey) {
            throw new AccessDeniedHttpException('Permission denied, apikey not valid');
        }
    }
}
