<?php

namespace AppBundle\Controller;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CoverController
 * @package AppBundle\Controller
 *
 * We served cover images directly from Pimcore from Nov 2020 until May 2021, when we moved this to a
 * separate service, so this controller now only redirects to the new service. We might eventually handle
 * redirects higher up in the stack (webserver level) to make them faster, but it might still be a good
 * idea to keept this controller around indefinitely as a fallback.
 */
class CoverController extends FrontendController
{
    private $mediaServerUrl;

    public function __construct(string $mediaServerUrl)
    {
        // Normalize end
        $this->mediaServerUrl = rtrim($mediaServerUrl, '/');
    }

    private function thumbnailResponse(string $uuid, string $variant)
    {
        return $this->redirect("{$this->mediaServerUrl}/$uuid/cover/$variant", 301);
    }

    /**
     * Permanent URL for original cover images, exported to Promus.
     *
     * @Route("/{uuid}/cover/original.jpg", name="cover_original")
     */
    public function coverOriginalJpegAction(string $uuid): Response
    {
        return $this->thumbnailResponse($uuid, 'original.jpg');
    }

    /**
     * Permanent URL for cover image thumbnails, exported to Promus.
     *
     * @Route("/{uuid}/cover/thumbnail.jpg", name="cover_thumbnail")
     */
    public function coverThumbnailJpegAction(string $uuid): Response
    {
        return $this->thumbnailResponse($uuid, 'thumbnail.jpg');
    }
}
