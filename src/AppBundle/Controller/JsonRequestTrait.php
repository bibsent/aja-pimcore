<?php

namespace AppBundle\Controller;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

trait JsonRequestTrait
{
    /**
     * @param Request $request
     * @return mixed
     */
    protected function decodeJsonRequest(Request $request)
    {
        if ($request->getContentType() !== 'json') {
            throw new HttpException(500, 'Expected JSON request');
        }
        try {
            return new ParameterBag(json_decode(
                (string)$request->getContent(),
                true,
                512,
                JSON_THROW_ON_ERROR
            ));
        } catch (\JsonException $exception) {
            throw new HttpException(500, 'Invalid JSON request');
        }
    }
}
