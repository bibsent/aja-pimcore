<?php

namespace AppBundle\Controller;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class DefaultController extends FrontendController
{
    /**
     * 2021-01-08:
     * setViewAutoRender is deprecated, but the Pimcore Document editor breaks if we remove it,
     * so we will have to wait until Pimcore (and Pimcore Toolbox?) is ready for the removal of it.
     *
     * @param FilterControllerEvent $event
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        // set auto-rendering to twig
        $this->setViewAutoRender($event->getRequest(), true, 'twig');
    }

    public function defaultAction(Request $request)
    {
    }
}
