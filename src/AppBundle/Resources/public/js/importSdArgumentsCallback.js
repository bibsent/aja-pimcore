pimcore.registerNS("pimcore.plugin.processmanager.executor.callback.importSdArguments");
pimcore.plugin.processmanager.executor.callback.importSdArguments = Class.create(pimcore.plugin.processmanager.executor.callback.abstractCallback, {

    // Example: https://github.com/elements-at/ProcessManager/blob/master/src/Resources/public/js/executor/callback/example.js

    name: "importSdArguments",

    getFormItems: function () {
        return [
            this.getDateField('date', {
                mandatory: false,
                tooltip: t("plugin_pm_date_tooltip") 
            }),
            this.getCheckbox('full', {
                mandatory: false,
                tooltip: t("plugin_pm_full_tooltip") 
            }),
        ];
    },

    execute: function () {
        this.openConfigWindow();
    }
});