pimcore.registerNS("pimcore.plugin.AppBundle");

pimcore.plugin.AppBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.AppBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("AppBundle ready!");
    },

    postOpenObject: function (object, type) {
        /*
        if (object.data.general.o_className === "Product") {
            object.toolbar.add({
                iconCls: "aja_icon_admin_menu",
                scale: "medium",
                tooltip: "Ajajaja",
                menu: {
                    xtype: "menu",
                    items: [{
                        text: t("Export to Promus"),
                        iconCls: "pimcore_icon_upload",
                        scale: "small",
                        handler: this.exportToPromus.bind(this, object),
                    }]
                }
            });
            pimcore.layout.refresh();
        }
        */
    },

    exportToPromus: function (obj) {
        const id = obj.data.general.o_id;
        //var productId = obj.data.data.productId;
        //console.log(obj.data.data);
        Ext.Ajax.request({
            url: `/admin/aja/export-to-promus/${id}`,
            method: "POST",
            success: function(response, opts) {
                let data = Ext.decode(response.responseText);
                if (data.success) {
                    //  pimcore.helpers.showNotification(t("success"), t("plugin_pm_config_execution_success"), "success");
                    console.info("Export successful", data);
                    pimcore.helpers.showNotification("Varen ble eksportert", data.message, "success");
                } else{
                    pimcore.helpers.showNotification("Ikke eksportert", data.message, "error");
                }
            },
            failure: function(response, opts) {
                console.error("Export failed:", response.status);
                pimcore.helpers.showNotification(t("error"), "Omslagsbilde-URL-er ble ikke eksportert på grunn av en teknisk feil", "error", response.responseText);
            }
        });
    },

});

const AppBundlePlugin = new pimcore.plugin.AppBundle();
