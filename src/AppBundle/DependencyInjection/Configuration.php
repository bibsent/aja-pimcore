<?php

namespace AppBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('app');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        $treeBuilder->getRootNode()
            ->children()
                ->scalarNode('product_dir')->end()
                ->scalarNode('cover_dir')->end()
                ->scalarNode('cover_upload_dir')->end()
                ->scalarNode('blank_cover')->end()
                ->scalarNode('base_uri')->end()
                ->scalarNode('media_server_url')->end()
                ->scalarNode('promus_export_dir')->end()
                ->scalarNode('promus_export_archive_dir')->end()
                ->arrayNode('integrations')
                    ->children()
                        ->arrayNode('catmandu')
                            ->children()
                                ->scalarNode('api_key')->end()
                            ->end()
                        ->end()
                        ->arrayNode('nettbutikk_graphql')
                            ->children()
                                ->scalarNode('url')->end()
                                ->scalarNode('api_key')->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
