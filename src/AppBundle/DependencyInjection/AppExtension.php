<?php

namespace AppBundle\DependencyInjection;

use AppBundle\Controller\CoverController;
use AppBundle\Service\ExportService;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\DependencyInjection\ConfigurableExtension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class AppExtension extends ConfigurableExtension
{
    /**
     * @inheritdoc
     */
    public function loadInternal(array $mergedConfig, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');

        // Sanity checks

        $fs = new Filesystem();
        foreach (['promus_export_dir', 'promus_export_archive_dir'] as $dir) {
            if (!$fs->exists($mergedConfig[$dir])) {
                $fs->mkdir($mergedConfig[$dir], 0755);
            }
        }

        // Set parameters

        $container->setParameter('aja.catmandu_api_key', $mergedConfig['integrations']['catmandu']['api_key']);
        $container->setParameter('aja.product_dir', $mergedConfig['product_dir']);

        // Manually wire scalar arguments that are only used in a single service

        $definition = $container->getDefinition(ExportService::class);
        $definition->setArgument('$exportDir', $mergedConfig['promus_export_dir']);
        $definition->setArgument('$archiveDir', $mergedConfig['promus_export_archive_dir']);

        $definition = $container->getDefinition(CoverController::class);
        $definition->setArgument('$mediaServerUrl', $mergedConfig['media_server_url']);
    }
}
