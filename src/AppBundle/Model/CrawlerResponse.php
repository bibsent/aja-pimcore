<?php

namespace AppBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Crawler\Crawler;

class CrawlerResponse
{
    protected $logger;
    protected $data;
    public $sourceUrl;
    public $crawlerName;
    public $success;

    public function __construct()
    {
        $this->data = new ArrayCollection();
    }

    public static function make(Crawler $crawler, string $sourceUrl, bool $success = true): CrawlerResponse
    {
        $obj = new static();
        $obj->crawlerName = $crawler->shortName();
        $obj->sourceUrl = $sourceUrl;
        $obj->success = $success;
        return $obj;
    }

    public function set($key, $value): CrawlerResponse
    {
        $this->data->set($key, $value);
        return $this;
    }

    public function get($key)
    {
        return $this->data->get($key);
    }
}
