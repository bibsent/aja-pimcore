<?php

namespace AppBundle\Model;

use AppBundle\Crawler\Crawler;
use Jkphl\Micrometa\Ports\Format;
use Jkphl\Micrometa\Ports\Parser;
use Pimcore\Log\ApplicationLogger;
use Symfony\Component\DomCrawler\Crawler as DomCrawler;

class CrawlerPage
{
    public $url;
    public $crawler;
    public $dom;
    public $microdata;

    protected $logger;
    protected $html;

    public function __construct(ApplicationLogger $logger, Crawler $crawler, string $url, string $html)
    {
        $this->logger = $logger;
        $this->crawler = $crawler;
        $this->url = $url;
        $this->html = $html;
        $this->dom = new DomCrawler($html);
        $microdata = (new Parser(Format::MICRODATA))($url, $html);
        $this->microdata = count($microdata) ? $microdata[0] : null;
    }

    public function extract(array $fields): CrawlerResponse
    {
        $response = CrawlerResponse::make($this->crawler, $this->url);

        foreach ($fields as $field => $cb) {
            try {
                $response->set($field, $cb($this));
            } catch (\Exception $err) {
                $this->logger->debug("Could not extract '$field' from {$this->url}", [
                    'component' => $this->crawler->shortName(),
                ]);
            }
        }

        return $response;
    }
}
