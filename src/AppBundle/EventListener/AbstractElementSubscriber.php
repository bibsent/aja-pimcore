<?php

namespace AppBundle\EventListener;

use Pimcore\Event\Model\AssetEvent;
use Pimcore\Event\Model\DataObjectEvent;
use Pimcore\Model\DataObject\Product;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AbstractElementSubscriber implements EventSubscriberInterface
{
    /**
     * Map of specific data object types to listener classes.
     *
     * @var array
     */
    private $listeners;

    /**
     * ObjectListener constructor.
     *
     * @param ProductListener $productListener
     */
    public function __construct(ProductListener $productListener)
    {
        $this->listeners = [
            Product::class => $productListener,
        ];
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            'pimcore.dataobject.preAdd' => 'onPreAdd',
            'pimcore.dataobject.postAdd' => 'onPostAdd',
            'pimcore.dataobject.preUpdate' => 'onPreUpdate',
            'pimcore.dataobject.postUpdate' => 'onPostUpdate',
        ];
    }

    /**
     * Call the $method listener on a more specific listener class, if found.
     *
     * @param DataObjectEvent|AssetEvent $event
     * @param string $method
     */
    private function callListener($event, string $method)
    {
        foreach ($this->listeners as $className => $listener) {
            if (is_a($event->getElement(), $className)) {
                if (method_exists($listener, $method)) {
                    $listener->{$method}($event->getElement(), $event);
                }
            }
        }
    }

    /**
     * @param DataObjectEvent|AssetEvent $event
     */
    public function onPreAdd ($event)
    {
        $this->callListener($event, 'onPreAdd');
    }

    /**
     * @param DataObjectEvent|AssetEvent $event
     */
    public function onPostAdd ($event)
    {
        $this->callListener($event, 'onPostAdd');
    }

    /**
     * @param DataObjectEvent|AssetEvent $event
     */
    public function onPreUpdate ($event)
    {
        $this->callListener($event, 'onPreUpdate');
    }

    /**
     * @param DataObjectEvent|AssetEvent $event
     */
    public function onPostUpdate ($event)
    {
        $this->callListener($event, 'onPostUpdate');
    }
}
