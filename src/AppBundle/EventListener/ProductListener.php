<?php

namespace AppBundle\EventListener;

use AppBundle\Exception\CrawlFailed;
use AppBundle\Model\Product;
use AppBundle\Service\CrawlerService;
use Pimcore\Event\Model\DataObjectEvent;
use Psr\Log\LoggerInterface;

class ProductListener
{
    private $logger;
    private $crawlers;

    public function __construct(
        LoggerInterface $logger,
        CrawlerService $crawlers
    )
    {
        $this->logger = $logger;
        $this->crawlers = $crawlers;
    }

    public function onPreAdd (Product $product)
    {
        $productKey = $product->getKey();
        $this->logger->info('[Product.onPreAdd] ' . $productKey);

        // Do we still need this, or could we deprecate it?
        $product->setProductId($productKey);

        // If the product key looks like an ISBN, add it to the ISBN field
        if (strlen($productKey) == 13 && str_starts_with($productKey, '978')) {
            $product->setIsbn($productKey);
        }

        // Products should be published by default
        $product->setPublished(true);

        // If the Product doesn't have an image, check if we can find one.
//        try {
//            $this->crawlers->updateProduct($product);
//        } catch (CrawlFailed $err) {
//            $this->logger->error($err->getMessage(), [
//                'component' => 'CrawlerService',
//            ]);
//        }
    }

    public function onPostAdd (Product $product)
    {
        //
    }

    public function onPreUpdate (Product $product)
    {
        //
    }

    public function onPostUpdate (Product $product, DataObjectEvent $event)
    {
        //
    }
}
