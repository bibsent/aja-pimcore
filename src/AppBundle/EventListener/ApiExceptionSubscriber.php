<?php

namespace AppBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class ApiExceptionSubscriber implements EventSubscriberInterface
{
    protected $apiRoutePrefixes = [
        '/integrations',
    ];

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException',
        ];
    }

    protected function isApiRequest(string $path): bool {
        foreach ($this->apiRoutePrefixes as $prefix) {
            if (str_starts_with($path, $prefix)) {
                return true;
            }
        }
        return false;
    }

    public function onKernelException(ExceptionEvent $event)
    {
        if (!$this->isApiRequest($event->getRequest()->getPathInfo())) {
            return;
        }
        $error = $event->getThrowable();
        $data = [
            'error' => $error->getMessage(),
        ];
        if (\Pimcore::inDebugMode()) {
            $data['trace'] = $error->getTraceAsString();
        }
        $event->setResponse(new JsonResponse(
            $data,
            $error instanceof HttpException ? $error->getStatusCode() : 500
        ));
    }

}
