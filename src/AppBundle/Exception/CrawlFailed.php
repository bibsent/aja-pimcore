<?php

namespace AppBundle\Exception;

use AppBundle\Crawler\Crawler;

class CrawlFailed extends \RuntimeException
{
    public $crawler;

    public function __construct($message, Crawler $crawler)
    {
        parent::__construct($message);
        $this->crawler = $crawler;
    }
}
