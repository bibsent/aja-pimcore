<?php

namespace AppBundle\DataDirector;

use Pimcore\Model\DataObject\Binding;
use Pimcore\Model\DataObject\AbstractObject;

class BindingTranslator
{

    private static $bindingDir = '/Kategorier/Innbindinger/';

    /**************************************************
     * Returnerer navn på innbindingskoden i en DataDirector-callback-funksjon
     * Eksempelbruk:
     *
     *     return \AppBundle\DataDirector\BindingTranslator::translate($params['value']);
     *
     * @param string|null $value
     * @return string|null
     */
    public static function translate(?string $value): ?string
    {
        //Henter objektet vha stien
        $path = self::$bindingDir . strtolower($value);
        $binding = AbstractObject::getByPath($path);

        if (!is_null($binding) && ($binding instanceof Binding)) {
            //Hvis finnes og er av typen Binding
            return $binding->getName();
        }
        else {
            return null;
        }
    }

}
