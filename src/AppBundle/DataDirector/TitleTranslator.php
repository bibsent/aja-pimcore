<?php

namespace AppBundle\DataDirector;

class TitleTranslator
{
    /*************************************************************************
     * Returnerer hovedtittel, serietittel, serienummer eller undertittel fra en tittel.
     *
     * Eksempelbruk: For å hente ut hovedtittel i en DataDirector-callback-funksjon:
     *
     *     return \AppBundle\DataDirector\TitleTranslator::translate($params['value'], 'maintitle')
     *
     * For å hente ut undertittel:
     *
     *     return \AppBundle\DataDirector\TitleTranslator::translate($params['value'], 'seriestitle')
     */
    public static function translate(?string $title, string $titlePart): ?string
    {
        if (is_null($title)) {
            return null;
        }
        $titleParts = self::splitTitle($title);
        if (!isset($titleParts[$titlePart])) {
            throw new \Exception('Unknown title part: ' . $titlePart);
        }

        return $titleParts[$titlePart];
    }

    /*************************************************************************
     * Deler opp en tittel i fire deler: tittel, serietittel, serienummer og
     * undertittel ved hjelp av en interessant regex.
     */
    public static function splitTitle(string $title): array
    {
        // Test: https://regexr.com/5l88p
        preg_match('/^(?:(.*?)(\d*)\. (?![a-z]))?(.+?)(?: - (.*))?$/', $title, $matches);

        $parts = [
            'seriestitle' => $matches[1] ?? '',
            'seriesnumber' => $matches[2] ?? '',
            'maintitle' => $matches[3] ?? '',
            'subtitle' => $matches[4] ?? '',
        ];

        foreach ($parts as $key => $value) {
            $value = trim($value);
            $parts[$key] = empty($value) ? null : $value;
        }

        return $parts;
    }

}
