<?php

namespace AppBundle\DataDirector;

class CreatorTranslator
{
    public static function translate(?string $value, bool $invertAlways = false): array
    {
        return is_null($value) ? [] : self::invertNamesSometimes(
            self::toObjects(
                self::splitNames($value)
            ), $invertAlways
        );
    }

    /************************************************************************************
     * Del opp forfattere på "/" og "og", ";", "&" og i noen tilfeller ","
     *
     * Eksempler:
     *    "Lirhus, Agnar"
     *      -> ["Lirhus, Agnar"]  (ingen endring)
     *    "Viestad, Vibeke Maria og Helena Lindholm"
     *      -> ["Viestad, Vibeke Maria", "Helena Lindholm"]
     *    "Eidsvold, Trude Trønnes, Rosa von Krogh og Kine Yvonne Kjær"
     *      -> ["Eidsvold, Trude Trønnes", "Rosa von Krogh", "Kine Yvonne Kjær"]
     *    "Moestrup, Mette / Søyseth, Eira"
     *      -> ["Moestrup, Mette", "Søyseth, Eira"]
     */
    public static function splitNames(string $value): array
    {
        // Vi deler først opp på ' og ', '/', '&' og ';'
        $values = preg_split('/( og |\/|&|;)/', $value);

        // Deretter deler vi på annenhvert komma innad i hvert ledd
        $values = array_merge(...array_map(function($value) {
            return array_map(
                function($iv) {
                    return implode(',', $iv);
                },
                array_chunk(
                    explode(',', $value),
                    2
                )
            );
        }, $values));

        // Og fjerner omkringliggende whitespace
        return array_map('trim', $values);
    }


    /************************************************************************************
     * Gjør om til objekter og skill ut tekst i parentes som rolle.
     * Mapp forkortelser til utskrevne verdier.
     */
    public static function toObjects(array $values): array
    {
        $roleMap = [
            'red' => 'redaktør',
            'ill' => 'illustratør',
            'debutant' => 'forfatter',
            'ove' => 'oversetter',
            'over' => 'oversetter',
            'med' => 'medarbeider',
            'for' => 'forfatter',
            'forf' => 'forfatter'
        ];

        return array_map(function($value) use ($roleMap) {
            $role = 'forfatter';

            if (preg_match('/^(.*) \((.*?)\)$/', $value, $matches)) {
                $value = trim($matches[1]);
                $role = rtrim(mb_strtolower($matches[2]),'.');
                if (isset($roleMap[$role])) {
                    $role = $roleMap[$role];
                }
            }

            return [
                'creatorType' => 'Person',
                'creatorName' => $value,
                'creatorRole' => $role,
            ];
        }, $values);
    }

    /************************************************************************************
     * Hvis $invertAlways=false, inverter resten av ansvarshavere hvis første ansvarerhaver har invertert navn, ellers ingen endring.
     * Eksempler:
     *   ["Kaluza, Susanne", "Leonard Furuberg"] -> ["Kaluza, Susanne", "Furuberg, Leonard"]
     *   ["Diverse forfattere"] -> ["Diverse forfattere"]  (ingen endring)
     *
     * Hvis $invertAlways=true, inverter alle ansvarshavere som ikke er inverterte.
     * Eksempler:
     *   ["Bjørn Ingvaldsen", "Kristoffer Kjølberg"] -> ["Ingvaldsen, Bjørn", "Kjølberg, Kristoffer"]
     *   ["Jo Nesbø] -> ["Nesbø, Jo"]
     */
    public static function invertNamesSometimes(array $values, bool $invertAlways = false): array
    {
        if (!count($values)) {
            return [];
        }

        if ($invertAlways) {
            $invertNames = true;
        } else {
            $invertNames = (mb_strpos($values[0]['creatorName'], ',') !== false);
        }

        if (!$invertNames) {
            return $values;
        }

        return array_map(function($value) {
            if (mb_strpos($value['creatorName'], ',') === false && mb_strpos($value['creatorName'], ' ') !== false) {
                $value['creatorName'] = explode(' ', $value['creatorName']);
                $value['creatorName'] = array_pop($value['creatorName']) . ', ' . implode(' ', $value['creatorName']);
            }
            return $value;
        }, $values);
    }

}


