<?php

namespace AppBundle\Util;

use Carbon\Carbon;
use Traversable;

class Utils
{
    static public function ensureDirExists(string $path): string
    {
        if (!file_exists($path)) {
            mkdir($path, 0755, true);
        }
        return $path;
    }

    /**
     * Map function that works with iterators.
     *
     * @param callable $fn
     * @param Traversable $values
     * @return array
     */
    static public function map(callable $fn, Traversable $values): array
    {
        $result = [];

        foreach ($values as $value) {
            $result[] = $fn($value);
        }

        return $result;
    }

    /**
     * @param string $value
     * @param Carbon $date
     * @return string
     */
    static public function expandDateTemplate(string $value, Carbon $date): string
    {
        // Expand [[date:*]] template
        return preg_replace_callback(
            '/\[\[date:(.*?)\]\]/',
            function(array $matches) use ($date) {
                return $date->isoFormat($matches[1]);
            },
            $value
        );
    }
}
